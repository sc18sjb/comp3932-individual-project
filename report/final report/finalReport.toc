\contentsline {chapter}{\numberline {1}Motivation and Background Research}{1}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Motivation}{1}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Problem Statement}{1}{section.1.2}% 
\contentsline {section}{\numberline {1.3}Background Research}{2}{section.1.3}% 
\contentsline {subsection}{\numberline {1.3.1}Stable-marriage problem}{2}{subsection.1.3.1}% 
\contentsline {subsubsection}{Matching:}{2}{subsubsection*.3}% 
\contentsline {subsubsection}{Bipartite graph:}{2}{subsubsection*.4}% 
\contentsline {subsubsection}{Gale-Shapely Algorithm:}{2}{subsubsection*.5}% 
\contentsline {subsubsection}{Constraint Satisfaction Problem (CSP):}{2}{subsubsection*.6}% 
\contentsline {subsection}{\numberline {1.3.2}Recommender Systems}{3}{subsection.1.3.2}% 
\contentsline {subsubsection}{Collaborative Filtering:}{3}{subsubsection*.7}% 
\contentsline {subsubsection}{Memory-Based Approaches:}{3}{subsubsection*.8}% 
\contentsline {subsubsection}{Model-Based Approaches:}{4}{subsubsection*.9}% 
\contentsline {subsection}{\numberline {1.3.3}Evaluation Techniques}{4}{subsection.1.3.3}% 
\contentsline {subsubsection}{Mean Absolute Error:}{4}{subsubsection*.10}% 
\contentsline {subsubsection}{Root Mean Square Error:}{4}{subsubsection*.11}% 
\contentsline {subsection}{\numberline {1.3.4}Integer Programming (IP) Model Formulation}{5}{subsection.1.3.4}% 
\contentsline {subsubsection}{ILP Solvers:}{5}{subsubsection*.12}% 
\contentsline {subsection}{\numberline {1.3.5}Existing Solutions}{5}{subsection.1.3.5}% 
\contentsline {subsubsection}{Hospitals / Residents problem:}{5}{subsubsection*.13}% 
\contentsline {subsubsection}{Assignment Problem:}{6}{subsubsection*.14}% 
\contentsline {subsubsection}{Nurse Scheduling Problem:}{6}{subsubsection*.15}% 
\contentsline {subsection}{\numberline {1.3.6}Existing Deployed Applications}{6}{subsection.1.3.6}% 
\contentsline {subsubsection}{RotaCloud:}{6}{subsubsection*.16}% 
\contentsline {subsubsection}{Rota:}{6}{subsubsection*.17}% 
\contentsline {subsubsection}{Deputy:}{6}{subsubsection*.18}% 
\contentsline {subsection}{\numberline {1.3.7}Datasets}{7}{subsection.1.3.7}% 
\contentsline {subsubsection}{Nurse Rostering Benchmark Instances:}{7}{subsubsection*.19}% 
\contentsline {subsubsection}{Employee Schedule - Kaggle:}{7}{subsubsection*.20}% 
\contentsline {chapter}{\numberline {2}Designing a Solution}{8}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Modelling the problem}{8}{section.2.1}% 
\contentsline {section}{\numberline {2.2}Creating a Stable Matching}{8}{section.2.2}% 
\contentsline {subsubsection}{Modelling Preferences:}{9}{subsubsection*.21}% 
\contentsline {subsubsection}{Choosing a Proposer:}{9}{subsubsection*.22}% 
\contentsline {subsection}{\numberline {2.2.1}Additional Constraints}{9}{subsection.2.2.1}% 
\contentsline {subsection}{\numberline {2.2.2}Modified Gale Shapely}{10}{subsection.2.2.2}% 
\contentsline {section}{\numberline {2.3}Creating a recommendation engine}{11}{section.2.3}% 
\contentsline {subsubsection}{Modelling the ratings:}{12}{subsubsection*.23}% 
\contentsline {subsection}{\numberline {2.3.1}Combining ratings}{12}{subsection.2.3.1}% 
\contentsline {subsection}{\numberline {2.3.2}Collecting Ratings}{13}{subsection.2.3.2}% 
\contentsline {subsubsection}{Rating a Rota / Shift:}{13}{subsubsection*.25}% 
\contentsline {subsubsection}{Inferring Ratings:}{13}{subsubsection*.26}% 
\contentsline {subsection}{\numberline {2.3.3}Algorithms}{13}{subsection.2.3.3}% 
\contentsline {subsubsection}{Cosine Similarity:}{13}{subsubsection*.27}% 
\contentsline {subsubsection}{Adjusted Cosine Similarity:}{14}{subsubsection*.28}% 
\contentsline {subsubsection}{k-nearest Neighbourhood:}{14}{subsubsection*.29}% 
\contentsline {subsubsection}{Choice of $k$:}{14}{subsubsection*.30}% 
\contentsline {chapter}{\numberline {3}Implementing the Web Application}{15}{chapter.3}% 
\contentsline {section}{\numberline {3.1}System Architecture}{15}{section.3.1}% 
\contentsline {subsubsection}{System Goals:}{15}{subsubsection*.31}% 
\contentsline {subsection}{\numberline {3.1.1}Designing a Database:}{15}{subsection.3.1.1}% 
\contentsline {subsection}{\numberline {3.1.2}Software Structure}{16}{subsection.3.1.2}% 
\contentsline {subsubsection}{File Structure:}{17}{subsubsection*.33}% 
\contentsline {section}{\numberline {3.2}Implementation}{17}{section.3.2}% 
\contentsline {subsection}{\numberline {3.2.1}Version Control}{17}{subsection.3.2.1}% 
\contentsline {subsection}{\numberline {3.2.2}Back-end Implementation}{18}{subsection.3.2.2}% 
\contentsline {subsubsection}{Languages and Libraries Used:}{18}{subsubsection*.35}% 
\contentsline {subsubsection}{Code Structure:}{18}{subsubsection*.36}% 
\contentsline {subsubsection}{A REST API:}{18}{subsubsection*.37}% 
\contentsline {subsubsection}{Distinguishing Users:}{18}{subsubsection*.38}% 
\contentsline {subsubsection}{Securing the Application:}{19}{subsubsection*.39}% 
\contentsline {subsection}{\numberline {3.2.3}Front-end (client) Implementation}{19}{subsection.3.2.3}% 
\contentsline {subsubsection}{Languages and Libraries Used:}{19}{subsubsection*.40}% 
\contentsline {subsubsection}{Code Structure:}{19}{subsubsection*.41}% 
\contentsline {subsubsection}{Implemented Views:}{19}{subsubsection*.42}% 
\contentsline {subsubsection}{Distinguishing Employees:}{20}{subsubsection*.43}% 
\contentsline {subsection}{\numberline {3.2.4}Algorithm's Implementation}{21}{subsection.3.2.4}% 
\contentsline {subsubsection}{Languages and Libraries Used:}{21}{subsubsection*.44}% 
\contentsline {subsubsection}{Modified Gale Shapely:}{21}{subsubsection*.45}% 
\contentsline {subsubsection}{Collaborative Filtering (CF):}{21}{subsubsection*.47}% 
\contentsline {subsubsection}{A FastApi:}{22}{subsubsection*.50}% 
\contentsline {chapter}{\numberline {4}Results and Evaluation}{23}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Unit Testing}{23}{section.4.1}% 
\contentsline {section}{\numberline {4.2}User Evaluation}{23}{section.4.2}% 
\contentsline {subsection}{\numberline {4.2.1}Participants}{24}{subsection.4.2.1}% 
\contentsline {subsection}{\numberline {4.2.2}Procedure}{24}{subsection.4.2.2}% 
\contentsline {subsubsection}{Stage 1) Testing the Web Application:}{24}{subsubsection*.52}% 
\contentsline {subsubsection}{Stage 2) Testing the Matching Algorithm:}{24}{subsubsection*.53}% 
\contentsline {subsubsection}{Stage 3) Testing CF Methods:}{25}{subsubsection*.55}% 
\contentsline {subsection}{\numberline {4.2.3}Results}{25}{subsection.4.2.3}% 
\contentsline {subsubsection}{Web Application Usability Results:}{25}{subsubsection*.57}% 
\contentsline {subsubsection}{Modified Gale-Shapely Results with no Predictions:}{27}{subsubsection*.59}% 
\contentsline {subsubsection}{CF method choice:}{27}{subsubsection*.62}% 
\contentsline {subsubsection}{Modified Gale-Shapely Results with Predictions:}{28}{subsubsection*.64}% 
\contentsline {chapter}{\numberline {5}Discussion and Conclusion}{29}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Problems with the Experiment}{29}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Conclusions}{29}{section.5.2}% 
\contentsline {subsection}{\numberline {5.2.1}Web Application Reflection}{29}{subsection.5.2.1}% 
\contentsline {subsection}{\numberline {5.2.2}Shift Schedule Reflection}{29}{subsection.5.2.2}% 
\contentsline {section}{\numberline {5.3}Ideas for future work}{30}{section.5.3}% 
\contentsline {subsubsection}{Fine-tuning:}{30}{subsubsection*.66}% 
\contentsline {subsubsection}{User Interaction}{30}{subsubsection*.67}% 
\contentsline {subsubsection}{Analytics}{30}{subsubsection*.68}% 
\contentsline {subsubsection}{Mobile Application}{30}{subsubsection*.69}% 
\contentsline {chapter}{References}{31}{subsubsection*.69}% 
\contentsline {chapter}{Appendices}{35}{section*.71}% 
\contentsline {chapter}{\numberline {A}Self-appraisal}{35}{appendix.a.A}% 
\contentsline {section}{\numberline {A.1}Critical self-evaluation}{35}{section.a.A.1}% 
\contentsline {subsection}{\numberline {A.1.1}What Went Well}{35}{subsection.a.A.1.1}% 
\contentsline {subsection}{\numberline {A.1.2}What Could be Improved}{35}{subsection.a.A.1.2}% 
\contentsline {section}{\numberline {A.2}Personal reflection and lessons learned}{36}{section.a.A.2}% 
\contentsline {section}{\numberline {A.3}Legal, social, ethical and professional issues}{37}{section.a.A.3}% 
\contentsline {subsection}{\numberline {A.3.1}Legal issues}{37}{subsection.a.A.3.1}% 
\contentsline {subsection}{\numberline {A.3.2}Social issues}{37}{subsection.a.A.3.2}% 
\contentsline {subsection}{\numberline {A.3.3}Ethical issues}{38}{subsection.a.A.3.3}% 
\contentsline {subsection}{\numberline {A.3.4}Professional issues}{38}{subsection.a.A.3.4}% 
\contentsline {chapter}{\numberline {B}External Material}{39}{appendix.a.B}% 
\contentsline {section}{\numberline {B.1}Gale-Shapely Algorithm}{39}{section.a.B.1}% 
\contentsline {section}{\numberline {B.2}External Libraries}{39}{section.a.B.2}% 
\contentsline {subsection}{\numberline {B.2.1}Back-End Libraries}{39}{subsection.a.B.2.1}% 
\contentsline {subsection}{\numberline {B.2.2}Front-End Libraries}{39}{subsection.a.B.2.2}% 
\contentsline {subsection}{\numberline {B.2.3}Algorithms Libraries}{40}{subsection.a.B.2.3}% 
\contentsline {chapter}{\numberline {C}URL's and Deployed Applications}{41}{appendix.a.C}% 
\contentsline {section}{\numberline {C.1}Source Code}{41}{section.a.C.1}% 
\contentsline {subsubsection}{Back-End Source Code:}{41}{subsubsection*.72}% 
\contentsline {subsubsection}{Front-End Source Code:}{41}{subsubsection*.73}% 
\contentsline {subsubsection}{Algorithms Source Code:}{41}{subsubsection*.74}% 
\contentsline {section}{\numberline {C.2}Soft-deployed Applications}{41}{section.a.C.2}% 
\contentsline {subsubsection}{Front-End Application:}{41}{subsubsection*.75}% 
\contentsline {subsubsection}{Back-End API Link:}{41}{subsubsection*.76}% 
\contentsline {subsubsection}{Algorithms API Link:}{41}{subsubsection*.77}% 
\contentsline {section}{\numberline {C.3}Questionnaire}{41}{section.a.C.3}% 
\contentsline {subsubsection}{Questionnaire Link:}{41}{subsubsection*.78}% 
\contentsline {subsubsection}{Test Account Details:}{41}{subsubsection*.79}% 
\contentsline {chapter}{\numberline {D}Example Solutions}{42}{appendix.a.D}% 
\contentsline {section}{\numberline {D.1}Example Modified Gale-Shapely Algorithm Walkthrough}{42}{section.a.D.1}% 
\contentsline {section}{\numberline {D.2}Combining Preferences}{44}{section.a.D.2}% 
\contentsline {chapter}{\numberline {E}Figures and Tables}{45}{appendix.a.E}% 
\contentsline {section}{\numberline {E.1}UML diagrams}{45}{section.a.E.1}% 
\contentsline {section}{\numberline {E.2}Software File Structure}{46}{section.a.E.2}% 
\contentsline {section}{\numberline {E.3}Back-end Application Routes}{48}{section.a.E.3}% 
\contentsline {section}{\numberline {E.4}Middleware Implementation}{50}{section.a.E.4}% 
\contentsline {section}{\numberline {E.5}Token Assignment}{51}{section.a.E.5}% 
\contentsline {section}{\numberline {E.6}Front-end views}{52}{section.a.E.6}% 
\contentsline {chapter}{\numberline {F}Project Information Sheet}{55}{appendix.a.F}% 
\contentsline {chapter}{\numberline {G}User Testing Quantitative Results}{58}{appendix.a.G}% 
