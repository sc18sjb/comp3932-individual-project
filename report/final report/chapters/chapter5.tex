\chapter{Implementing the Web Application}
\label{chapter3}

This chapter covers the implementation of the web application for the problem, including the system architecture, design choices and testing and validation of the final result.

\section{System Architecture}

\subsubsection{System Goals:} Section 2.1 described the problem in the face of a company with a number of employees and shifts. Each company will have 2 types of employees: \textit{managers} and \textit{regular} employees. Managers should be able to log into the application, manage employees, departments and shifts for other employees to be matched to. The term 'manage' here refers to creating, updating and deleting objects. Managers should also be able to initiate the matching algorithms, or re-match employees, say if something, such as ratings, changes. Managers are a subset of regular employees, that means they can also do everything an employee can. Regular employees should be able to log into the accounts a manager created for them, and view their matched shifts for a given week. They should then be able to \textit{rate} either individual shifts, or a whole rota (described in section 2.3.2). 

\forceindent \textit{Figure \ref{UCD}} shows a use case diagram to represent this statement.


\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.6\linewidth}
    \includegraphics[width=\linewidth]{images/use-case-diagram.png}
  \end{subfigure}
  
  \caption{UCD for the software application.}
  \label{UCD}
\end{figure}


\subsection{Designing a Database:} 
To produce a database model, we refine what was described in section 2.1:

\begin{itemize}
	\item A company has a number of operating days defining when it is open, being an integer where 1 is Monday and 7 is Sunday.
	\item A shift has a start and end time, minimum and maximum number of employees, and a department associated with it. For our application, a shift will also have a company operating day associated with it. 
	\item Furthermore, a shift will also have a date associated with it, such that the same shift on the same day can be repeated on multiple dates. We can call this a shift \textit{instance}. For example, a shift from 9am-5pm on Monday on one week, and another shift on Monday with the same time on another week are the \textbf{same} shift associated with different dates. This is important for the CF techniques described in section 2.3. Each date having a unique shift would not work, as ratings would be non-effective if the shift never appeared again. 
	\item A department must have a unique name for a given company.
	\item An availability is a list of pre-defined working ranges (see section 2.2).	
	\item An employee has a name, email and password to allow them to login to the system. Each employee might work in many departments and have many availabilities, and each department and availability will have many employees. An employee must have a role defining if they are a \textit{manager} or \textit{regular} employee.
	\item An employee can be matched to many shift instances, creating a pair of (employee, shift-instance) matchings. Note the use of the shift instance and not the shift, so the employee knows the date they are working.
	\item An employee can \textit{rate} their matched shifts, creating an \{employee, shift, rating\} triple. The shift here is not the shift-instance, so that ratings are carried across dates. Each shift rating also has a provider, being either the \textit{system} or \textit{employee}, to distinguish if the rating came from CF or the employee.
	\item A set of weekly shifts for a given company is called a \textit{rota}. Each rota has a unique name and the range of days included within the rota.
\end{itemize}

Taking all these considerations into account we can model and entity relationship diagram (ERD) for the implementation of the database. \textit{Appendix \ref{uml-diagrams}}, \textit{Figure \ref{ERD}} shows this final ERD. It has been ensured that the database follows third normal-form schema design approach as described by William \cite{kent1983simple}.

\subsection{Software Structure}

There are 3 components to the system: 

\begin{enumerate}
	\item The front-end application, showing the shifts, employees, departments and matchings. Data is collected here through forms.
	\item The back-end application, providing a link between the front-end, where the data is collected, and the database, where the data is sent to and from. Any processing of such data also happens here.
	\item The algorithms discussed in \textit{Chapter 2} will be implemented here, sending data to and from the back-end.
\end{enumerate}

Both components 2) and 3) will implement their own application programming interface (API) to allow them to speak to one another, and allowing the front-end to send data between them. For example, when a manager wants to assign matchings it will call a route on the back-end API to fetch all the data from the database the algorithms need to create matchings (shifts, employees, preferences etc.). The back-end then calls the algorithm's API which creates the matchings and returns the resulting pairs to the back-end API, which inserts them into the database. The back-end API then either returns a 'success' or 'error' message if the whole process worked or if any part failed. 

\forceindent This approach allows for easy extensibility and maintainability of the code. In fact, the whole back-end or algorithms components could be completely re-implemented without each other component knowing, and without having to change a single line of code at the other components, as long as the API routes remain the same name.

\subsubsection{File Structure:} \textit{Appendix \ref{section:software-file-structure}}, \textit{Figure \ref{fig:file-structure}} illustrates a simplified version of the file structure used for the application, including the different components 1), 2) and 3) split under different directories. Each directory can be split further into sub-directories for each component, that are representing in \textit{Figures \ref{fig:laravel-file-structure}, \ref{fig:react-file-structure}} and \textit{\ref{fig:algorithms-file-structure}} respectively, with descriptions.


\section{Implementation}

\subsection{Version Control}

When implementing the web application, and during the write-up of this report, version control was managed through Git, with all the files being hosted on GitLab, and commit history illustrated in \textit{Figure \ref{git-commit}}. Version control was used to allow for easy and systematic rollbacks, such as when something went wrong (i.e. a test case failed).

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.8\linewidth}
    \includegraphics[width=\linewidth]{images/commits.png}
  \end{subfigure}
  
  \caption{Git commit history during the project timeline.}
  \label{git-commit}
\end{figure}

\textit{Appendix \ref{section:source-code}} provides links to the Git repository for this project, including the source code for components 1), 2) and 3) described in section 3.1.2.

\subsection{Back-end Implementation}

\subsubsection{Languages and Libraries Used:} PHP v7.4.0 in conjunction with Laravel v8.65 was used to implement the back-end of the application and its API. Apache, provided through WAMP (Windows, Apache, MySQL, PHP) was used to run the back-end application on a local server on the implemented PC, as well as the corresponding MySQL database. MySQL Workbench was used as the database management system (DBMS) to allow for table inspection and easy SQL query execution on the database (such as to check for specific data).

\forceindent PHP and Laravel were chosen due to the author's prior experience, thereby facilitating more time to focus on algorithmic design. Laravel also provides a full suite of helpful tools that makes it easier, and quicker, to implement a web application, that will be discussed throughout this section.

%Laravel provides a full suite of tools that makes it easy to implement a web %application, including the Eloquent ORM (Object-relational mapping) and %database migrations that allows relational databases to be quickly defined, %web and API routing, query builder to get, insert, update and delete items %from the database easily, and the Artisan console that provides tools for %managing the web application. 

\subsubsection{Code Structure:} PHP provides an object-oriental programming (OOP) approach through the use of \textbf{namespaces}. A class can be defined in its own namespace, or in a shared namespace with other classes, which can then be imported (to allow it to be used by other classes) into other, different namespaces. Laravel follows this convention and implements a clean, extensible solution to code and project structure. 


\textit{Appendix \ref{section:software-file-structure}}, \textit{Figure \ref{fig:laravel-file-structure}} illustrates a simplified version of the Laravel file structure. We will see throughout this section that each directory is segregated to hold differentiable code to perform different tasks.  

\subsubsection{A REST API:} A REST (representational state transfer) API is an architecture style that defines a number of constraints on a RESTful web service, as described by Li and Chou \cite{li2011design}. Most importantly, a REST API is limited to GET, POST, PUT, PATCH and DELETE requests. For the purpose of our application, we can omit the use of PATCH requests. In our application, we have a number of routes that are URI's to point to an endpoint. Each route has a specific request type associated with it such that only those types of requests are allowed to that route. For example, a route to get all employees might be GET '/employees', and a route to update a specific employee might be PUT '/employee/<id>', where <id> is the ID of the employee to update. POST and PUT requests accept a request body, which is the data to be sent to the server. 

\forceindent \textit{Appendix \ref{back-end-routes}} shows the admin, employee and public routes respectively. Admin routes represent the manager actor defined in \textit{Figure \ref{UCD}}, employee routes the employee actor and public routes the user actor. In addition, employees will be able to perform all GET requests on the system. In Laravel, each route links to a \textit{controller}, as the endpoint of that route. A controller contains all the processing of any data sent to and from the route, including any database queries and validation. Laravel \textbf{Eloquent} is an object relational mapper (ORM) \cite{keith2009object} that was used to implement all queries to the database. Each entity in the database has an associated model class with attributes being the fields of the entity. We access these models in the controllers to communicate with the database.

\subsubsection{Distinguishing Users:} We want to distinguish between the different user actors shown in \textit{Figure \ref{UCD}}. In Laravel, we can implement custom \textit{middleware}, that is essentially a check performed on HTTP requests, before they are sent down the correct route. The employee role field in the databased (described in section 3.1.1) can be used to implement this check. We can take a list of roles a route is permitted to take and check if the employee has any of these roles. If they do, they are permitted to enter the route, otherwise they are forbidden. The implementation for this middleware, along with the role-check are shown in \textit{Appendix \ref{middleware-implementation}}. Middleware 'groups' are a collection of routes each role can go through, \textit{Appendix D.4}, \textit{Figures \ref{admin-routes}, \ref{employee-routes}} show these groups for admin only and employee/admin routes. \textit{Figure \ref{public-routes}} does not have any middleware check, since they are public routes (everyone can access them). 

\subsubsection{Securing the Application:} Only logged-in employees should be able to access non-public routes. Since we are developing an API, we can achieve this using \textit{tokens}. Laravel offers the \textbf{Sanctum} package that provides simple token-based API's. When an employee logs into the system, we issue a token to them that is stored in the database for a duration (24 hours). These tokens are hashed with SHA-256, essentially making them uncrackable, as described by Gilbert and Handschuh \cite{gilbert2003security}. \textit{Appendix \ref{token-assignment}}, \textit{Figure \ref{sanctum-token}} shows the implementation for this token assignment. As long as the token exists in the database, and as long as the same token is sent as the 'bearer-token' in the request body, then an employee will be authenticated, otherwise forbidden (i.e. if the tokens do not match). We can again use Laravel middleware to group routes under the Sanctum authentication. All of the employee and admin routes shown in \textit{Appendix D.3}, \textit{Figures \ref{admin-routes}, \ref{employee-routes}} are grouped under this middleware.    


\subsection{Front-end (client) Implementation}

\subsubsection{Languages and Libraries Used:} JavaScript (JS) v1.7 was used in combination with the React framework v17.0.2 to build the front-end application. Axios v0.21.4 can be used when making requests to the back-end API. React was chosen as the most popular web framework for making single page applications (SPA)'s, that would be suitable for this project, since we will be making a high number of requests to our back-end API. Having to refresh the page on each request would lead to a disjointed experience, instead we can dynamically alter content in real-time.

\subsubsection{Code Structure:} React offers a component-based architecture. Each component is its own function that returns something to be rendered to the screen, this might be other components or simply HTML elements. React offers class-based or functional-based component objects, the difference being to replace state methods with hooks that check for state changes. A state is a variable that, when changed, causes a re-render of the content. We choose to use functional components as the author has previous experience using them, and it allows for simpler component definitions. 

\forceindent \textit{Appendix \ref{section:software-file-structure}}, \textit{Figure \ref{fig:react-file-structure}} shows the file structure for the front-end application. We have a different JS file for each component, allowing better modularity of the code. Each 'page' imports these components for use in dynamic rendering.


\subsubsection{Implemented Views:} We implement views (different pages the employee can access) to facilitate all the functionality described in \textit{Figure \ref{UCD}}. This consists of 4 views:

\begin{enumerate}
	\item \textbf{Manager Dashboard:} Shows a table that iterates through each of the companies' weekly operating days as the columns, and displays each hour of the day as the rows. A shift spans multiple cells in a specific column to represent the duration. This is illustrated in \textit{Appendix \ref{front-end-views}}, \textit{Figure \ref{manager-dashboard}}. Selecting a cell of the hour and day a shift starts, allows managers to create new shifts. Likewise, selecting an existing, unmatched shift, allows it to be updated or deleted. We do not want matched shifts to be updated or deleted, since this would make the current matching (based on preferences and ratings as described in chapter 2) obsolete.

\forceindent The manager dashboard affords a top-bar where they can save all the shifts in a week as a \textit{rota}, duplicate existing rotas or clear a rota. Most importantly, the manager can manage matchings. In a single click of a button the application will asynchronously match employees to shifts and update the dashboard, using the techniques described in section 3.1.2. Matchings can also be removed or updated by re-matching the shifts.
	
	\item \textbf{Employee Dashboard:} This is an alternate dashboard that has the employees in the company as the rows instead of the hours. Employees are ordered such that the active employee is always the 1st row. Only shifts matched to an employee are shown in each cell and clicking on said shifts shows its matchings and details. This is illustrated in \textit{Appendix \ref{front-end-views}}, \textit{Figure \ref{employee-dashboard}}. As described in section 2.3.2, clicking on a shift matched to the active employee also provides a rating 'faces' option, to rate the shift on a scale of 1-5. Likewise, when fetching the shifts on load, if there are any shifts matched to the employee found to have a rating of 0, a rota rating pop-up will show to rate all un-rated shifts together. These two options are illustrated in \textit{Appendix \ref{front-end-views}}, \textit{Figure \ref{employee-dashboard-faces}, \ref{employee-dashboard-shift-rating}}.
	
	\item \textbf{Employee Management Dashboard:} Will contain a table of each employee in the company, including their name, email, departments and availability preferences (in order), illustrated in \textit{Appendix \ref{front-end-views}}, \textit{Figure \ref{employee-management-dashboard}}. Managers can create, update or delete existing employees. Deleting an employee will remove their matchings from shifts. Likewise, updating an employee to work in a department different to their current shift matchings, will remove any matchings for the old department they no longer work in.
	
	\item \textbf{Department Management Dashboard:} Comparably, this view will contain a table of each department in the company, its name and assigned colour (for differentiation in the main dashboard), shown in \textit{Appendix \ref{front-end-views}}, \textit{Figure \ref{department-management-dashboard}}. Managers can create, update or delete departments. Deleting a department will remove all the shifts associated to that department, and un-match any employee matchings to that shift.
\end{enumerate}

Managers will be able to access all views 1), 2), 3), 4), whereas regular employees will only be able to access view 2). Managers can switch between the two views 1) and 2) through a toggle  in the top-bar. Different views can be accessed through a navigation bar at the side of the page.

\subsubsection{Distinguishing Employees:} Conditional rendering can be used to restrict regular employee's access to views 1), 3) and 4). On first load of the client application, we make a request to the back-end API for the active user's details, including their role. This is stored in a global state, along with a role check function, such that every component can access it. Then, when we render, we can check the role of the active employee and conditionally show specific components based upon this role. React offers a \textbf{Router} package to give the 'illusion' of different pages (given this is an SPA) and is how we implement the different views. We can also conditionally render this router, such that views 1), 3) and 4) only render if the employee's role is manager. If an employee manages to bypass the conditions (being on the client), they will still be unauthorized for routes by the checks performed at the back-end API, descibed in section 3.2.2.


%\subsubsection{Making Requests:} JS offers the \textbf{Axios} package that was used to make any requests to the back-end API. JS is asynchronous that means these requests happen at the same time as one another, which speeds up rendering on the clients browser. An \textit{api.js} file was created that holds all of the axios requests we make as an object of functions. We can then simply call these functions in our components every time we need data from the back-end API's. Sometimes, we want \textit{blocking} calls to wait for the request to return before continuing (such as if we need the data in another, subsequent request). JS offers the async/await syntax to facilitate this. \textit{Appendix ??}, \textit{Figure ??} shows default GET, POST, PUT and DELETE requests made with blocking calls to the axios package in \textit{api.js}, along with error handling if the request does not succeed.



\subsection{Algorithm's Implementation}

\subsubsection{Languages and Libraries Used:} Python v3.9 and Numpy v1.22.3 was used to implement the algorithms described in chapter 2. FastApi v0.75 was used to implement the API to allow the back-end to communicate with such algorithms. Familiarity was the main deciding factor behind using Python, and Numpy offers an extensive suite of mathematical operations as described by Oliphant \cite{oliphant2006guide}. FastApi provides the fastest Python web server package, making requests to create matchings marginally quicker.

%\subsubsection{Code Structure:} All code was structured in an OOP approach, %following PEP8 \cite{van2001pep} standards where applicable.


\subsubsection{Modified Gale Shapely:} An implementation of \textit{Algorithm \ref{alg:modifiedgaleshapely}} and \textit{\ref{alg:canworkshift}} was written in an OOP approach. Availability, department, employee and shift model classes were written, conforming to the relationships in \textit{Appendix D.1}, \textit{Figure \ref{ERD}}. In addition, shifts will maintain a list of the employees working them and employees will maintain a list of the shifts they are matched to. Each time a new pair is created, these lists are updated. This is important for \textit{Algorithm \ref{alg:canworkshift}} where we need to check which shifts employees are already matched to. \textit{Appendix \ref{uml-diagrams}}, \textit{Figure \ref{uml-class-diagram}} shows a class diagram for these models.

\forceindent \textit{Algorithm \ref{alg:modifiedgaleshapely}} runs iteratively for each operating day of the week. Preferences are set for the  employees and shifts for each of these operating days as a map of days and shifts / employees. Preferences are calculated using \textit{Algorithm \ref{alg:combiningratings}} for a given day. \textit{Figure \ref{fig:days-shift-preferences}} represents the employee's preferences for some given days where 1 is Monday and 7 is Sunday. When \textit{Algorithm \ref{alg:modifiedgaleshapely}} runs, it takes the preferences with the \textit{key} being the current day. Likewise, on each iteration the algorithm creates employee-shift matchings for the current day. \textit{Figure \ref{fig:days-shift-matchings}} show these matchings for employee-ID, shift-ID pairs. It is this data-structure that is returned to the back-end API and inserted into the employee-shifts entity.

\forceindent We have a requirement 2) defined in section 2.2.1 that employees can work multiple shifts in one day, therefore further iterations of \textit{Algorithm \ref{alg:modifiedgaleshapely}} are needed, and we concatenate new pairs to the existing pairs of the given day. We iterate until there are no pairs left to find in a given day, at which point we move onto the next day. At the end of each iteration, we restore all shift preferences, since we are popping them throughout the algorithm (and they are needed for subsequent iterations). We make a python deep copy of the preferences set which is then copied over to achieve this.

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  
  \begin{hashtable}
   1 &  Shift 1 $\rightarrow$ Shift 2 \\
   2 &  Shift 3 \\
   5 &  Shift 4 $\rightarrow$ Shift 5 $\rightarrow$ Shift 6 \\
   7 &  Shift 7 $\rightarrow$ Shift 8 \\
  \end{hashtable}
  
  \caption{Employee shift preferences}
  \label{fig:days-shift-preferences}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  
  \begin{hashtable}
   1 &  (1, 5) $\rightarrow$ (3, 4) $\rightarrow$ (4, 3) \\
   2 &  (2, 12) \\
   5 &  (3, 11) $\rightarrow$ (3, 10) $\rightarrow$ (1, 2) \\
   7 &  (4, 7) $\rightarrow$ (3, 13) \\
  \end{hashtable}  
  
  \caption{Employee-shift matchings}
  \label{fig:days-shift-matchings}
\end{subfigure}
\caption{Storing preferences and matchings for some operating days of the week}
\label{fig:storing-days}
\end{figure}


\subsubsection{Collaborative Filtering (CF):} With the ratings gathered from the front-end application, view 2), discussed in section 3.2.3, we implement user-user and item-item CF for both algorithms cosine similarity, and adjusted cosine similarity (discussed in section 2.3.3). Numpy can be used to perform these calculations: the package offers native support for matrix and vector manipulations.

\begin{figure}[h!]
\begin{lstlisting}[language=Python]
def cosineSim(x, y):
    xy = np.sum(np.multiply(x, y))
    normx = la.norm(x)
    normy = la.norm(y)
    return xy / (normx * normy) if (normx * normy) > 0 else 0

def weightedAverage(ratings, predictions):
    xy = np.sum(np.multiply(ratings, predictions))
    return xy / np.sum(predictions) if np.sum(predictions) > 0 else 0	
\end{lstlisting}
\caption{Cosine similarity and weighted sum algorithms implementation}
\label{fig:cosine-weighted}
\end{figure}

\textit{Figure \ref{fig:cosine-weighted}} implements the cosine similarity and weighted average algorithms. For adjusted cosine similarity, we take the employees / shifts average ratings \cite{adjusted-cosine-similarity}, as demonstrated in \textit{Figure \ref{fig:adjusted-cosine}}. These calculations are performed for each shift or employee in the matrix, with the final weighted average calculation returning the prediction. 

\forceindent Since the back-end API will return a matrix with a number of unknown ratings in it, we perform the prediction calculations iteratively for all unknowns in the matrix. This builds a data structure for the row, column (being shifts, employees or visa versa) of the unknown, and the prediction that is returned to the back-end API and inserted into the employee-shifts entity. This calculation can be performed beforehand, each time the matching algorithm is called, leading to two subsequent calls by the back-end API: first to the CF algorithms, then to the matching algorithms.

\begin{figure}[h!]
\begin{lstlisting}[language=Python]
mean_col = (sum(sum(x, y)) / y.size) if y.size > 0 else 0
cosineSim(x - mean_col, y - mean_col))
\end{lstlisting}

\caption{Adjusted cosine similarity taking averages}
\label{fig:adjusted-cosine}
\end{figure} 

\subsubsection{A FastApi:} We have 2 routes for our algorithms API, one for the the matching algorithm, and one for the CF algorithms. FastApi uses the Python Typing package to denote what data types are permitted to each route. Our matching route expects a request body containing a list of availability, departments, employees, shifts and shift ratings. Each can be retrieved from the back-end API, following the types in the entities described in \textit{Appendix \ref{uml-diagrams}}, \textit{Figure, \ref{ERD}}. Likewise, the CF route only expects a request body containing shift ratings that it will use to build a matrix from (using the shift and employee id's and ratings).

%\subsubsection{Unit Testing:} The Kaggle Employee Schedule dataset [reference] provided test data to check each component of the modified gale shapely algorithm, including the constraints and matchings. Example matrices, provided in the COMP3771 User Adaptive Intelligent Systems [reference] module, were used to test user-user and item-item CF for both adjusted and regular cosine similarity. 10 tests were run in total, each test passed and the results can be seen in \textit{Appendix ??}, \textit{Figure ??}.

