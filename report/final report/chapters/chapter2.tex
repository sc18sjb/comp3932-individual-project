

\chapter{Designing a Solution}
\label{chapter2}

This chapter will cover the design of any algorithms used in solving the problem, including the different methods and approaches for deriving such algorithms.

\section{Modelling the problem}
%Before we can begin to solve the problem, we first need to define the model and the variables that will be needed.
We can think of solving the problem for a company $C$ with a number of $m$ employees $E$, $n$ shifts $S$, and $j$ departments $D$. Each company will also have a number of operating days $O \subseteq \{Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday\}$. A shift $s \in S$ has the following attributes: 

\begin{itemize}
	\item Let $s_{start}$ be the start time of the shift.
	\item Let $s_{end}$ be the end time of the shift, where $S_{end} > s_{start}$.
	\item Let $s_{min}$ be the minimum number of employees to work the shift, where $s_{min} \geq 0$ .
	\item Let $s_{max}$ be the maximum number of employees to work the shift, where $ m \geq s_{max} \geq s_{min} \geq 0$.
	\item Let $s_{dep} \in D$ be the department the shift is in. 
\end{itemize}

An employee $e \in E$ has a set of departments $e_{dep} \subseteq D$ they can work in. Both employees and shifts have a set of preferences for the other type: $s_{employee} \subseteq E$ and $e_{shift} \subseteq S$, as well as a set of matchings to define which employee is working which shift (and visa versa): $s_{memployee} \subseteq E$ and $e_{mshift} \subseteq S$.

\section{Creating a Stable Matching}
The stable marriage problem (see section 1.3.1) can be directly applied to the shift scheduling problem, exchanging the sets of woman and men for sets of employees, and shifts that these employees might want to work. We still want to create a stable matching so that the no employee-shift matching would prefer to be matched to a different employee / shift \cite{stable-marriage}.

\forceindent The high likelihood that there will be more shifts than employees $n > m$, that means we will have incomplete lists of preferences, needs to be considered. How can we ensure a stable matching when Gale and Shapely \cite{gale-shapely} only proved a solution for equal sets? Furthermore, how do we represent these preferences? An employee might have a preference for a shift, but how can we model a shift's employee preference? An exploration into these problems, presenting possible solutions, will be discussed throughout this chapter. 

\subsubsection{Modelling Preferences:} The preferences are two-fold. We can model employee preferences for shifts simply as the set of shifts they prefer the most. Explicitly, gathering this information is cumbersome and not user-friendly, since it would require the employee to constantly be updating their set of preferences in line with new shifts added, and updated. A dynamic, implicit approach is required. We can solve this by introducing a new constant to represent availability $A = (morning, afternoon, evening, night)$ where, for a given availability $a \in A$:

\begin{itemize}
	\item $morning$ shifts are: $a_{start} = 06am$, $a_{end} = 12pm$.
	\item $afternoon$ shifts are: $a_{start} = 12pm$, $a_{end} = 5pm$.
	\item $evening$ shifts are: $a_{start} = 5pm$, $a_{end} = 8pm$.
	\item $night$ shifts are: $a_{start} = 8pm$, $a_{end} = 6am$.	
\end{itemize}

Each employee can then present their set of availability $\forall e \in E$  $e_{available} \subseteq A$ they prefer to work. When solving the stable marriage problem for this new set of preferences, we look at the shifts being matched. If a shift $s$ start time falls in-between an availability preference, $a_{start} \geq s_{start} < a_{end}$, then $s$ is added to $e_{shift}$, otherwise the shift is ignored. Shifts added to $e_{shift}$ are ordered, such that an employee who prefers working $morning$, then $afternoon$ will have a set of shifts with morning shifts first, then afternoon shifts. All other shifts (evening, night) will be ignored. This solution also allows employees to specify when they \textit{do not} want to work during the day.

\forceindent Secondly, to model shift preferences for employees we use the department $s_{dep}$ a shift is assigned to. Two shifts with the same $s_{start}$ and $s_{end}$ but different $s_{dep}$ are distinct. A shift's set of employee preferences, $s_{employee}$, is then the employees where $\exists e \in s_{employee}$ $s_{dep} \in e_{dep}$; the employees who work in the shift's department. For now, this is taken in order of employee (where the first employee was the first employee in the company and the last employee was the most recent recruitment), but we will see in section 2.3.1 how we can combine CF techniques to refine this set.


\subsubsection{Choosing a Proposer:} Using the Gale-Shapely algorithm described in section 1.3.1, we need to decide who should be the proposer; who will get the most preferable matchings? For the purpose of our application we will choose this to be the shifts $S$, to ensure the most suitable employee is working a shift. Section 2.3.1 will discuss techniques for \textit{rating} shifts that will alter the preference set $s_{employee}$. Shifts will pick the most desirable employee and, with luck, the employee who prefers that shift the most. Therefore, the proposer has to be the shifts $S$ since an employee's preferences, $e_{available}$ used to calculate $e_{shift}$, is unlikely to change, resulting in the same, or similar matchings. 


\subsection{Additional Constraints} 
We can directly apply Manlove \textit{et al.} \cite{constraint-satisfaction-SM} findings to the problem to account for the additional constraints introduced. We have already defined the following variables $X = \{S, E, D, A, O\}$. The domain for $S, E, D$ is the shifts, employees and departments of the company. The domain for $A$ is $morning, afternoon, evening, night$. The domain for $O$ is $Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday$. We want to model the following constraints, in order to comply with the working practices guidelines \cite{weekly-working-hours, rest-breaks}:

\begin{enumerate}
	\item A shift $s \in S$ must have between $s_{min}$ and $s_{max}$ employees working it, where $0 \geq s_{min} \geq s_{max} \geq m$.
	\item An employee $e \in E$ can work multiple shifts in one day, such that $|e_{mshift}| \in \mathbb{N}$ for a given day $o \in O$. Working hours are limited to 8 hours per day, for each of these days, such that $\sum_{k=1}^{|e_{mshift}|} s_{k_{end}} - s_{k_{start}} <= 8$ where $s_k \in e_{mshift}$.
	\item An employee $e \in E$ cannot work more than 48 hours in any given week, such that $\sum_{o=1}^{|O|} \sum_{k=1}^{|e_{mshift}|} s_{ok_{end}} - s_{ok_{start}} <= 48$ where $s_{ok} \in e_{mshift}$ for a given day $o \in O$. 
	\item An employee $e \in E$ must have an 11 hour gap between the end of their last shift on one day, and the start of their first shift the next day. Such that $s_{o1_{start}} - s_{o-1k_{end}} \geq 11$ for a given day $o \in O$ where $k = |e_{mshift}|$ for the given day $o$.
\end{enumerate}

Miguel \cite{flexible-constraint-satisfaction} describes two types of CSP: flexible and dynamic. Our approach is indeed flexible since not all the constraints are hard; it is still possible to match an employee to a shift without the minimum being filled (say $min > 1$), and there may be a range of employees who can be matched to a shift.

\subsection{Modified Gale Shapely}
The stable marriage problem was solved by Gale and Shapely \cite{gale-shapely} for an \textit{equal} number of men and women, presented in \textit{Appendix \ref{section:gale-shapely-algorithm-pseudocode}}. We need to present a solution that solves the problem with an \textit{unequal} number of men and women, and adheres to the constraints defined in section 2.2.1. 

\forceindent \textit{Algorithm \ref{alg:modifiedgaleshapely}} presents a modified version of the Gale-Shapely algorithm. The algorithm runs until there are no shifts left in $S$; either all the shifts are matched, or it is impossible to match a shift. Shifts remain in $S$ until their minimum has been filled. On line 5 we check if a shift has any employee preferences left, since these are popped from the set. Shifts that have exhausted all of their preferences without a matching are considered \textit{unmatchable}. This might happen when, for example, at line 8 an employee does not have the shift in their preference, at line 13 when the employee prefers their previous shift, or at lines 23 and 30 when the employee cannot work the shift.

\forceindent For each unmatched shifts, the algorithm matches the shift to the first employee in their preference list at line 26. If the employee is already matched to another shift, we check if the employee prefers the new shift and swap at line 19. If the employees prefers the new shift, but the shift has a minimum of 0 and their current shift has a minimum > 0 then we do not swap, since the shift can essentially be 'skipped' at line 13. We prioritise shifts that need to be filled as per constraint 1 in the CSP model. Conversely, if the employee prefers their current shift, but this current shift has a minimum of 0 and the new shift has a minimum $> 0$ then we do swap, because the new shift needs to be filled whilst the current shift (even though it has a higher preference) doesn't. The inverse of this statement is presented at lines 12 and 13. At lines 22 and 29 we check if the shift has been filled. If it hasn't, the shift is re-added so that it may be matched again.

\clearpage
\begin{algorithm}
\caption{Modified Gale-Shapely}
\label{alg:modifiedgaleshapely}
\begin{algorithmic}[1]

\Procedure{ModifiedGaleShapely}{S, E}
\State Initialise $s \in S$ and $e \in E$ to \textit{free}
\While{$|s| \neq \emptyset $} \Comment{There are unmatched shifts}
\State $s \gets S.pop()$ 

\If{$|s_{employee}| > 0$ \textbf{and} $|s_{memployee}| < s_{max}$} $e \gets s_{employee}.pop()$
\Else {} \textbf{Continue to next iteration} \Comment{Impossible to match shift}
\EndIf

\If{$s \notin e_{shift}$}
\State $S \gets S.push(s)$
\State  \textbf{Continue to next iteration}
\EndIf

\If{$\exists$ some matching $(e, s')$} \Comment{The employee is already in a matching}

\If {($e$ prefers $s'$ to $s$ \textbf{and} $s'_{min} > 0$) \textbf{or} \\    \hspace{58pt} ($e$ prefers $s'$ to $s$ \textbf{and} $s_{min} \leq 0$  \textbf{and} $s'_{min} \leq 0$) \textbf{or} \\ \hspace{58pt} ($e$ prefers $s$ to $s'$ \textbf{and} $s_{min} \leq 0$ \textbf{and} $s'_{min} > 0$)} $S \gets S.push(s)$
\Else
\If {$\textsc{CanWorkShift}(e, s)$}
\State $S \gets S.push(s')$
\State $e_{mshift} \gets e_{mshift} \setminus s'$
\State $s'_{memployee} \gets s'_{memployee} \setminus e$
\State $(e, s)$ are matched
\State $e_{mshift} \gets e_{mshift}.push(s)$
\State $s_{memployee} \gets s_{memployee}.push(e)$

\If {$|s_{memployee}| < s_{min}$} $S \gets S.push(s)$
\EndIf
\Else {} $S \gets S.push(s)$
\EndIf

\EndIf

\Else \Comment{The employee is not yet matched}

\If{$\textsc{CanWorkShift}(e, s)$}
\State $(e, s)$ are matched
\State $e_{mshift} \gets e_{mshift}.push(s)$
\State $s_{memployee} \gets s_{memployee}.push(e)$

\If {$|s_{memployee}| < s_{min}$} $S \gets S.push(s)$
\EndIf
\Else {} $S \gets S.push(s)$
\EndIf

\EndIf

\EndWhile
\EndProcedure
\end{algorithmic}
\end{algorithm}

\begin{algorithm}
\caption{Check if an employee can work a given shift}
\label{alg:canworkshift}
\begin{algorithmic}[1]
\Procedure{CanWorkShift}{e, s}
\If {$\{s\} \cap e_{mshift}$} \Return false \Comment{Already working another shift at the same time}
\EndIf

\State $ps \gets $ The last shift from the day before in $e_{mshift}$

\If {$s_{start} - ps_{end} < 11$} \Return false \Comment{Break isn't 11 hours}
\EndIf

\State $ch \gets$ The sum length of shifts worked on the current day

\If {$ch + (s_{end} - s_{min}) > 8$} \Return false \Comment{Already working 8 hours}
\EndIf
\State $ah \gets$ The sum length of all shifts worked that week
\If {$ah + {s_{end} - s_{min} > 48}$} \Return false \Comment{Already working 48 hours the week}
\EndIf
\EndProcedure
\end{algorithmic}
\end{algorithm}


Constraints 2, 3 and 4 are solved by \textit{Algorithm \ref{alg:canworkshift}} at lines 4, 6 and 8 respectively. Line 2 checks for another, expected constraint, that no employee can work two shifts at the same time. If \textit{Algorithm \ref{alg:canworkshift}} returns true the employee can work the shift, otherwise they cannot.

\forceindent Both algorithms run for each operating day of the week $o \in O$ to present a final \textit{rota} of all the shifts each employee must work. The algorithms allow a stable matching for an equal set of shifts and employees (men and women), as per the original Gale Shapely algorithm \cite{gale-shapely}, whilst also presenting a solution for unequal sets.

\forceindent A full walk-through of the algorithms presented in this section, over an example problem, is described in  \textit{Appendix \ref{section:example-modified-gale-shapely-algorithm-walkthrough}}.

\section{Creating a recommendation engine}

Let us introduce a new attribute $s_{rating}$, $\forall s \in E_{mshift}$ as the rating for each of the employees matched shifts. Any newly matched shift will have an $s_{rating} = 0$ since we do not know if the employee likes, or dislikes the shift yet. We can use CF techniques to predict these ratings, the likes of which will be discussed in this section.

\subsubsection{Modelling the ratings:} With the employees $E$ along the x-axis and the shifts $S$ along the y-axis, ratings can be modelled in matrix form with each cell representing the $s_{rating} \in e_{mshift}$ for an employee $e \in E$. 

\begin{figure}[h]
\centering
\begin{tabular}{c|cccc}
\hline
& u1 & u2 & u3 & u4\\
\hline
s1 & 1 & 0 & 3 & 0 \\
s2 & 0 & 1 & 0 & 1 \\
s3 & 5 & 0 & 0 & 3 \\
s4 & 0 & 5 & 2 & 0 \\
\end{tabular}
\caption{An example employee-shift matrix}
\label{cf-matrix}
\end{figure}

\textit{Figure \ref{cf-matrix}} shows a typical example of how such a matrix can be represented. 0 is unknown, 1 is dislikes and 5 is highly likes.

\subsection{Combining ratings}
In section 2.2, we looked at dynamic approaches to modelling the set of preferences for the employees and shifts. Very little needs to be done by the employee, and nothing by the shifts (since they are abstract), to gather a set of preferences. The caveat though, is that unless the employee changes their set of availability, the preferences will be very similar, if not the same, each time the algorithm is called. One way to solve this problem is to combine the preferences with the ratings predicted by CF techniques to give a new set of preferences. This has the effect that, when we start predicting ratings, we can alter the set of preferences with these new predictions such that when the modified Gale-Shapely algorithm (presented in section 2.2.2) is run, we get a different set of matchings more alike the employees actual preference.

\forceindent We can scale the rating $s_{rating}$ by the weight of the shift, where this weight is the probability $P(s)$ of the employee preferring the shift. It is simple to calculate $P(s)$ from the index of the shift in the employees preference. If it is the first shift in the preference we have a 100\% certainty they will like the shift. For all subsequent shifts we will have a $\{100 - (100 / |e_{shift}|) * n - 1\}$\% chance of them liking the shift, where $n$ is the shift index in $e_{shift}$. If a shift has a $s_{rating} = 0$ we instead take the max rating possible (such as \textit{Figure \ref{cf-matrix}} is 5), divided by the index $n$. \textit{Appendix \ref{section:combining-preferences}} presents an example to visually explain how to combine preferences.

\forceindent Now we have a basis for calculating dynamic preferences, \textit{Algorithm \ref{alg:combiningratings}} presents a solution for implementing employee preferences $e_{shift}$, incorporating what we discussed in section 2.2, combined with ratings. Lines 3-6 calculate the preferences based on availability, and lines 7-12 combine the ratings for a given shift $s \in S$.

\begin{algorithm}
\caption{Dynamic approach to gathering preferences}
\label{alg:combiningratings}
\begin{algorithmic}[1]

\Procedure{SetPreferences}{e, S}

\State Initialise $P$ and $P'$ to an empty set \Comment{Preferences and sorted preference lists}

\For{$\forall a \in e_{available}$}
\For{$\forall s \in S$}
\If{$s_{dep} \subseteq e_{dep}$ \textbf{and} $a_{start} \geq s_{start} < a_{end}$}

\State $P \gets P.push((s, s_{rating}[e]))$ \Comment{Preferences based on availability}
\EndIf

\EndFor
\EndFor

\For{$i = 1$ to $|P|$}
\State $P(s) \gets 100 - (100 / |P|) * i - 1$
\If {$P[1] > 0$} $P' \gets P'.push((P[0], 5 / i))$
\Else {} $P' \gets P'.push((P[0], P[0] * P(s)))$
\EndIf
\EndFor

\State $P \gets P'$ sorted by $P'[1]$ descending \Comment{Preferences based on availability and ratings}

\State \Return $P$

\EndProcedure
\end{algorithmic}
\end{algorithm}

\subsection{Collecting Ratings}
\textit{Figure \ref{cf-matrix}} shows a non-sparce matrix. That is, we have some existing ratings before we can begin making predictions. The \textit{cold start} \cite{lika2014facing} problem is a common issue in recommender systems; how do we know information about the user $U$ when they have just joined? There are a number of implicit and explicit collection techniques that can be deployed in order to gather these ratings.

\subsubsection{Rating a Rota / Shift:} We can ask employees to rate rotas produced by the modified Gale-Shapely algorithm discussed in section 2.2.2. This rating can then be distributed across all the shifts they are matched to, possibly by assigning the same rating as the rota to all matched shifts. Likewise, we can just ask employees to rate individual shifts, allowing for better differentiation in the ratings, thus better predictions. 

\subsubsection{Inferring Ratings:} Nichols \cite{implicit-collection-techniques} describes 13 techniques for implicit information gathering. One of these techniques relates to a user purchasing an item, with the idea being that purchase patterns inform marketing activities that can tell us which items the user prefers (hence ratings). We can adapt these purchases as matchings. If an employee is matched to the same shift a number of times we can infer that they like this shift, otherwise they would have rated it poorly to alter their preferences (as described in 2.3.1). 

\subsection{Algorithms}
We will be using user-user and item-item CF \cite{memory-based-approach}, which were briefly described in section 1.3.2. These memory based approaches work by calculating the weights between the active user $U$ and each other user $u$ in the matrix (\textit{Figure \ref{cf-matrix}}) \cite{memory-based-approach} using various algorithms for computing similarity. This section will cover each of these implemented algorithms. 

\subsubsection{Cosine Similarity:} We calculate the cosine similarity for two arbitrary vectors $i$ and $j$ in some inner product space, as the cosine of the angle between them. This angle is the similarity between $i$ and $j$. \textit{Equation \ref{eq:1}} defines the formula for cosine similarity \cite{memory-based-comparison}.

\begin{equation} \label{eq:1}
sim(i, j) = cosine(\vec{i}, \vec{j}) = \frac{\vec{i} \cdot \vec{j}}{||\vec{i}||||\vec{j}||} = \frac{\sum\limits_{k=1}^{n} R_{i,k} R_{j,k}}{\sqrt{\sum\limits_{k=1}^{n} R_{i,k}^2} \sqrt{\sum\limits_{k=1}^{n} R_{j,k}^2}}
\end{equation}

%[need to better explain the formula for user-user and item-item CF: Currently it is wrong and doesn't make any sense !!!]

Where $R_{i,k}$ is the rating of item, or employee $i$ at index $k$.

$R_{j,k}$ is the rating of item or employee $j \neq i$ at index $k$. \\[12pt]

In user-user CF we take $i$ and $j$ to be the columns in the item-space of the current employee $e$ and another employee $e' \in E$. In item-item CF, $i$ and $j$ are the rows in the user-space of the current shift $s$ and another shift $s' \in S$. In user-user CF we do not include the row vector of the shift for the current employee $e$, and in item-item CF we do not include the column vector of the employee for the current shift $s$. In both, the calculation is performed between all employees $e' \in E$ or all shifts $s' \in S$.

\subsubsection{Adjusted Cosine Similarity:} One of the main problems of cosine similarity for item-item CF is that it does not take the difference in rating scale between different employee's into account \cite{adjusted-cosine-similarity}. We can solve this problem subtracting the employee average from each pair of shifts the employee has rated. Badrul \cite{adjusted-cosine-similarity} suggests a new formula (\textit{Equation \ref{eq:3}}) for calculating similarity.

\begin{equation} \label{eq:3}
sim(i, j) = \frac{\sum\limits_{e \in E}^{|E|} (R_{e, i} - \bar{R}_e)(R_{e, j} - \bar{R}_e)}{\sqrt{\sum\limits_{e \in E}^{|E|} R_{e, i} - \bar{R}_e} \sqrt{\sum\limits_{e \in E}^{|E|} R_{e, j} - \bar{R}_e}}
\end{equation}

Where $\bar{R}_e$ is the average employee $e$ rating.

\subsubsection{k-nearest Neighbourhood:} Once the similarity has been computed between the target and all shifts, or employees in the matrix, we then take the k most similar neighbours (k-nearest), where $k$ is the number of employees or shifts we want to be in the neighbourhood. Stronger similarity is taken as the neighbours whose similarity is closest to 1.

\forceindent We can compute a simple weighted average of all the k-nearest item-item or user-user similarities scaled by the rating of the item for that user, or the user for that item. The result is the prediction of an employee $e$ on shift $s$, or visa versa, as defined in \textit{Equation \ref{eq:2}} \cite{weighted-average}.

\begin{equation} \label{eq:2}
P(e, s) = \frac{\sum\limits_{i=1}^{k} S_{s, i} * R_{e, i}}{\sum\limits_{i=1}^{k} S_{s, i}} 
\end{equation}

Where $P(e, s)$ is the prediction between the employee and the shift.

$R_{e, i}$ is the rating for the employee $e$ on the shift $i$.

$S_{s, i}$ is the similarity of a shift $j$. 

\subsubsection{Choice of $k$:} $k$ is a constant. The choice of $k$ affects the predictions. We choose $k$ to be the $\sqrt{i}$, as described by Bhattacharya \textit{et al.} \cite{bhattacharya2015probabilistic}, where $i$ is the column vector in user-user CF, or the row vector in item-item CF. We can evaluate different choices for $k$ in chapter 5.
