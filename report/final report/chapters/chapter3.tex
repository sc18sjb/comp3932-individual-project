\chapter{Results and Evaluation}
\label{chapter4}

%<Results, evaluation (including user evaluation) {\em etc.} should be described in one or more chapters. See the `Results and Discussion' criterion in the mark scheme for the sorts of material that may be included here.>

Within this chapter we will provide and evaluate the results of the testing of the produced web application, including presenting an experiment to see how successful we are at achieving the problem statement.

\section{Unit Testing}

Unit testing was designed to measure the reliability of the matchings and CF algorithms on sample data. The Kaggle employee schedule dataset \cite{employee-schedule-kaggle} and the nurse rostering benchmark instances \cite{shift-scheduling-dataset}, specifically instance \textit{1\_225} were taken as sample data to test these algorithms. Furthermore, example matrices, provided in the COMP3771 User Adaptive Intelligent Systems \cite{CF-matrix-user-adaptive-user, CF-matrix-user-adaptive-item} module, were used to test user-user and item-item CF for both adjusted and regular cosine similarity. 

\forceindent 10 tests were run in total on each \textit{unit} of the algorithms: 5 tests for the matchings algorithm and CSP, and 5 tests for the CF algorithms. Code for the implementations discussed in section 3.2.4 was written using test-first methodology, as described by Astels \cite{astels2003test}. This made sure the algorithms were implemented correctly for user testing in section 4.2. The results for these tests can be found in \textit{Figure \ref{fig:unit-testing}}.

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.85\linewidth}
    \includegraphics[width=\linewidth]{images/unit-testing.png}
  \end{subfigure}
  
  \caption{Unit testing results and test descriptions}
  \label{fig:unit-testing}
\end{figure}

\section{User Evaluation}

A user test was designed, split into 3 stages, to test the interface of the web application, the matchings from the modified Gale-Shapely algorithm, and the best CF method that gave the most accurate predictions. This, in turn, tests the feasibility of the problem statement, with a full evaluation of the results being considered in section 4.2.3.

\forceindent User testing was required as part of this project, to gather real ratings and preferences from participants, that we can match the algorithms against. Datasets would not be suitable because none were found that included ratings of shifts, combined with preferences.



\subsection{Participants}

12 participants (9 men, 3 women) with a mean age of 25.2, 8 of which were students at the University of Leeds and 6 undertaking a Computer Science related course, were gathered to take part in the experiment. All participants had previous experience using the web and web applications, but none had any direct experience with the topics covered within this project. All participants were given a project information sheet and consent form to sign (see \textit{Appendix \ref{appendix:participant-infomation-sheet}}), and made aware that they can exit the experiment at any point. Participants were made aware that no payment would be offered for taking part in the experiment, and that all results gathered would remain anonymous. Once the evaluation of these results had been completed, participants were made aware their information would be discarded and user accounts terminated.

\subsection{Procedure}

\subsubsection{Stage 1) Testing the Web Application:}

%\subsubsection{Data Protection:} We asked users to perform a series of tasks on the deployed application. A link was provided for this application to all users who carried out the test:

%\begin{center}
	%\href{https://rotaboard.herokuapp.com}{Link to deployed application.}
%\end{center}

%Any information gathered from test users and stored in the database was ensured to conform to general data protection regulations (GDPR) [reference]. Once the evaluation on this information has been completed it was discarded and user accounts terminated. \\[12pt]

Participants were given a link to a \textit{soft-deployed} version of the website, that is, a prototype website where user testing could take place. The same prototype can be accessed in \textit{Appendix \ref{section:soft-deployed-application}}. Testing of the web application required users be on a 'desktop' device (laptop, ChromeBook, PC etc.) because a mobile prototype of the application had not yet been developed, and was beyond the scope of this project. Participants taking the experiment on a mobile device would skip to stage 2).

\forceindent On the application, participants were directed to perform 8 tasks and rate the difficulty in performing these tasks, on a scale of 1-5 where 1 was hard and 5 was easy. These tasks are split into 2: 5 tasks for a \textit{manager} employee and 3 tasks for a \textit{regular} employee.

\forceindent Firstly, participants would create an account for the application and log into the dashboard. They would be asked to create a number of departments, shifts and employees for an example company of their choice. Participants would then logout of this account and log back into a previously set-up, test account, the credentials of which were provided to them and can be seen in \textit{Appendix \ref{section:questionnaire}}. When logged in, participants would view a regular employee dashboard with a number of example matched shifts, similar to \textit{Appendix \ref{front-end-views}}, \textit{Figure \ref{employee-dashboard}}. Participants were requested to differentiate which shifts were matched to them and other employees, as well as rating these matched shifts.

\forceindent Although desirable, it was not possible to reassign shifts for every participant after rating shifts. An in-person approach to user testing would have been required for this. Therefore, to test the matching and CF algorithms, two further stages to the experiment were implemented. 

\subsubsection{Stage 2) Testing the Matching Algorithm:}

\textit{Figure \ref{fig:example-shifts}} represents shifts, split over 3 days, that were created using information from the Kaggle employee schedule \cite{employee-schedule-kaggle} dataset, combined with the shifts created by the author, to cover the range of availability defined in section 2.2. Participants were asked to input their availability preference, then pick their most favourable shift from each of the 3 days (one each). 

\begin{figure}[h!]
\centering
\begin{tabular}{|c|c|c|c|}
\hline
Day & Monday & Tuesday & Wednesday \\
\hline
Shifts & 
    \begin{tabular}{@{}c@{}}07:00 - 10:00 \\ 07:00 - 15:00 \\ 10:00 - 15:00 \\ 15:00 - 22:00\end{tabular} &
    \begin{tabular}{@{}c@{}}07:00 - 10:00 \\ 07:00 - 15:00 \\ 15:00 - 22:00 \\ 17:00 - 22:00\end{tabular} &
    \begin{tabular}{@{}c@{}}06:00 - 09:00 \\ 09:00 - 12:00 \\ 12:00 - 17:00 \\ 17:00 - 22:00\end{tabular}\\
\hline
\end{tabular}

\caption{Example shifts split across 3 working days}
\label{fig:example-shifts}
\end{figure}

We can compare these results with the results from the modified Gale-Shapely algorithm and see how accurately it chooses the correct shift. There is a maximum of 3 correct matchings for 3 days. For instances where it does not correctly match each 3 days, we can enter the ratings provided from stage 3) of the matched shifts, and iterate the algorithm again. We record how many iterations it takes for a better matching, or until all ratings have been exhausted.

\subsubsection{Stage 3) Testing CF Methods:}

There are 8 unique shifts shown in \textit{Figure \ref{fig:example-shifts}}, extrapolated in \textit{Figure \ref{fig:unique-shifts}}. Participants were then asked to rate each shift on a scale of 1-5, where 1 is `doesn't like' and 5 is `highly likes', to enable us to take each rating of each shift, for each participant. We then build a matrix of employee-shift (in this case the participants are employees) ratings, such as shown in the example of \textit{Figure \ref{cf-matrix}}. To improve accuracy, we increase the number of shifts in the matrix (as in real-world applications shifts $>$ employees), by duplicating arbitrary points of previous ratings to construct a new row for the new shifts. We compared 18 shifts against 12 employees.

\begin{figure}[h!]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|c|c|}
\hline
Start & 06:00 & 07:00 & 07:00 & 09:00 & 10:00 & 12:00 & 15:00 & 17:00 \\
\hline
End & 09:00 & 10:00 & 15:00 & 12:00 & 15:00 & 17:00 & 22:00 & 22:00 \\
\hline
\end{tabular}
\caption{8 unique shifts from 3 working days}
\label{fig:unique-shifts}
\end{figure}

We tested CF methods by entering an arbitrary number of zeros into the matrix, then running the user-user and item-item algorithms, comparing the predictions with the original values just removed. There were a number of variables: the size of the neighbourhood, the number of zeros entered, and if adjusted cosine or regular cosine similarity was used.

\forceindent Once an optimal method has been identified, we combined the predicted ratings with preferences (as discussed in section 2.3.1), to improve the choice of shift for the matching algorithm. It was also predicted that, with a more refined preference set, less iterations would be needed until the best matching had been chosen. 

\subsection{Results}

\subsubsection{Web Application Usability Results:}

\textit{Figure \ref{fig:rate-difficulty}} displays the percentage of ratings for each score on the scale 1-5. It can be seen that participants rated the usability of the application highly, with the lowest average of $4.6$ and an average rating across all tasks of $4.76$. Most of these tasks however, were relatively trivial, involving filling in short forms, which may be a contributing factor for these high scores. Participants were also be given the opportunity to express their opinions on the look and feel of the application. To summarise: 

\begin{itemize}
	\item 6 participants stated that the website was "clean" and "minimalist", and that "the application was easy to use".
	\item 2 participants commented on the rating feature, stating it lacked "feedback" (confirmation) that a rating had gone through, or that a comment box could be added, to state \textit{why} an employee likes/dislikes a shift.
	\item 2 participants stated it could be "confusing" to tell which employee they were when viewing shifts, and that an indicator to show the current time of the day should be added.
	\item 1 participant specifically mentioned the font was too small.
\end{itemize}


\begin{figure}[h!]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|c|}
\hline
Task & Description & 1 & 2 & 3 & 4 & 5 & Average\\
\hline
1 & Create an account & 0\% & 0\% & 0\% & 10\% & 90\% & 4.9 \\
\hline
2 & Create a department & 0\% & 0\% & 10\% & 20\% & 70\% & 4.6 \\
\hline
3 & Navigate to dashboard & 0\% & 0\% & 10\% & 10\% & 80\% & 4.7 \\
\hline
4 & Create Shifts & 0\% & 0\% & 0\% & 30\% & 70\% & 4.7 \\
\hline
5 & Create employees & 0\% & 0\% & 0\% & 0\% & 100\% & 5 \\
\hline
6 & View matched shifts & 0\% & 0\% & 10\% & 20\% & 70\% & 4.6 \\
\hline
7 & View you who are working with in matched shift & 0\% & 0\% & 0\% & 20\% & 80\% & 4.8 \\
\hline
8 & Rate a shift & 0\% & 0\% & 0\% & 20\% & 80\% & 4.8 \\
\hline
\multicolumn{7}{|c|}{} & 4.76 \\
\hline
\end{tabular}
\caption{Rating difficulty of performing 8 tasks on the web application}
\label{fig:rate-difficulty}
\end{figure}

Overall, qualitative feedback shows all participants demonstrated understanding of the use of the application, with the majority expressing how clean and concise the interface was. Task 6) scored a lower average of 4.6 compared to the other tasks, which  could be attributed to the confusing nature of distinguishing employees on the dashboard.

\forceindent Participants were also given the ability to comment on the process of creating departments, employees and shifts during the experiment. The following qualitative data can be summarised:

\begin{itemize}
	\item 4 participants expressed dissatisfaction in the slider for creating shifts, stating it would be easier to directly enter the start and end times.
	\item 2 participants stated the colour option for the 'add new' button was "confusing" and hard to see.
	\item 2 participants stated they would like to see content in the password field when editing employees, as they were "unsure" if their password had been saved.
	\item 3 participants expressed the desire to be able to enter a specific start and end time, as well as specific days for employee availability.
\end{itemize} 

The main observation was the dissatisfaction in the duration slider when creating shifts, although with an average rating of 4.7, participants generally found the rest of the process easy. Likewise, there were a number of suggestions by participants to improve the usability of the application. These are beyond the scope of this short evaluation, but we will see in chapter 5 how these could contribute to ideas for future work.

\subsubsection{Modified Gale-Shapely Results with no Predictions:}

\textit{Figure \ref{fig:modified-gale-shapely-no-predictions}} shows the results for the modified Gale-Shapely algorithm with no predictions, that is, no ratings calculated using CF. We ran up to a maximum of 3 iterations as all ratings are exhausted. It can be seen that the number of correct matchings improves after each subsequent iteration. Participants 3, 4, 5 and 6 have 0 correct matchings on iteration 1, which went up to 2 (or 3 in the case of participant 3) after just 1 more iteration. The raw data used to produce these findings is shown in \textit{Appendix \ref{appendix:user-testing-results}}.

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.8\linewidth}
    \includegraphics[width=\linewidth]{images/modified-gale-correct-matchings.png}
  \end{subfigure}
  
  \caption{Correct matchings over a number of days for different participants}
  \label{fig:modified-gale-shapely-no-predictions}
\end{figure}

As per the procedure described in section 4.2.2, there is a maximum of 3 correct matchings for each participant. \textit{Figure \ref{fig:modified-gale-shapely-no-predictions-average}} shows the average matchings for each iterations. We can see that, without subsequent iteration the algorithm performs poorly, having $< 50\%$ (1.08/3) accuracy. This increases to $69\%$ (2.3/3) however, after 1 more iteration, but does not increase further on subsequent iterations. It will be seen whether combining CF techniques can change this.

\begin{figure}[h!]
\centering
\begin{tabular}{|c|c|c|}
\hline
Iteration 1 & Iteration 2 & Iteration 3 \\
\hline
1.08 & 2.3 & 2.3 \\
\hline
\end{tabular}
\caption{Average correct matchings (accuracy) for each iteration}
\label{fig:modified-gale-shapely-no-predictions-average}
\end{figure}

\subsubsection{CF method choice:}
\textit{Figure \ref{fig:CF-comparison-results}} compares user-user and item-item CF for a neighbourhood of $\sqrt{i}$ (where $i$ is the number of results), 6 and 3, and inserting 20 or 10 zeros into the matrix (see \textit{Appendix \ref{appendix:user-testing-results}}). We used RMSE and MAE (described in section 1.3.3) to evaluate each method; the lower the result, the better the prediction. Both evaluation methods yield different results, if the same patterns can be spotted in both methods, we know the data is valid.

\begin{figure}[h!]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
 & & & \multicolumn{2}{|c|}{Cosine Sim} & \multicolumn{2}{|c|}{Adjusted Cosine Sim} \\
\hline
Method & Neighbourhood Size & \# of zeros & RMSE & MAE & RMSE & MAE \\
\hline
\multirow{6}{*}{item-item} & \multirow{2}{*}{$\sqrt{i}$} & 20 & 1.196 & 0.934 &  1.142 & 0.983 \\\cline{3-7}
& & 10 & 1.290 & 0.902 & 0.823 & 0.633 \\\cline{2-7}
& \multirow{2}{*}{6} & 20 & 1.276 & 0.917 & 1.240 & 0.842 \\\cline{3-7}
& & 10 & 1.566 & 1.037 & 1.565 & 1.033 \\\cline{2-7}
& \multirow{2}{*}{3} & 20 & 1.423 & 1.113 & 1.422 & 1.100 \\\cline{3-7}
& & 10 & 1.538 & 1.403 & 1.531 & 1.300 \\
\hline
\multirow{6}{*}{user-user} &\multirow{2}{*}{$\sqrt{i}$} & 20 & 1.820 & 1.556 & 1.250 & 1.025 \\\cline{3-7}
& & 10 & 1.580 & 1.320 & 2.020 & 1.750 \\\cline{2-7}
& \multirow{2}{*}{6} & 20 & 1.632 & 1.375 & 1.591 & 1.341 \\\cline{3-7}
& & 10 & 1.567 & 1.417 & 1.642 & 1.450 \\\cline{2-7}
& \multirow{2}{*}{3} & 20 & 1.727 & 1.359 & 1.784 & 1.350 \\\cline{3-7}
& & 10 & 1.555 & 1.461 & 1.761 & 1.633 \\
\hline
\end{tabular}
\caption{user-user and item-item comparison results for different CF methods}
\label{fig:CF-comparison-results}
\end{figure}

\forceindent Several observations can be made from these results. The error seemingly increases and decreases at random as the number of zeros decreases. This is expected, as zeros are arbitrarily entered into the matrix, that could lead to sparce rows or columns. It can be seen however, that increasing the neighbourhood, decreases the error, in-line with the work by Bhattacharya \textit{et al.} \cite{bhattacharya2015probabilistic}. This is seen across both RMSE and MAE. Likewise, the error for user-user CF is higher than item-item CF for all instances of variable combinations on RMSE and MAE, which supports the work by Badrul \textit{et al.} \cite{memory-based-comparison}. This is expected as there will always be more shifts to compare than employees: more data means better predictions.

\forceindent We chose item-item CF for adjusted cosine similarity as the most optimal method for predicting ratings. Adjusted cosine similarity has a lower error for all instances of neighbourhood size and zeros, that is minimised on item-item CF. We took $k = \sqrt{i}$, as per section 2.3.3, because this has the lowest error of 1.142 and 0.823 respectively for 20 and 10 zeros with RMSE. The item-item CF method was combined with the modified Gale-Shapely algorithm to evaluate whether accuracy had improved.

\subsubsection{Modified Gale-Shapely Results with Predictions:}
\textit{Figure \ref{fig:modified-gale-shapely-average-predictions}} shows a comparison between the average correct matchings without predictions (shown in \textit{Figure \ref{fig:modified-gale-shapely-no-predictions-average}}), and those with predictions. It can be seen that adding predictions afforded by item-item CF using adjusted cosine similarity improves the accuracy for all iterations. Likewise, we can see an increase in iteration 3 that did not occur before. Unfortunately, accuracy on first iterations, although better, are still poor at $< 50\%$ (1.25/3). However, an accuracy of $83\%$ (2.7/3) after 3 iterations demonstrates significant improvement for meeting the employees and shifts preferences. 

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.66\linewidth}
    \includegraphics[width=\linewidth]{images/modified-gale-correct-matchings-average.png}
  \end{subfigure}
  
  \caption{Comparison of average correct matchings over a number of days and iterations for no-predictions and predictions}
  \label{fig:modified-gale-shapely-average-predictions}
\end{figure}





