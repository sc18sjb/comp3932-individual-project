\chapter{Motivation and Background Research}

% You can cite chapters by using '\ref{chapter1}', where the label must
% match that given in the 'label' command, as on the next line.
\label{chapter1}

% Sections and sub-sections can be declared using \section and \subsection.
% There is also a \subsubsection, but consider carefully if you really need
% so many layers of section structure.
\section{Motivation}

%<A brief introduction suitable for a non-specialist, {\em i.e.} %without using technical terms or jargon, as far as possible. This may %be similar/the same as that in the 'Outline and Plan' document. The %remainder of this chapter will normally cover everything to be %assessed under the `Background Research` criterion in the mark %scheme.>

%Here is an example of how to cite a reference using the numeric system %\cite{parikh1980adaptive}.

Companies offering shift-work (such as those providing zero-hour contracts) have an ever-prevalent scheduling problem: how do we define an optimal schedule when employees can vary in preference and availability as well as skill-set? An optimal schedule is one in which all shifts are fulfilled to the greatest possible extent and all employees receive the most relevant shift, in accordance to pre-defined preferences. The problem is not only limited to companies offering shift-work, and we see similar issues in industries such as airlines, hospitality, retail and transportation services (rail and bus) \cite{shift-scheduling-dataset}. %General observations about the lack of a formal, yet efficient way of scheduling shifts has been made from many instances of working in zero-hours positions

\forceindent It seems, to manually create a schedule, especially for a large company or organization, would be a waste of resources and time without guarantee the schedule will be optimal or that the data being worked with is 100\% accurate \cite{rotacloud}. Yet, why is it that so many companies (especially the small ones) stick to outdated and cumbersome paper schedules, which are often manually created? Perhaps it is because of the flexibility offered by manual changes, or because of its simplicity?


\forceindent When we look at companies such as Amazon or Netflix, we see they are recommending products or videos to users based upon their preferences \cite{burke2011recommender}; the model it has learnt about them. We present the question: why can this not be done for shifts in a schedule? If we already have pre-defined preferences, can these preferences not be used as a basis for shift recommendation? Would it be possible to predict which shifts an employee might like, based upon the similarity of other shifts they have worked \cite{collaborative-filtering-1}?

\forceindent There is a plethora of research on the topic of shift scheduling, commonly approached by integer programming (IP) models \cite{williams2009integer}. This project aims to take a different approach and determine which shifts an employee would be best fit for, by exploring different matching algorithms and prediction methods. Although a number of published works have explored each technique individually, it is the intent of  this project to merge these techniques to create an optimal schedule, which can be presented to the user through an intuitive web application.


\section{Problem Statement}
The goal of this project is to create an optimal schedule with the highest possible accuracy. No known algorithm can guarantee an optimal schedule, within realistic computing times \cite{shift-scheduling-dataset}, therefore we determine accuracy as an employee getting their most, or as close as possible to their most, preferable shift.

\forceindent Garett \textit{et al.} \cite{garett2016literature} discuss 20 elements of web design that affect user engagement. It is also the goal of this project to present shift scheduling to users in a understandable, easy-to-use manner, such that user engagement is maintained. Elements such as navigation, accuracy, security/privacy and simplicity will all need to be considered. 

% Must provide evidence of a literature review. Use sections
% and subsections as they make sense for your project.
\section{Background Research}
%<This section heading is purely a suggestion -- you should subdivide %this chapter in whatever manner you think makes most sense for your %project. It may also make sense to spread the `Background Research' %over more than one chapter, in which case they should be named %sensibly.>

%\subsection{Problem Statement}
%- Do we need this section???

%Talk about \textit{why} the problem needs to be approached - what %challenges have I and other people especially seen that would warrent %a change?

%General observations about the lack of a formal, yet efficient way of %scheduling shifts has been made from many instances working in zero-%hour positions and from feedback of friends and family. It seems this %research alone warrants the exploration into a better and more %optimal shift scheduling technique.  

\subsection{Stable-marriage problem}

The following definitions are necessary in our understanding of the stable-marriage problem:

\subsubsection{Matching:} A matching of a graph $G=(V, E)$ is a subset of Edges $E$ such that no two edges have a common endpoint \cite{graph-theory}. You can think of a matching as an independent set of edges $E$, none of which are loops. 

\subsubsection{Bipartite graph:} A bipartite graph $G=(V, U, E)$ is a set of graph vertices decomposed into two disjoint sets $V$ and $U$ such that no two graph vertices $v \in V, u \in U$ are adjacent. An edge connects $v \in V$ to $u \in U$. A \textbf{matching} in a bipartite graph is a subset of edges $E$ with each edge endpoint in a different vertex set $V$ and $U$.\\[12pt]


The goal of the stable-marriage problem is to find a \textbf{stable matching} of edges $E$ in a bipartite graph $G=(V, U, E)$ where the two disjoint vertex sets $V, U$ are equal length. In this case, the vertices in the two vertex sets $V$ and $U$ represent men and woman respectively who each have their own list of preferences for the opposite type. Our goal is to find a matching in which there does not exist a man or woman who are not assigned to each other, but would prefer to be assigned to each other, over their present matching \cite{stable-marriage}. Such a matching is thought to be \textbf{stable}. 

\forceindent The applications of the stable-marriage problem can be applied to different real-world situations such as our shift-scheduling problem. The two vertex sets $V$ and $U$ represent arbitrary values and are not limited to just men and women. 


\subsubsection{Gale-Shapely Algorithm:} Gale and Shapely \cite{gale-shapely} presented a solution to the stable-marriage problem, to find a stable-matching for any equal number of men and woman in an algorithmic solver. Their solution is presented in \textit{Appendix \ref{section:gale-shapely-algorithm-pseudocode}}. The algorithm works iteratively with one disjoint vertex set of the bipartite graph acting as a proposer to the other set. In the first iteration, the proposers (this is typically women) proposes to the first of the opposite type in their list of preferences. If the opposite type is not yet a part of any matching then the two are automatically matched. If the opposite type is a part of a matching then a check is done to see if the proposer is higher in the preference list: if it is, then we create a new matching with the proposer and un-match the previous proposer. Subsequent iterations perform the same check on all unmatched proposers, iterating through their preference list until there is a matching between all men and woman: a \textbf{stable} matching. 
%TODO: I probably wont a graphical illustration in the appendix here with a walkthrough to make it easier to understand.

\forceindent The algorithm is biased, as the proposing side, the women, get a better matching (higher in their preference list) than the accepting side \cite{gale-shapely-bias}. Careful consideration is therefore needed when deciding who are the proposers and accepters for real-world applications. 

\subsubsection{Constraint Satisfaction Problem (CSP):} Manlove \textit{et al.} \cite{constraint-satisfaction-SM} propose a constraint satisfaction solution to the stable-marriage problem, for modelling the problem with an incomplete lists of preferences. By definition, a constraint satisfaction problem (CSP) involves a set of objects whose state must satisfy a finite number of constraints \cite{constraint-satisfaction-problem}. We model constraints in a triple $\{X, D, C\}$ with $X$, $D$ being a set of $n$ variables and their domain of $n$ values, and $C$ being a set of $m$ constraints \cite{constraint-satisfaction-problem-domain}.

\forceindent Manlove \textit{et al.}'s \cite{constraint-satisfaction-SM} work, permitting CSPs to be used as a model for the stable marriage problem, is important because it allows us to model our shift scheduling problem in the correct context. We can add constraints such as maximum and minimum employees and cost factorization into the equation, which would not have been possible before, and is suitable to this project's proposal.


\subsection{Recommender Systems}  
Recommender systems aim to predict a user's rating of an item, to see which items can be recommended to said user in the future \cite{recommender-system-1, recommender-system-2}. The basic premise is that we take some background information about the user $U$ and the items $I$ and try to create a matching of an item $i \in I$ to a user $u \in U$ based on inferred relationships, typically the rating of a user-item matching \cite{recommender-system-graph}. This is done in a process called \textbf{filtering}. It is these recommendation engines that have been used by Amazon, Netflix or YouTube \cite{burke2011recommender} to name a few, recommending videos or products based on the user profile. For example, a user views an item on Amazon, then implicitly there is a likelihood of the user being interested in items in the same, or similar categories. Likewise, if the same user rated an item poorly, then explicitly there is a likelihood of the user \textit{not} liking items in similar categories.  

\forceindent Indeed, we can represent relationships in recommender systems as a weighed bipartite graph $G=(U, I, E)$ with $U$ and $I$ being the user and item sets \cite{recommender-system-graph} and the weights being the ratings. More commonly however, is to represent relationships as a matrix of integers \cite{recommender-system-1}. There are various forms of filtering in recommender systems but the most typical examples are Collaborative Filtering, Content-based Filtering or Knowledge-Based filtering. For the purpose of this project, we will only focus on Collaborative filtering.

\subsubsection{Collaborative Filtering:} Collaborative Filtering (CF) is a filtering technique that takes the ratings of different users on certain items and uses these to predict new ratings by comparing their similarity \cite{collaborative-filtering-1}. The user model in a recommender system that implements CF would include a matrix of user ratings typically from 0-5 where 0 means we don't know whether the user likes the item, 1 means the user doesn't like the item and 5 means the user highly likes the item. We can gather these ratings from different explicit and implicit collection techniques. 

\forceindent Breese \textit{et al.} \cite{memory-based-approach} describe two approaches to implementing CF. \textbf{Memory-based} approaches, which operate over the entire database to make predictions, and \textbf{Model-based} approaches that learns from the database to create a model, which can then be used for making predictions. 

\subsubsection{Memory-Based Approaches:} Memory-based approaches to CF can be split into two forms: a user-user or item-item approach \cite{memory-based-approach}. In a user-user approach, we recommend items based on what other, similar users have liked. Whereas, in an item-item approach we recommend based on the ratings of other, similar items. Both approaches expect no prior knowledge about the items being recommended, except from the ratings used to calculate predictions. Badrul \textit{et al.} \cite{memory-based-comparison} propose that the item-item based algorithms provides dramatically better performance than user-user based algorithms. The two can be compared using different evaluation techniques described in section 1.3.3.

\forceindent User-user memory-based approaches use a pre-defined set of users, similar to the target user, often called the neighbourhood \cite{memory-based-comparison}. These k-nearest neighbour approaches calculate the weights between the active user $U$ and each other user $u$ in the matrix \cite{memory-based-approach}, often to reflect similarity between the users. Different algorithms such as cosine-based similarity or correlation-based similarity can be used to achieve this. These weights can then be used to make predictions to the active user $U$ by taking the \textbf{weighted average} of the k-nearest users $u$ and their ratings \cite{memory-based-comparison}. Similarly, item-item memory based approaches where the neighbourhood is a set of k similar items $i$ to the active users item $I$, and predictions are formed from the \textbf{weighted average} of the target users ratings on these similar items \cite{memory-based-comparison}.

\subsubsection{Model-Based Approaches:} Alternatively, model-based approaches to CF use machine-learning algorithms to build a model of user ratings that can be used to make predictions \cite{memory-based-comparison}. Su and Khoshgoftaar \cite{su2009survey} describe 3 implementations of these machine learning algorithms including bayesian models, clustering models and dependency networks. Many of these methods solve the shortcomings of memory-based approaches, such as data sparsity and scalability issues, where we don't have enough data to make accurate predictions, or where there are a large number of users / items in the matrix, in which similarity between all of them needs to be calculated (inefficient). For the purpose of this short project, we will not be considering model-based approaches to CF.

\subsection{Evaluation Techniques}

Different metrics can be used to evaluate predicted results from algorithms, such as the ones discussed in section 1.3.1 and 1.3.2. Calculating the \textit{error} between the true and predicted values: lower errors means better predictions. Different evaluation metrics are used depending on the goal of the algorithm, and the data presented \cite{su2009survey, herlocker2004evaluating}.


\begin{multicols}{2}
\begin{equation} \label{eq:6}
\text{MAE} = \frac{\sum\limits_{i=1}^{n} |y_i - x_i|}{n}
\end{equation}
\begin{equation} \label{eq:7}
\textbf{RMSE} = \sqrt{\frac{\sum\limits_{i=1}^{n} (y_i - x_i)^2}{n}}
\end{equation}
\end{multicols}

\subsubsection{Mean Absolute Error:} Mean absolute error (MAE) (see \textit{Equation \ref{eq:6}} \cite{herlocker2004evaluating}) calculates the difference between the true and predicted values, and averages over the set of results \cite{chai2014root}. MAE is so called since it takes the absolute values, such that the error is always positive.   

\subsubsection{Root Mean Square Error:} Root mean square error (RMSE) (see \textit{Equation \ref{eq:7}} \cite{herlocker2004evaluating}) also calculates the difference between the true and predicted values, averaged over the set of results. RMSE makes values positive by squaring the difference, and subsequently square rooting the result \cite{chai2014root}. Compared to MAE, RMSE is sensitive to large errors that makes it a more favourable metric when smaller errors are preferred. Typically MAE and RMSE are used in conjunction with one other.

\subsection{Integer Programming (IP) Model Formulation}

We state in \textit{Appendix \ref{section:what-i-could-improve-upon}} it was not possible to include Integer Programming (IP) model formulation as part of this small project, and that different approaches (discussed above) are used instead. It is still worth mentioning the topic however, because of the breadth of research, that is highly relevant to the problem statement.

\forceindent Integer programming (IP) is an area of optimization \cite{conforti2014integer} that takes an objective function and a number of variables, many of which are restricted to be integers. Integer linear programming (ILP) is a type of IP in which the constrains on variables are \textit{linear}. In mixed integer linear programming (MILP), specific decision variables must take integer values, whilst others may take functional values \cite{combinatorial-optimization}.

An ILP can be expressed in \textit{canonical} form \cite{conforti2014integer}:

\begin{equation} \label{eq:5}
\begin{array}{ll@{}ll}
\text{maximize}  & \displaystyle cx &\\
\text{subject to}& \displaystyle Ax \leq b\\
                 &                x \geq 0 \text{ and integer}
\end{array}
\end{equation}

$c, b, x$ are vectors that are to be decided \cite{papadimitriou1998combinatorial} and $A$ is a matrix of integer entries. IP models are relevant to the problem statement, and models could minimize the number of employees to schedule, with variables such as the number of employees, the number of shifts and the days of the week. 

%\subsubsection{Knapsack Problem:} Dowsland and Thompson \cite{10.2307/253963} present a solution to the nurse scheduling problem (see section 1.3.5) using knapsacks.  

\subsubsection{ILP Solvers:} \textit{Equation \ref{eq:5}} can be converted to \textit{standard} form by eliminating inequalities using slack and artificial variables. These are variables that take the form of $b$ to create a basic feasible solution (BFS). Vanderbei \cite{vanderbei2014integer} suggests two algorithms for solving ILP models: the (primal) simplex method and duel-simplex method. The branch and bound method can be used to find an exact upper or lower bounds for integer constraints, such as shown in \textit{Equation \ref{eq:5}}. These solvers work iteratively, by eliminating areas of the search space to find a BFS: a solution that adheres to the right hand side (RHS) and the objective function. 


\subsection{Existing Solutions}

There are a variety of existing solutions that aim to achieve, or partially achieve, the problem statement. No solutions were found that combine all, or several of the previously mentioned methods discussed in this chapter.

\subsubsection{Hospitals / Residents problem:} The Hospitals / Residents problem (HRP), which can commonly be referred to as the \textbf{college admissions problem} \cite{gale-shapely}, is an example of the stable-marriage problem formulated with the CSP. Here, $r$ residents and $h$ hospitals have a list of preferences for each of the opposite type. Hospitals $h$ include an additional constraint of the capacity $c \leq r$ that must be filled. Essentially, a hospital $h$ can be matched to multiple residents $r$. Gale and Shapely \cite{gale-shapely}, in the same paper as they proposed the Gale-Shapely algorithm, also proposed a linear-time algorithm to finding a stable matching in the HRP. Manlove \textit{et al.} \cite{constraint-satisfaction-SM} proposed a CSP approach to solving the HRP  with incomplete lists of preferences, when each hospital $h$ has a capacity $c = 1$.

\subsubsection{Assignment Problem:} The Assignment Problem considers an edge-weighted bipartite graph $G=(U, V, E)$ and asks for a perfect matching of G, having the lowest possible weights for the edges $E$. \cite{assignment-problem}. We are essentially taking a number of $agents \in U$ and $tasks \in V$ and creating agent-task matchings with costs. We want to minimize this total cost by assigning, at most, one agent, and one task to each other. It is clear that linear programming is a common technique for solving this problem and various algorithms exist to solve the LP models in polynomial time. One of these algorithms is the well-established Hungarian method, which is beyond the scope of this project. 

\subsubsection{Nurse Scheduling Problem:} The Nurse Scheduling problem (NSP) involves scheduling nurses to shifts given a set of $C$ constraints formulated as a CSP \cite{nurse-scheduling-problem}. The constraints $C$ might include limiting the shifts a nurse can work to morning, day or night, preventing two nurses who dislike each other from working together, and pairing lazy nurses with hard working nurses. Hakim \textit{et al.} \cite{nurse-scheduling-problem-approach}, proposed a nonlinear approach to solving the (NSP) with the objective of minimizing the variance of the workload, and proved that the model could be solved. 

\forceindent The NSP is important because it directly relates to the shift scheduling problem of this project, with the nurses equating to the employees and the constraints $C$ equating to the constraints on the shifts we assign employees to.


\subsection{Existing Deployed Applications}

%There is a vast assortment of existing deployed SAAS application that %aim to achieve, or partially achieve the problem statement. Many of %these applications are obscure, therefore during the research process %it was decided to focus on only the main application offering fully-%fledged solutions. 

Different deployed applications propose implementations to some of the existing solutions previously mentioned in section 1.3.5. Most of these applications focus more on interactivity, but are none-the-less important to understanding why the problem exists. 

%TODO: include image that shows the interface of RotaCloud.com in the Appendix
\subsubsection{RotaCloud:} RotaCloud \cite{rotacloud-website} is an interactive website and mobile application that allows `managers to create and share rotas in minutes, regardless of their experience' \cite{rotacloud-what-is-it}. The application offers time and attendance monitoring, manual shift scheduling and payroll integrations, to name a few features. The application is primarily focussed towards scheduling companies offering full-time contracts, rather than changeable zero-hour contracts and does not feature any techniques for auto scheduling.

%TODO: include image that shows the interface of Rota.com in the Appendix
\subsubsection{Rota:} Rota \cite{rota-website} is an alternative to RotaCloud, also offering a web-based and mobile-based approach to shift scheduling. The application offers time and attendance monitoring and communication between employees through briefing requirements \cite{rota-com}. 

\forceindent Unlike RotaCloud, Rota proposes a system that offers automated shift scheduling to identify and fill the gaps of a schedule \cite{rota-com}; finding shifts that have not been filled, and matching these to available employees. The application implements a form of a recommender system, described in section 1.3.2, to seemingly manage the performance of employees rated by managers, allowing better recommendations of shift-employee combinations and `identify areas for improvement and training needs', as described by Williams \cite{rota-com}.


\subsubsection{Deputy:} Whilst Deputy \cite{deputy-website} offers the same features as RotaCloud and Rota, it additionally offers the most extensive suite of auto-scheduling, similar to the problem statement. They propose a system that creates employee-shift matchings based on priorities such as lowest cost, spread of hours and past preferences \cite{deputy-com}. Although it is unclear the techniques used to implement this system (as this information is not disclosed by the company), it is likely, based on the statement given, that a stable matching is created using techniques described in section 1.3.1. \\[12pt]

These applications show that, above all, there is a need for better shift scheduling techniques in the work place. Combined, the applications help over 304,000 businesses \cite{rotacloud-what-is-it, rota-com, deputy-com} manage and deploy schedules online. Furthermore, these works demonstrate that different individual techniques to shift scheduling described throughout this literature review work in-practice, and an exploration into combining these techniques seems valuable.


\subsection{Datasets}

Research to find datasets that would match the problem statement was carried out to help advance and improve the results from this project. The following suitable datasets were found:

\subsubsection{Nurse Rostering Benchmark Instances:} The Nurse Rostering Benchmark Instances dataset \cite{nurse-rostering-dataset} is contained within the wider Shift Scheduling Benchmark dataset \cite{shift-scheduling-dataset}. The dataset provides instances for benchmarking solutions to the nurse scheduling problem described in section 1.3.5. These benchmarks are designed to test the efficiency of a given solution, that the schedules produced are significantly better than a human equivalent and devised in a fraction of the time. The instances also provides solutions to a given benchmark. 

\subsubsection{Employee Schedule - Kaggle:} The Employee Schedule dataset \cite{employee-schedule-kaggle} provides the necessary data to create a schedule based on preferences, where these preferences are defined as the employee availability and the minimum and maximum hours an employee must work. The dataset does not provide 'shifts' \textit{per se}, but provides instances of demand that needs to be filled in given groups. The dataset, with small modifications, allows the testing of matchings provided by this project.
