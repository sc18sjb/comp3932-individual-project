\begin{appendices}

%
% The first appendix must be "Self-appraisal".
%
\chapter{Self-appraisal}

%<This appendix should contain everything covered by the 'self-appraisal' criterion in the mark scheme. Although there is no length limit for this section, 2---4 pages will normally be sufficient. The format of this section is not prescribed, but you may like to organise your discussion into the following sections and subsections.>

\section{Critical self-evaluation} 
\label{section:self-evaluation}

\subsection{What Went Well}
From the start of the project, a clear plan of the order in which to complete tasks was set out. This roughly followed the route of: initial research, algorithm design without CF, website design, algorithm design with CF, and final changes and polishing of the web application. It was not hard to come up with this plan, and it was closely adhered to during the entire project process, allowing for the whole project to run smoothly.

\forceindent As the author had previously studied COMP3771 User Adaptive Intelligent Systems, the process of researching and implementing CF techniques was found relatively straightforward, since many of the topics had previously been covered during that module. CF techniques were successfully implemented for shift scheduling as, from the research conducted in chapter 1, no previous examples, or academic papers, were found that could be referred to. The results also showed promise of this being a viable method. It is clear we also did well to explain these techniques in this report, and a fine-line between algorithmic design and website implementation was pursued and well adhered to. Splitting methods into two chapters: design and implementation was one way this was achieved.

\forceindent The web application produced was both professional and functional. PHP, JavaScript and Python were chosen for each of the 3 components (see section 3.1.2) based upon familiarity and allowed for better effectiveness and efficiency in implementation, that led to a successful solution. Choosing unfamiliar languages could have negatively led to a drastically different outcome for this project, such as more time being spent on implementation, taking away from possible research potential.

\forceindent Finally, the motivation around this project was well-founded, and we have clearly expressed \textit{why} the research was to be conducted. When looking at existing applications, there was a clear need for such solutions presented in this project, in addition to other areas of research that had not currently been filled. 

\subsection{What Could be Improved}
\label{section:what-i-could-improve-upon}

Although familiar languages were chosen for the implementation of the algorithms and, most specifically, the web application, lots of time was wasted during this project on trivial tasks such as creating requests to and from the API's, and implementing ORM's. The front-end application, although successful, may have been 'overdone' as a much simpler implementation could have achieved similar results. Many of these tasks took away time from further research potential of the project, such as exploring model-based approaches (see section 1.3.2), that could have presented a more interesting set of results. Although the plan for this project was well-defined in terms of stages, it was not well-defined in terms of timings of each stage, that meant the vast majority of this project was completed in the last third of term time. The plan also did not account for coursework or exams that can be notoriously difficult to estimate completion time for, and only furthered to minimize the possible time allocated to this project.

\forceindent A poorly timed plan also meant the results and discussions of this project suffered. Although we looked at various results for accuracy improvement, we never mention other factors such as the speed of these algorithms. Specific benchmarking would have been a good way to achieve this, and was a missed opportunity. Furthermore, we mentioned how unit testing was carried out (see section 4.1), but it is unclear as to the degree these tests were completed. For example, it is easy to overlook that testing of \textit{equal} sets on the matching algorithms was also carried out, to ensure the regular (non-modified) Gale-Shapely algorithm still worked. One of the initial plans of the project, to create an ILP model to solve for the minimum number of employees needed to fill shifts, also had to be discarded due to the poorly timed plan.

\forceindent During user testing, it was hard to get a large sample size of participants as it was conducted during the Easter holidays, and all participants had to complete the form on-line. Again, a better timed plan could have meant user testing could be conducted earlier, that would have also allowed real-time re-matching to take place, that we discussed in section 5.1.



\section{Personal reflection and lessons learned}

Overall, having completing the project, I can confidently say I am proud with what I have accomplished, not only in terms of the goals of the project, but in the software solutions produced and the quality and extensibility of the code. Throughout the project I wanted to ensure that, if I were to extend my solution, I could. Looking at the possibility of future work excites me to continue working on the project, especially with refinement of the web application.

\forceindent As we saw in \textit{\ref{section:self-evaluation}}, from the start of the project, I feel I had a good plan in terms of which order to complete tasks. I learnt however, to take more consideration of other variables, such as exams and coursework, that could significantly alter the time scale of a project. Even with these set-backs, I believe I made good progress to investigate the problem, write applications with over 10,000 lines of code, test these applications, and produce this report.

\forceindent One of my main take-aways from this project is the notion of KISS (keep it simple, stupid). Regrettably, scope creep was an ever-prevalent issue, and I learnt from the outset that a more comprehensive list of what I wanted to achieve, and where to stop,  should have been defined. For example, I spent 2 months developing the web application and better improving my expertise in React (that I had little experience with). One could deem this unnecessary for this project, and a simpler interface (or no interface at all) would have been sufficient to test the algorithms.

\forceindent Finally, I would like to state that this project was both enjoyable and satisfying to undertake and that I believe there to be an exciting and ever-growing range of applications for the algorithms and methods investigated.

\section{Legal, social, ethical and professional issues}

%<Refer to each of these issues in turn. If one or more is not relevant to your project, you should still explain {\em why} you think it was not relevant.>

\subsection{Legal issues} There are legal issues to this project. The most notable being the requirement to remain compliant with the General Data Protection Act (GDPR) \cite{voigt2017eu}. This ensures the confidentiality and protection of personal data belonging to participants who signed up to the \textit{soft-deployed} application during user testing, and any other users who may come across the application. No user tracking takes place on the web application and if, at a future date, this was to be implemented, a cookies policy and declaration would need to be presented. It has been ensured that users data is stored correctly and in accordance to GDPR Regulations, including correct hashing of passwords described in section 3.2.2. A Terms and Conditions sheet, and Privacy Statement, should be provided when users sign up to the application, explaining how the data collected will be used and stored. If the application was to be \textit{fully-deployed} (not a prototype), this would need to be added. 

\forceindent Other legal issues relate to the use of unauthorized content, such as images or software packages. All images used within this web application have been taken from royalty-free websites such as svgrepo \cite{svgrepo} and cited as necessary. All software packages and libraries used are open-source, or require no license.

\subsection{Social issues}
Iskra-Golec \textit{et al.} \cite{iskra2016social} describe social issues relevant to shift work. They state working irregular hours can cause work-life imbalances, or even family issues, as well as sleep and daytime functioning issues (say, if working late into the night). In this project, we have presented a solution to creating schedules for shift work. However, we have shown it takes several iterations for the accuracy of these schedules to increase, and that first iterations will always have a $< 50\%$ accuracy. The social issues described by Iskra-Golec \textit{et al.} \cite{iskra2016social} may therefore be present in our proposed solution: potentially two employees who don't like one another may get matched to the same shift, or an employee maybe required to work late into the night affecting their sleep schedule, to name a few issues. Before subsequent iterations can happen, these social issues will be present.

\forceindent Furthermore, were the application to be \textit{fully-deployed}, accessibility issues would need to be considered (Equality Act 2010 \cite{hepple2010new}). For example, employees with learning difficulties that impact upon their ability to access the website, would need to have alternatives provided. Hanson and Richards \cite{hanson2013progress} describe assistive devices such as screen readers that could convert a shift schedule into speech. This also benefits those with other accessibility issues, such as visual impairments. They describe other features, such as the ability to change font sizes (which was also mentioned in the qualitative feedback from user testing), high-contrast colour options, and simplified text options, to name a few. Training could also be provided (such as by a manager) to offer assistance in using the application, ensuring no one is disadvantaged from not being able to read their schedule.

\forceindent Likewise, physical access to technology needs to be considered, and those who cannot access devices to view their shifts online should not be disadvantaged. Alternatives such as a print option on the website, can help alleviate such issues.

\subsection{Ethical issues}
As user testing was a crucial part of this project, to mitigate any ethical issues regarding consent, all participants were supplied with a consent form to sign, and a project information sheet declaring how their data would be collected and what it would be used for. Participants were also clearly advised that they could leave the experiment at any time, and that their personal information would be safely removed. Were the application is to be \textit{fully-deployed}, users would also give be required to give their consent when signing up.

\forceindent Dembe \cite{dembe2009ethical} describes ethical issues of working long hours, including increased likelihood of illness or injury. In this project we explored a CSP approach, using different constraints to prevent employees working long hours, or not getting enough rest. We also adhered to working practice guidelines \cite{weekly-working-hours, rest-breaks} that helped ensure these ethical issues would be mitigated.

\subsection{Professional issues}
To ensure all professional issues were mitigated, we followed the code of conduct defined by the British Computing Society (BCS) \cite{BSC-code-of-conduct}. They state that professional competence and integrity should be maintained, meaning the professional (the author) should only undertake work, or claim a level of competence, that is actually maintainable (can actually do). Throughout this project we have referenced work that we did not contribute or partake in, and have provided a list of external materials (\textit{Appendix \ref{appendix:external-materials}}) used. We made the distinction between what we did and \textit{can} do, and what we \textit{cannot} do. Therefore, no professional issues, as far as the author could tell, occurred during this project.

%
% Any other appendices you wish to use should come after "Self-appraisal". You can have as many appendices as you like.
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% External Material Appendix 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{External Material}
\label{appendix:external-materials}

\section{Gale-Shapely Algorithm}
\label{section:gale-shapely-algorithm-pseudocode}

The Gale-Shapely algorithm \cite{gale-shapely} was used as a basis, and inspiration for the \textit{modified} Gale-Shapely algorithm presented in this project.

\begin{algorithm}
\caption{Gale-Shapely Algorithm}
\label{alg:galeshapely}
\begin{algorithmic}[1]
\Procedure{GaleShapely}{M, W}
\State Initialise $m \in M$ and $w \in W$ to be \textit{free}
\While {$\exists$ \textit{free} man $m$ who has a woman $w$ to propose to}
\State $w \gets$ first woman on m's list to whom m has not yet proposed
\If {$\exists$ some pair $(m', w)$}
\If {$w$ prefers $m$ to $m'$}
\State $m'$ becomes \textit{free}
\State $(m, w)$ become engaged
\EndIf
\Else
\State $(m, w)$ become engaged
\EndIf
\EndWhile
\EndProcedure
\end{algorithmic}
\end{algorithm}

\section{External Libraries}

\subsection{Back-End Libraries}
The following external libraries/frameworks were used in the implementation of the back-end API:

\begin{center}
\begin{tabular}{|c|c|}
\hline
Library & Version \\
\hline
PHP & 7.4.0 \\
\hline
laravel/framework & 8.65 \\
\hline
laravel/sanctum & 2.14 \\
\hline
laravel/tinker & 2.5 \\
\hline
laravel/ui & 3.3 \\
\hline
guzzlehttp/guzzle & 7.4 \\
\hline
\end{tabular}
\end{center}

\subsection{Front-End Libraries}
The following external libraries/frameworks were used in the implementation of the front-end views:

\begin{center}
\begin{tabular}{|c|c|}
\hline
Library & Version \\
\hline
JavaScript & 1.7 \\
\hline
react & 17.0.2 \\
\hline
react-dom & 17.0.2 \\
\hline
sass & 1.32.11 \\
\hline
sass-loader & 11.0.1 \\
\hline 
axios & 0.21.4 \\
\hline
jquery & 3.6 \\
\hline
moment & 2.29.2 \\
\hline
moment-timezone & 0.5.34 \\
\hline
react-bootstrap & 2.1.2 \\
\hline
react-bootstrap-range-slider & 3.0.3 \\
\hline
react-loading-overlay & 1.0.1 \\
\hline
react-moment & 1.1.1 \\
\hline
react-router-dom & 6.2.1 \\
\hline
react-spinners & 0.11.0 \\
\hline
\end{tabular}
\end{center}

\subsection{Algorithms Libraries}
The following external libraries/frameworks were used in the implementation of the algorithms API:

\begin{center}
\begin{tabular}{|c|c|}
\hline
Library & Version \\
\hline
Python & 3.9 \\
\hline 
numpy & 1.22.3 \\
\hline
fastApi & 0.75.0 \\
\hline
requests & 2.27.1 \\
\hline
scipy & 1.8.0 \\
\hline
typing\_extensions & 4.1.1 \\
\hline
uvicorn & 0.17.6 \\
\hline
Werkzeug & 2.0.3 \\
\hline
\end{tabular}
\end{center}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% URL's and Deployed Applications Appendix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{URL's and Deployed Applications}

\section{Source Code}
\label{section:source-code}

\subsubsection{Back-End Source Code:} \url{gitlab.com/sc18sjb/comp3932-individual-project/-/tree/main/web_app/rotaboard}

\subsubsection{Front-End Source Code:} \url{gitlab.com/sc18sjb/comp3932-individual-project/-/tree/main/web_app/rotaboard/resources/js}

\subsubsection{Algorithms Source Code:} \url{gitlab.com/sc18sjb/comp3932-individual-project/-/tree/main/scheduler}

\section{Soft-deployed Applications}
\label{section:soft-deployed-application}

\subsubsection{Front-End Application:} \url{https://rotaboard.herokuapp.com}

\subsubsection{Back-End API Link:} \url{https://rotaboard.herokuapp.com/api/}

\subsubsection{Algorithms API Link:} \url{http://rotaboard-matching.herokuapp.com/}

\section{Questionnaire}
\label{section:questionnaire}

\subsubsection{Questionnaire Link:} \url{https://forms.gle/kmjePC6gHoFMqsdaA}

\subsubsection{Test Account Details:}

\begin{itemize}
	\item \textbf{Username/Email:} test@user.com
	\item \textbf{Password:} unileeds3912
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Example solutions appendix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Example Solutions}

\section{Example Modified Gale-Shapely Algorithm Walkthrough}
\label{section:example-modified-gale-shapely-algorithm-walkthrough}
\begin{center}
\begin{tabular}{ |c|c|c|c|c| } 
 \hline
 \multicolumn{5}{|c|}{Shifts} \\
 \hline
 ID & Start - End & Department & Min & Max \\ 
 \hline
 1 & 06:30:00 - 12:00:00 & Shop floor & 0 & 2 \\ 
 \hline
 2 & 06:15:00 - 12:00:00 & Counter & 2 & 2 \\
 \hline
 3 & 09:00:00 - 17:00:00 & Post office & 1 & 2 \\
 \hline
 4 & 12:00:00 - 20:00:00 & Shop floor & 0 & 2 \\
 \hline
 5 & 12:00:00 - 20:00:00 & Counter & 1 & 1 \\
 
 \hline
\end{tabular} \\[12pt]

\begin{tabular}{ |c|c|c| }
\hline
\multicolumn{3}{|c|}{Employees} \\
\hline
Name & Departments & Availability \\
\hline
A & \{post Office\} & \{morning, afternoon\} \\
\hline
B & \{Shop floor, Counter\} & \{afternoon, evening\} \\
\hline
C & \{Shop floor, Counter, Post office\} & \{morning, afternoon\} \\
\hline
D & \{Shop floor, Counter\} & \{morning, afternoon, evening, night\} \\
\hline

\hline
\end{tabular} \\[12pt]

Let us first define a set of Shifts $S = \{1, 2, 3, 4, 5\}$, employees $E = \{A, B, C, D\}$ and departments $D = \{Shop floor, Counter, Post office\}$. The attributes for each of these are defined in the tables above.
\end{center}

\begin{center}
\begin{tabular}{|c|c|l|}
\hline
\multicolumn{3}{|c|}{Shifts} \\
\hline
ID & Min & employee\\
\hline
1 & 0 & \cancel{B} C D \\
\hline
2 & 2 & \cancel{B} C D \\
\hline
3 & 1 & \circled{A} C \\
\hline
4 & 0 & \circled{B} C D \\
\hline
5 & 1 & B C D \\
\hline
\end{tabular}
\quad
\begin{tabular}{|c|l|}
\hline
\multicolumn{2}{|c|}{Employees} \\
\hline
Name & shift\\
\hline
A & \circled{3} \\
\hline
B & \circled{4} 5 \\
\hline
C & 1 2 3 4 5 \\
\hline
D & 1 2 4 5 \\
\hline
\end{tabular}
\quad
\begin{tabular}{|c|c|}
\hline
\multicolumn{2}{|c|}{Matchings} \\
\hline
A & 3 \\
\hline
B & 4 \\
\hline
\end{tabular} \\[12pt]

1) We first match each shift to their most preferable employee if possible. We see shifts $1$ and $2$ could not be matched to employee $B$, since $B$ does not have these shifts in its preferences. Therefore, employee $B$ is matched to shift $4$ and employee $A$ to $3$.
\end{center}


\begin{center}
\begin{tabular}{|c|c|l|}
\hline
\multicolumn{3}{|c|}{Shifts} \\
\hline
ID & Min & employee\\
\hline
1 & 0 & \cancel{B} C D \\
\hline
2 & 2 & \cancel{B} C D \\
\hline
3 & 1 & \circled{A} C \\
\hline
4 & 0 & \circled{\cancel{B}} C D \\
\hline
5 & 1 & \circled{B} C D \\
\hline
\end{tabular}
\quad
\begin{tabular}{|c|l|}
\hline
\multicolumn{2}{|c|}{Employees} \\
\hline
Name & shift\\
\hline
A & \circled{3} \\
\hline
B & \circled{\cancel{4}} \circled{5} \\
\hline
C & 1 2 3 4 5\\
\hline
D & 1 2 4 5\\
\hline
\end{tabular}
\quad
\begin{tabular}{|c|c|}
\hline
\multicolumn{2}{|c|}{Matchings} \\
\hline
A & 3 \\
\hline
B & 5 \\
\hline
\end{tabular} \\[12pt]

2) Shift $5$ also has employee $B$ as its first preference. In this case we swap employee $B$ from $4$ to $5$, even though its lower in its preferences,  because shift $5$ has a minimum of 1 whilst shift $4$ has a minimum of 0 and doesn't 'need' to be filled.
\end{center}

\begin{center}
\begin{tabular}{|c|c|l|}
\hline
\multicolumn{3}{|c|}{Shifts} \\
\hline
ID & Min & employee\\
\hline
1 & 0 & \cancel{B} \circled{C} D \\
\hline
2 & 2 & \cancel{B} C D \\
\hline
3 & 1 & \circled{A} C \\
\hline
4 & 0 & \circled{\cancel{B}} C D \\
\hline
5 & 1 & \circled{B} C D \\
\hline
\end{tabular}
\quad
\begin{tabular}{|c|l|}
\hline
\multicolumn{2}{|c|}{Employees} \\
\hline
Name & shift\\
\hline
A & \circled{3} \\
\hline
B & \circled{\cancel{4}} \circled{5} \\
\hline
C & \circled{1} 2 3 4 5 \\
\hline
D & 1 2 4 5 \\
\hline
\end{tabular}
\quad
\begin{tabular}{|c|c|}
\hline
\multicolumn{2}{|c|}{Matchings} \\
\hline
A & 3 \\
\hline
B & 5 \\
\hline
C & 1 \\
\hline
\end{tabular} \\[12pt]

3) We now loop back over any unmatched shifts and perform the same steps as 1). Here, shift $1$ is matched to employee $C$, the next in its preferences.
\end{center}

\begin{center}
\begin{tabular}{|c|c|l|}
\hline
\multicolumn{3}{|c|}{Shifts} \\
\hline
ID & Min & employee\\
\hline
1 & 0 & \cancel{B} \circled{\cancel{C}} D \\
\hline
2 & 2 & \cancel{B} \circled{C} D \\
\hline
3 & 1 & \circled{A} \cancel{C} \\
\hline
4 & 0 & \circled{\cancel{B}} \cancel{C} D \\
\hline
5 & 1 & \circled{B} \cancel{C} D \\
\hline
\end{tabular}
\quad
\begin{tabular}{|c|l|}
\hline
\multicolumn{2}{|c|}{Employees} \\
\hline
Name & shift\\
\hline
A & \circled{3} \\
\hline
B & \circled{\cancel{4}} \circled{5} \\
\hline
C & \circled{\cancel{1}} \circled{2} \cancel{3} \cancel{4} \cancel{5}\\
\hline
D & 1 2 4 5\\
\hline
\end{tabular}
\quad
\begin{tabular}{|c|c|}
\hline
\multicolumn{2}{|c|}{Matchings} \\
\hline
A & 3 \\
\hline
B & 5 \\
\hline
C & 2 \\
\hline
\end{tabular} \\[12pt]

4) As with 2), shift $2$ also has employee $C$ as the next highest in its preferences. Again, we swap here for the same reasons, as shift $1$ can be 'skipped' having a minimum of 0. $C$ will not be swapped to either shift $3, 4$ or $5$ since they are all lower in its preferences, and $C$ is already matched to a shift with a minimum $> 0$.
\end{center}

\begin{center}
\begin{tabular}{|c|c|l|}
\hline
\multicolumn{3}{|c|}{Shifts} \\
\hline
ID & Min & employee\\
\hline
1 & 0 & \cancel{B} \circled{\cancel{C}} \circled{D} \\
\hline
2 & 2 & \cancel{B} \circled{C} D \\
\hline
3 & 1 & \circled{A} \cancel{C} \\
\hline
4 & 0 & \circled{\cancel{B}} \cancel{C} D \\
\hline
5 & 1 & \circled{B} \cancel{C} D \\
\hline
\end{tabular}
\quad
\begin{tabular}{|c|l|}
\hline
\multicolumn{2}{|c|}{Employees} \\
\hline
Name & shift\\
\hline
A & \circled{3} \\
\hline
B & \circled{\cancel{4}} \circled{5} \\
\hline
C & \circled{\cancel{1}} \circled{2} \cancel{3} \cancel{4} \cancel{5}\\
\hline
D & \circled{1} 2 4 5\\
\hline
\end{tabular}
\quad
\begin{tabular}{|c|c|}
\hline
\multicolumn{2}{|c|}{Matchings} \\
\hline
A & 3 \\
\hline
B & 5 \\
\hline
C & 2 \\
\hline
D & 1 \\
\hline
\end{tabular} \\[12pt]

5) We repeat 1) again for any remaining unmatched shifts. This time shift $1$ is matched to employee $D$.
\end{center}

\begin{center}
\begin{tabular}{|c|c|l|}
\hline
\multicolumn{3}{|c|}{Shifts} \\
\hline
ID & Min & employee\\
\hline
1 & 0 & \cancel{B} \circled{\cancel{C}} \circled{\cancel{D}} \\
\hline
2 & 2 & \cancel{B} \circled{C} \circled{D} \\
\hline
3 & 1 & \circled{A} \cancel{C} \\
\hline
4 & 0 & \circled{\cancel{B}} \cancel{C} \cancel{D} \\
\hline
5 & 1 & \circled{B} \cancel{C} \cancel{D} \\
\hline
\end{tabular}
\quad
\begin{tabular}{|c|l|}
\hline
\multicolumn{2}{|c|}{Employees} \\
\hline
Name & shift\\
\hline
A & \circled{3} \\
\hline
B & \circled{\cancel{4}} \circled{5} \\
\hline
C & \circled{\cancel{1}} \circled{2} \cancel{3} \cancel{4} \cancel{5}\\
\hline
D & \circled{\cancel{1}} \circled{2} \cancel{4} \cancel{5}\\
\hline
\end{tabular}
\quad
\begin{tabular}{|c|c|}
\hline
\multicolumn{2}{|c|}{Matchings} \\
\hline
A & 3 \\
\hline
B & 5 \\
\hline
C & 2 \\
\hline
D & 2 \\
\hline
\end{tabular} \\[12pt]

6) As with 4) shift $2$ has not filled its minimum, therefore employee $D$ swaps to shift $2$ leaving shift $1$. $D$ cannot be swapped to shifts $4$ or $5$ since they are lower in its preferences, and $D$ is already matched to a shift with a minimum $> 0$.
\end{center}

The resulting matchings follow the CSP modelled in section 2.2.1. We see all shifts with a minimum of 0 are not filled, since they are unessential. Shifts with a minimum $> 0$ are filled until $S_{memployee} \geq S_{min}$.


\section{Combining Preferences}
\label{section:combining-preferences}

For an example employee $e$ and example shift $s$ the following shows how we can combine preferences with ratings as described in section 2.3.1.

\begin{center}
\begin{tabular}{|c|c|c|c|c|c|c|c|c|}
\hline
$P(s)$ & 1 & 0.88 & 0.75 & 0.63 & 0.5 & 0.38 & 0.25 & 0.13 \\
\hline
Index & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 \\
\hline
$e_{shift}$ & 1 & 2 & 4 & 5 & 7 & 8 & 3 & 6 \\
\hline
$s_{rating}$ & 3 & 5 & 4 & 2 & 3 & 1 & 4 & 4 \\
\hline
New $s_{rating}$ & 3 & 4.4 & 3 & 1.26 & 1.5 & 0.38 & 1 & 0.52 \\
\hline
\end{tabular}
\end{center}

We show the old and new $e_{shift}$ next to each other, to demonstrate how the combination creates a better preference set, more in-line with the employees actual preferences. 

\begin{center}
\begin{tabular}{|c|c|c|c|c|c|c|c|c|}
\hline
Old $e_{shift}$ & 1 & 2 & 4 & 5 & 7 & 8 & 3 & 6 \\
\hline	
New $e_{shift}$ & 2 & 1 & 4 & 7 & 5 & 3 & 6 & 8 \\
\hline
\end{tabular}
\end{center}

\chapter{Figures and Tables}

\section{UML diagrams}
\label{uml-diagrams}

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.8\linewidth}
    \includegraphics[width=\linewidth]{images/ERD.png}
  \end{subfigure}
  
  \caption{Entity relationship diagram (ERD) for the implemented database.}
  \label{ERD}
\end{figure}

\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.7\linewidth}
    \includegraphics[width=\linewidth]{images/class-diagram.png}
  \end{subfigure}
  
  \caption{UML class diagram for the models used in the modified gale shapely algorithm}
  \label{uml-class-diagram}
\end{figure}

\newpage
\section{Software File Structure}
\label{section:software-file-structure}

\begin{figure}[h]
\centering
\begin{minipage}{6cm}
\dirtree{%
.1 Web Application.
.2 algorithms/.
.3 tests/.
.3 modified\_gale\_shapely.
.3 collaborative\_filtering.
.2 web\_app/.
.3 front-end/.
.3 back-end/.
}
\end{minipage}
\caption{A simplified version of the system file structure}
\label{fig:file-structure}
\end{figure}

\textit{Figure \ref{fig:file-structure}} is the whole file structure for the applications discussed throughout this project. Because each component: back-end, front-end and algorithms was stored in a single git repository (see \textit{Appendix \ref{section:source-code}}, a histochemical approach to component separation was considered.

\forceindent Algorithms discussed in section 3.2.4 are stored under the \textbf{algorithms/} directory. The front-end application discussed in section 3.2.3 are stored under the \textbf{web\_app/front-end/} directory. The back-end application discussed in section 3.2.2 are stored under the \textbf{web\_app/back-end/} directory.

\clearpage
\begin{figure}[h]
\centering
\begin{minipage}{6cm}
\dirtree{%
.1 Back-end application.
.2 app/.
.3 Http/.
.4 Controllers/.
.4 Middleware/.
.3 Models/.
.3 Validators/.
.2 database/.
.3 migrations/.
.2 public/.
.2 resources/.
.2 routes/.
}
\end{minipage}
\caption{A simplified version of the back-end, Laravel file structure}
\label{fig:laravel-file-structure}
\end{figure}

In \textit{Figure \ref{fig:laravel-file-structure}}, \textbf{Controllers/} stores the controller for each model under \textbf{Models/} that is where the bulk process of data to and from the database takes place, including queries and validation. Custom validation rules are stored under \textbf{Validators/}. Each model represent an entity in the database that are defined under \textbf{migrations/}. Here, methods to rollback and restore the database are also present in the case of schema changes. Each API and web routes defined in \textit{Appendix \ref{back-end-routes}} are stored under the \textbf{routes/} directory and point to an associated controller. The middleware and security protocols discussed in section 3.2.2 are stored under \textbf{Middleware/}. The un-compiled front-end application (JS) is stored under \textbf{resources/}, and the compiled, minified JS for the front-end, as well as images used are stored under \textbf{public/}.


\begin{figure}[h]
\centering
\begin{minipage}{6cm}
\dirtree{%
.1 Front-end application.
.2 auth/.
.2 components/.
.2 pages/.
.2 utils/.
.2 svg/.
}
\end{minipage}
\caption{A simplified version of the front-end, react file structure}
\label{fig:react-file-structure}
\end{figure}

In \textit{Figure \ref{fig:react-file-structure}}, each component used by the pages are stored in \textbf{components/}, and the pages in \textbf{pages/}. \textbf{utils/} contains helper functions and an axios API file for making requests to the back-end. \textbf{auth/} has the employee global context. \textbf{svg/} stores react-svgr images.

\begin{figure}[h]
\centering
\begin{minipage}{6cm}
\dirtree{%
.1 Algorithms application.
.2 tests/.
.4 data/.
.2 env/.
.2 modified\_gale\_shapely.py.
.2 collaborative\_filtering.py.
.2 models.py.
.2 working.py.
}
\end{minipage}
\caption{Algorithms API and implementations file structure}
\label{fig:algorithms-file-structure}
\end{figure}

For \textit{Figure \ref{fig:algorithms-file-structure}}, unit testing seen in section 4.1 is stores under \textbf{tests/}. \textit{models.py} is an ORM for the data sent to the API, and \textit{working.py} contains the fastApi interface and routes.

\clearpage
\section{Back-end Application Routes}
\label{back-end-routes}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure to show admin only API routes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1.0\linewidth}
    \includegraphics[width=\linewidth]{images/admin-routes.png}
  \end{subfigure}
  
  \caption{Admin only routes for the back-end application}
  \label{admin-routes}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure to show employee/admin API routes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1.0\linewidth}
    \includegraphics[width=\linewidth]{images/employee-routes.png}
  \end{subfigure}
  
  \caption{Employee and admin routes for the back-end application}
  \label{employee-routes}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure to show public API routes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1.0\linewidth}
    \includegraphics[width=\linewidth]{images/public-routes.png}
  \end{subfigure}
  
  \caption{Public routes for the back-end application}
  \label{public-routes}
\end{figure}


\clearpage
\section{Middleware Implementation}
\label{middleware-implementation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure to show rolecheck middleware
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.75\linewidth}
    \includegraphics[width=\linewidth]{images/role-middleware.png}
  \end{subfigure}
  
  \caption{EmployeeRoleMiddleware class for validating if an employee has a role from a given set of roles}
  \label{role-middleware}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure to show hasroles() method in Employee model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.8\linewidth}
    \includegraphics[width=\linewidth]{images/employee-hasroles.png}
  \end{subfigure}
  
  \caption{Employee hasRoles() method for checking if an employee has a certain role}
  \label{employee-hasroles}
\end{figure}

\clearpage
\section{Token Assignment}
\label{token-assignment}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure to show token assignment in login controller
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{0.8\linewidth}
    \includegraphics[width=\linewidth]{images/token-assignment.png}
  \end{subfigure}
  
  \caption{Login method ans assigning employee tokens for authentication with Sanctum middleware}
  \label{sanctum-token}
\end{figure}

\newpage


\section{Front-end views}
\label{front-end-views}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure to show the manager dashboard
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1.0\linewidth}
    \includegraphics[width=\linewidth]{images/views/manager-dashboard.png}
  \end{subfigure}
  
  \caption{Manager dashboard view}
  \label{manager-dashboard}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure to show the employee management dashboard 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1.0\linewidth}
    \includegraphics[width=\linewidth]{images/views/employee-management-dashboard.png}
  \end{subfigure}
  
  \caption{Employee Management view}
  \label{employee-management-dashboard}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure to show the department management dashboard
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1.0\linewidth}
    \includegraphics[width=\linewidth]{images/views/department-management-dashboard.png}
  \end{subfigure}
  
  \caption{Department management view}
  \label{department-management-dashboard}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure to show the employee dashboard
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1.0\linewidth}
    \includegraphics[width=\linewidth]{images/test-user.png}
  \end{subfigure}
  
  \caption{Regular employee dashboard with shifts matched to employees}
  \label{employee-dashboard}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure to show the employee dashboard with rota rating faces
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1.0\linewidth}
    \includegraphics[width=\linewidth]{images/views/employee-dashboard-faces.png}
  \end{subfigure}
  
  \caption{Regular employee dashboard with rota rating faces}
  \label{employee-dashboard-faces}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Figure to show the employee dashboard with shift rating faces
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h!]
  \centering

  \begin{subfigure}[b]{1.0\linewidth}
    \includegraphics[width=\linewidth]{images/views/employee-dashboard-shift-rating.png}
  \end{subfigure}
  
  \caption{Regular employee dashboard with shift information and rating faces}
  \label{employee-dashboard-shift-rating}
\end{figure}


\chapter{Project Information Sheet}
\label{appendix:participant-infomation-sheet}

\includegraphics[scale=0.8,page=1]{consent-forms/ethicsUserTesting.pdf}
\newpage
\includegraphics[scale=0.8,page=2]{consent-forms/ethicsUserTesting.pdf}


\chapter{User Testing Quantitative Results}
\label{appendix:user-testing-results}

\begin{figure}[h!]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
Preference 1 & Preference 2 & Preference 3 & Preference 4 & Day 1 Shift & day 2 Shift & Day 3 Shift \\
\hline
Morning & Afternoon & Evening & & 10:00-15:00 & 07:00 - 15:00 & 12:00 - 17:00 \\
\hline
Morning & Afternoon & Evening & Night & 07:00 - 15:00 & 07:00 - 15:00 & 09:00 - 12:00 \\
\hline
Afternoon & Morning & Evening & Night & 07:00 - 10:00 & 07:00 - 10:00 & 09:00 - 12:00 \\
\hline
Afternoon & Morning & & & 10:00 - 15:00 & 07:00 - 15:00 & 09:00 - 12:00 \\
\hline
Afternoon & Morning & & & 07:00 - 10:00 & 07:00 - 10:00 & 09:00 - 12:00 \\
\hline
Morning & Afternoon & Evening & & 07:00 - 15:00 & 07:00 - 15:00 & 12:00 - 17:00\\
\hline
Morning & Afternoon & Evening & Night & 10:00 - 15:00 & 07:00 - 10:00 & 09:00 - 12:00 \\
\hline
Afternoon & Morning & Evening & & 10:00 - 15:00 & 07:00 - 15:00 & 12:00 - 17:00 \\
\hline
Morning & Afternoon & & & 07:00 - 10:00 & 07:00 - 10:00 & 09:00 - 12:00 \\
\hline
Afternoon & Evening & Morning & Night & 10:00 - 15:00 & 07:00 - 15:00 & 12:00 - 17:00 \\
\hline
Afternoon & Morning & Evening & Night & 10:00 - 15:00 & 15:00 - 22:00 & 12:00 - 17:00 \\
\hline
Morning & Afternoon & & & 07:00 - 15:00 & 07:00 - 15:00 & 09:00 - 12:00 \\
\hline
\end{tabular}
\caption{Participants availability preference and select shifts for each of the 3 days}
\end{figure}

\begin{figure}[h!]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|c|c|}
\hline
Start & 06:00 & 07:00 & 07:00 & 09:00 & 10:00 & 12:00 & 15:00 & 17:00 \\
\hline
End & 09:00 & 10:00 & 15:00 & 12:00 & 15:00 & 17:00 & 22:00 & 22:00 \\
\hline
& 1 & 2 & 2 & 5 & 5 & 5 & 2 & 2 \\
\hline
& 3 & 4 & 4 & 3 & 4 & 3 & 2 & 2 \\
\hline
& 1 & 5 & 3 & 5 & 4 & 3 & 1 & 1 \\
\hline
& 2 & 4 & 5 & 5 & 5 & 4 & 1 & 1 \\
\hline
& 4 & 5 & 4 & 5 & 5 & 4 & 3 & 4 \\
\hline
& 4 & 4 & 5 & 4 & 5 & 5 & 2 & 1 \\
\hline
& 3 & 4 & 4 & 5 & 4 & 3 & 1 & 1 \\
\hline
& 2 & 3 & 4 & 4 & 4 & 5 & 3 & 2 \\
\hline
& 3 & 4 & 2 & 5 & 4 & 3 & 2 & 3 \\
\hline
& 2 & 3 & 3 & 5 & 5 & 5 & 1 & 1 \\
\hline
& 1 & 3 & 3 & 3 & 2 & 4 & 4 & 5 \\
\hline
& 1 & 2 & 4 & 4 & 4 & 4 & 1 & 1 \\
\hline
\end{tabular}
\caption{Participants ratings of each of the 8 unique shifts}
\end{figure}


\end{appendices}