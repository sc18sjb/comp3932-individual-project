\BOOKMARK [0][-]{chapter.1}{Motivation and Background Research}{}% 1
\BOOKMARK [1][-]{section.1.1}{Motivation}{chapter.1}% 2
\BOOKMARK [1][-]{section.1.2}{Problem Statement}{chapter.1}% 3
\BOOKMARK [1][-]{section.1.3}{Background Research}{chapter.1}% 4
\BOOKMARK [2][-]{subsection.1.3.1}{Stable-marriage problem}{section.1.3}% 5
\BOOKMARK [2][-]{subsection.1.3.2}{Recommender Systems}{section.1.3}% 6
\BOOKMARK [2][-]{subsection.1.3.3}{Evaluation Techniques}{section.1.3}% 7
\BOOKMARK [2][-]{subsection.1.3.4}{Integer Programming \(IP\) Model Formulation}{section.1.3}% 8
\BOOKMARK [2][-]{subsection.1.3.5}{Existing Solutions}{section.1.3}% 9
\BOOKMARK [2][-]{subsection.1.3.6}{Existing Deployed Applications}{section.1.3}% 10
\BOOKMARK [2][-]{subsection.1.3.7}{Datasets}{section.1.3}% 11
\BOOKMARK [0][-]{chapter.2}{Designing a Solution}{}% 12
\BOOKMARK [1][-]{section.2.1}{Modelling the problem}{chapter.2}% 13
\BOOKMARK [1][-]{section.2.2}{Creating a Stable Matching}{chapter.2}% 14
\BOOKMARK [2][-]{subsection.2.2.1}{Additional Constraints}{section.2.2}% 15
\BOOKMARK [2][-]{subsection.2.2.2}{Modified Gale Shapely}{section.2.2}% 16
\BOOKMARK [1][-]{section.2.3}{Creating a recommendation engine}{chapter.2}% 17
\BOOKMARK [2][-]{subsection.2.3.1}{Combining ratings}{section.2.3}% 18
\BOOKMARK [2][-]{subsection.2.3.2}{Collecting Ratings}{section.2.3}% 19
\BOOKMARK [2][-]{subsection.2.3.3}{Algorithms}{section.2.3}% 20
\BOOKMARK [0][-]{chapter.3}{Implementing the Web Application}{}% 21
\BOOKMARK [1][-]{section.3.1}{System Architecture}{chapter.3}% 22
\BOOKMARK [2][-]{subsection.3.1.1}{Designing a Database:}{section.3.1}% 23
\BOOKMARK [2][-]{subsection.3.1.2}{Software Structure}{section.3.1}% 24
\BOOKMARK [1][-]{section.3.2}{Implementation}{chapter.3}% 25
\BOOKMARK [2][-]{subsection.3.2.1}{Version Control}{section.3.2}% 26
\BOOKMARK [2][-]{subsection.3.2.2}{Back-end Implementation}{section.3.2}% 27
\BOOKMARK [2][-]{subsection.3.2.3}{Front-end \(client\) Implementation}{section.3.2}% 28
\BOOKMARK [2][-]{subsection.3.2.4}{Algorithm's Implementation}{section.3.2}% 29
\BOOKMARK [0][-]{chapter.4}{Results and Evaluation}{}% 30
\BOOKMARK [1][-]{section.4.1}{Unit Testing}{chapter.4}% 31
\BOOKMARK [1][-]{section.4.2}{User Evaluation}{chapter.4}% 32
\BOOKMARK [2][-]{subsection.4.2.1}{Participants}{section.4.2}% 33
\BOOKMARK [2][-]{subsection.4.2.2}{Procedure}{section.4.2}% 34
\BOOKMARK [2][-]{subsection.4.2.3}{Results}{section.4.2}% 35
\BOOKMARK [0][-]{chapter.5}{Discussion and Conclusion}{}% 36
\BOOKMARK [1][-]{section.5.1}{Problems with the Experiment}{chapter.5}% 37
\BOOKMARK [1][-]{section.5.2}{Conclusions}{chapter.5}% 38
\BOOKMARK [2][-]{subsection.5.2.1}{Web Application Reflection}{section.5.2}% 39
\BOOKMARK [2][-]{subsection.5.2.2}{Shift Schedule Reflection}{section.5.2}% 40
\BOOKMARK [1][-]{section.5.3}{Ideas for future work}{chapter.5}% 41
\BOOKMARK [0][-]{subsubsection*.69}{References}{}% 42
\BOOKMARK [0][-]{section*.71}{Appendices}{}% 43
\BOOKMARK [0][-]{appendix.a.A}{Self-appraisal}{}% 44
\BOOKMARK [1][-]{section.a.A.1}{Critical self-evaluation}{appendix.a.A}% 45
\BOOKMARK [2][-]{subsection.a.A.1.1}{What Went Well}{section.a.A.1}% 46
\BOOKMARK [2][-]{subsection.a.A.1.2}{What Could be Improved}{section.a.A.1}% 47
\BOOKMARK [1][-]{section.a.A.2}{Personal reflection and lessons learned}{appendix.a.A}% 48
\BOOKMARK [1][-]{section.a.A.3}{Legal, social, ethical and professional issues}{appendix.a.A}% 49
\BOOKMARK [2][-]{subsection.a.A.3.1}{Legal issues}{section.a.A.3}% 50
\BOOKMARK [2][-]{subsection.a.A.3.2}{Social issues}{section.a.A.3}% 51
\BOOKMARK [2][-]{subsection.a.A.3.3}{Ethical issues}{section.a.A.3}% 52
\BOOKMARK [2][-]{subsection.a.A.3.4}{Professional issues}{section.a.A.3}% 53
\BOOKMARK [0][-]{appendix.a.B}{External Material}{}% 54
\BOOKMARK [1][-]{section.a.B.1}{Gale-Shapely Algorithm}{appendix.a.B}% 55
\BOOKMARK [1][-]{section.a.B.2}{External Libraries}{appendix.a.B}% 56
\BOOKMARK [2][-]{subsection.a.B.2.1}{Back-End Libraries}{section.a.B.2}% 57
\BOOKMARK [2][-]{subsection.a.B.2.2}{Front-End Libraries}{section.a.B.2}% 58
\BOOKMARK [2][-]{subsection.a.B.2.3}{Algorithms Libraries}{section.a.B.2}% 59
\BOOKMARK [0][-]{appendix.a.C}{URL's and Deployed Applications}{}% 60
\BOOKMARK [1][-]{section.a.C.1}{Source Code}{appendix.a.C}% 61
\BOOKMARK [1][-]{section.a.C.2}{Soft-deployed Applications}{appendix.a.C}% 62
\BOOKMARK [1][-]{section.a.C.3}{Questionnaire}{appendix.a.C}% 63
\BOOKMARK [0][-]{appendix.a.D}{Example Solutions}{}% 64
\BOOKMARK [1][-]{section.a.D.1}{Example Modified Gale-Shapely Algorithm Walkthrough}{appendix.a.D}% 65
\BOOKMARK [1][-]{section.a.D.2}{Combining Preferences}{appendix.a.D}% 66
\BOOKMARK [0][-]{appendix.a.E}{Figures and Tables}{}% 67
\BOOKMARK [1][-]{section.a.E.1}{UML diagrams}{appendix.a.E}% 68
\BOOKMARK [1][-]{section.a.E.2}{Software File Structure}{appendix.a.E}% 69
\BOOKMARK [1][-]{section.a.E.3}{Back-end Application Routes}{appendix.a.E}% 70
\BOOKMARK [1][-]{section.a.E.4}{Middleware Implementation}{appendix.a.E}% 71
\BOOKMARK [1][-]{section.a.E.5}{Token Assignment}{appendix.a.E}% 72
\BOOKMARK [1][-]{section.a.E.6}{Front-end views}{appendix.a.E}% 73
\BOOKMARK [0][-]{appendix.a.F}{Project Information Sheet}{}% 74
\BOOKMARK [0][-]{appendix.a.G}{User Testing Quantitative Results}{}% 75
