%
% Motivation / Introduction
%
\section{Introduction}

%<A brief overview of your project suitable for a non-specialised audience. %You may mention background and motivation, but should avoid technical terms %as far as possible. 1-2 paragraphs should be sufficient. Here is an example %of how to cite a reference \cite{parikh1980adaptive}.>


In this project, the author wants to tackle a problem that keeps arising during part-time work; namely those on zero-hour contracts. These type of jobs always face difficulties timetabling a rota based upon everyone's availability, which is particularly difficult since employees can work different times every day/every week, and shifts need to be able to adapt quickly based upon availability. Hence, most companies opt for outdated and cumbersome paper rota's which are filled out manually by managers or employees and are changed simply by crossing out or writing over names. This is a highly inefficient way of scheduling and can lead to `hours of valuable time lost' and `no guarantee that the data you'll be working with is 100\% accurate' \cite{rotacloud}.

The project will aim to address these challenges by creating a web interface that allows for automatic shift scheduling to create a digital rota. The project will investigate an optimal scheduling algorithm to create the most desirable rota based upon employee preferences such as preferred time of day, departments they can work in, length of shift and so on. Employees will be able to rank rota outcomes, to state if their scheduled shifts met their needs, and an exploration of pattern recognition algorithms will be used to match preferred shifts with preferences, ensuring the employee gets the shift best fit for them.


%
% Aims, Objectives and deliverables.
%
\section{Aims, Objectives and Delievarables}

\subsection{Project Aims}

%<A brief summary of the primary aims of this project. Typically 1-3 %sentences.>

The project aims to create an optimal digital rota based upon employee preferences and shift pattern recognition. The project aims to create a web interface to display the digital rota in an easy to use and intuitive manner. 

\subsection{Objectives}

%<A list of what you want to achieve {\em by the end of the project} -- note %this means `learn how to\ldots', `research into\ldots` are {\em not} %objectives, as they are intermediate milestones rather than final goals. %All objectives should be measurable, {\em i.e.} it should be possible to %provide evidence to confirm whether or not they have been achieved. 3-5 %objectives is typical.>

\begin{itemize}
	\item Create a model to represent a rota that allows multiple shifts to be assigned to multiple employees in multiple departments.
	\item Create a shift scheduler that can create a rota for $x$ shifts, $n$ employees and $t$ departments so that all shifts are covered.
	\item Create an intuitive web interface to display and manipulate rota's.
	\item Use pattern recognition and ratings to create an \textit{optimal} rota that establishes when employees prefer to work.
\end{itemize}

\subsection{Deliverables}

%<A list of what you will hand in at the end of the project. This will %include the final report (possibly spread across multiple deliverables, if %that makes sense for your project), code (possibly more than one version), %and so on. Ideally the deliverables should be cross-referenced to the %objectives. 2-3 deliverables is typical, but there can be more depending on %the nature of the project.>
\begin{itemize}
	\item Final report explaining the research, implementation and evaluation of the project.
	\item Python script(s) for for solving the scheduling problem
	\item Web server and client for running the scheduling problem
	\item Deployed web service for running the scheduling problem online
	\item READMEs with project installation and running instructions 
\end{itemize}

%
% Plan
%
\section{Project Plan}

%<Provide a plan for the full project, from when you started until the submission of %the final report. This should discuss the key stages of your project.>

The author has planned from the offset of the project to focus primarily on the research and implementation of the scheduling algorithms, over the creation of the web application. It is planned to create a simple web interface quickly to accommodate more time to focus on the scheduling algorithms. The interface can then be developed further, later in the projects development cycle. The following order of development has been created to ensure the objectives of the project can be met on time:

\begin{enumerate}
	\item Research and implement basic scheduling algorithms
	\item Create very simply web application to display and interact with results
	\item Research into pattern recognition and advanced scheduling algorithms
	\item Further develop web application to facilitate the advanced scheduling algorithms
\end{enumerate}

\noindent
The following key stages of the project has been created to ensure development runs on time from the inception of the project to the submission of the final report.

\begin{itemize}
	\item \textbf{Project Inception and Plan:} Brainstorming ideas for what to focus on in the project, creating the aim and main objectives. Investigation into existing applications can then be carried out. The initial supervisor meeting will also take place in this stage. This stage has already been completed.
	\item \textbf{Initial Research and Basic Implementation:} Initial research and investigation into existing algorithms such as Gale-shapely \cite{galeShapely} and constraint satisfaction problem \cite{constraintSatisfation}. Development of application model and basic implementation prototype of the scheduling problem. Development of the basic web application can be pursued given a working prototype is found. This stage has already been completed.   
	\item \textbf{Further Research:} Further research into existing and advanced algorithms for identifying patterns in structured data. Further development of application prototype to reflect this research. The first sections of the final report: background research and introduction can also be completed at this stage.
	\item \textbf{Full Functionality:} Development of the basic web application into a more advanced, feature-rich application and (given successful development) deployment. If the author feels the scheduling problem has not been fully addressed by this stage, more time and research will be assigned to that instead.
	\item \textbf{Testing:} Real-world user testing will be carried out at the stage including an ethical investigation. Changes to the web application to reflect feedback can be promptly made. Later sections of the final report: design and implementation and first drafts can be completed at this stage.
	\item \textbf{Submission:} Finishing all sections of the final report and polishing it. Critical review of the application and submission will take place at this stage.
\end{itemize}

\subsection{Timeline}

%<A graphical description of your plan, often as a Gantt chart.>

The author has created a gantt chart detailing a graphical description of the project plan. Bars in green represent stages of the project already completed. Bars in blue are stages yet to be complete.

% To include a figure, uncomment and modify the following. Vary the 'width'
% field until it looks suitable. This uses the 'graphicx' module that has
% already been included in 'config.tex'.
\begin{figure}[htbp]	
	\centerline{
		\includegraphics[width=18cm]{gantt.png}
	}
	\caption{Project plan gantt chart made with Teamgantt}
\end{figure}



%
% Risk mitigation.
%
\section{Risk Mitigation}

%<Identify risks to your project, and what you would do if they arose.>

One of the most significant risks of this project is the plausibility that no test users would be willing to step forward and trial the web application which would lead to a direct knock-on affect of having no real-world test data to work with. This makes it harder for the author to identify bugs in the application and fallacy's with the algorithms. The author has identified that this risk can be mitigated by ensuring a close group of friends or family who are willing to step in and act as test users, in the off-chance no one else is.

Another notable risk in the project is the advent of scope creep. The author has lots of propositions for the project beyond the initial aim and objectives, likely due to this being their own project proposal. If not mitigated, this risk can lead to growth in the scale of the project and an unrealistic schedule to follow. The risk can be mitigated by strictly following the project plan in the above section and focussing solely on the objectives set out in section 2. The author will allow extra time at the end of the project to further mitigate the risk, and to accommodate for any unforeseen setbacks.

%
% Ethics.
%
\section{Ethics}

%<If your project has ethical issues (e.g. gathering of user consent forms), then %you should state here how you intend to address them. If there are no ethical %issues then explicitly state: "There are no ethical issues for this project.">

The author expresses the inclusion of ethical issues in the need to gather user data for real-world user testing to take place. Where this is the case, subject to GDPR \cite{gdpr} practices, data will be stored transparently using appropriate security measures; encrypted with backups. None of this data will be made accessible on the GitLab repositories and will only be accessible by authorised members (such as the author). Data will be used for the intended purpose only and any participating users will asked to fill out a prior consent form allowing them to opt-in and out of the test at any moment. Where data is used outside of the intended purpose, further user content will need to be obtained beforehand.

Where the web application is deployed online, a cookies policy will be written to enforce what, how and why users data will be collected, and where (if) users data is sent to external sites. Users will easily be able to opt-in and reject these cookies through a simple web interface. Additionally, a terms of service will need to be agreed upon by any new users signing up to the web application and this will be easily identifiable through the graphical interface. Both the cookies policy and terms of service will need to be provided in accessible formats.