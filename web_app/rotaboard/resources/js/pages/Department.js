import { useEffect, useState } from "react";
import api from '../utils/api';
import SvgEdit from '../svg/Edit';
import CloseButton from 'react-bootstrap/CloseButton';
import CreateDepartmentModal from "../components/CreateDepartmentModal";
import SubDashboardTopBar from "../components/SubDashboardTopBar";
import LoadingOverlay from 'react-loading-overlay';
LoadingOverlay.propTypes = undefined
import HashLoader from 'react-spinners/HashLoader'
import { useLocation } from "react-router-dom";

export default function Department() {
    const [department, setDepartment] = useState({});
    const [departments, setDepartments] = useState([]);
    const [createDepartmentModalShow, setCreateDepartmentModalShow] = useState(false);
    const [loading, setLoading] = useState(true);
    const { state } = useLocation();

    useEffect(() => {
        const getDepartments = async () => {
            const departments = await api.fetchDepartments();
            setDepartments(departments);
            setLoading(false);
        }

        getDepartments();

        // Show the modal if the modal state is set
        if (state && state.modal && !loading) {
            setCreateDepartmentModalShow(true);
        }
    }, [loading])

    const handleNewDepartment = () => {
        setDepartment({});
        setCreateDepartmentModalShow(true);
    }

    const handleEditDepartment = (e, department) => {
        e.preventDefault();
        setDepartment(department);
        setCreateDepartmentModalShow(true);
    }

    // Delete a department from the list.
    const handleDeleteDepartment = async (e, department) => {
        e.preventDefault();
        const res = await api.deleteDepartment(department.id);

        if (res.success) {
            const remainingDepartments = departments.filter((e) => e.id !== department.id);
            setDepartments(remainingDepartments);
        } else {
            console.log(res);
        }
    };

    return (
        <div id='departments'>
            <LoadingOverlay
                active={loading}
                spinner={<HashLoader color={"#ffed4a"} size={50}/>}
            >
                <SubDashboardTopBar
                    addNew={handleNewDepartment}
                />

                <table className='table data-table sub-table'>
                    <thead>
                        <tr>
                            <th className='sub-table__cell'>Name</th>
                            <th className='sub-table__cell sub-table__cell--edit'></th>
                            <th className='sub-table__cell sub-table__cell--delete'></th>
                        </tr>
                    </thead>

                    <tbody>
                        {departments.map((department, i) => {
                            return (
                                <tr key={i}>
                                    <th className='sub-table__cell'>
                                        <span style={{backgroundColor: department.color}} className='color-dot'></span>
                                        {department.name.charAt(0).toUpperCase() + department.name.slice(1)}
                                    </th>
                                    <th className='sub-table__cell sub-table__cell--edit'>
                                        <div className='d-flex justify-content-center align-items-center'>
                                            <SvgEdit className='sub-table__edit-btn' onClick={(e) => handleEditDepartment(e, department)} />
                                        </div>
                                    </th>
                                    <th className='sub-table__cell sub-table__cell--delete'>
                                        <div className='d-flex justify-content-center align-items-center'>
                                            <CloseButton className='sub-table__delete-btn' onClick={(e) => handleDeleteDepartment(e, department)} />
                                        </div>
                                    </th>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </LoadingOverlay>

            <CreateDepartmentModal 
                department={department}
                departments={departments}
                setDepartments={setDepartments}
                show={createDepartmentModalShow}
                onHide={() => setCreateDepartmentModalShow(false)}
            />
        </div>
    );
}