import { useCallback, useEffect, useState, useContext, useRef } from "react";
import api from '../utils/api';
import DashboardTopBar from '../components/DashboardTopBar';
import helpers from '../utils/helpers';
import ShiftDashboard from "../components/ShiftDashboard";
import EmployeeDashboard from "../components/EmployeeDashboard";
import TextModal from "../components/TextModal";
import LoadingOverlay from 'react-loading-overlay';
LoadingOverlay.propTypes = undefined
import HashLoader from 'react-spinners/HashLoader'
import EmployeeContext from "../auth/EmployeeContext";
import moment from 'moment';

/**
 * Get the weekly dates for each day of the week the company operates, for a given date.
 * 
 * @param {array} days 
 * @param {string} date 
 */
function setDayDates(days, date) {
    if (days.length <= 0) return days;
    var now = (date === undefined) ? new Date : new Date(date);
    now.setDate(now.getDate() - (now.getDay() == 0 ? 6 : now.getDay() - 1));

    for (var i = 0; i <= 6; i++) {
        var d = new Date(now.setDate((now.getDate() - (now.getDay() - 1)) + i));

        days.some((e, k, arr) => {
            if (e.day == d.getDay()) {
                days[k]['date'] = d;
                return; // We return since we have found our match.
            }
        });
    }
}


/**
 * Get each of the hours of the day in either 24 or 12 hours format
 * 
 * @param {int} format 
 * @returns {array}
 */
function getHours(format=12) {
    var times = []
    for (var i = 0; i < 24; i++) {
        const hourFormat = (format = 12) ? 'h a' : 'HH';
        times.push(moment(i, 'H').format(hourFormat));
    }
    return times;
}

export default function Dashboard() {
    const [weekDays, setWeekDays] = useState([])
    const [hours, setHours] = useState(getHours());
    const [currentDate, setCurrentDate] = useState(new Date);
    const [shifts, setShifts] = useState([]);
    const [employees, setEmployees] = useState([]);
    const [departments, setDepartments] = useState([]); // All the departments.
    const [department, setDepartment] = useState({ // The current department.
        'id': 0,
        'name': 'All Departments'
    }); // Default 0 means 'all' departmnts.
    const [showEmployeeDashboard, setShowEmployeeDashboard] = useState(false);
    const [loading, setLoading] = useState(true);
    const [textModalShow, setTextModalShow] = useState(false);
    const [textModalContent, setTextModalContent] = useState({
        'title': '',
        'content': ''
    });
    const didMountRef = useRef(false);
    const user = useContext(EmployeeContext);

    useEffect(() => {
        // Only need to fetch the departments on load.
        const getDepartments = async () => {
            const departments = await api.fetchDepartments();
            setDepartments(departments);
        }

        // We only call this on load, since we don't need to get the employees over and over
        const getLoadData = async () => {
            // We can get employees and days at the same time.
            let [employees, days] = await Promise.all([
                api.fetchEmployees([], []),
                getDays() 
            ]);

            const shifts = await getShifts(days);
            employees = await fetchEmployeeShifts(shifts, employees);

            setEmployees(employees);
            setWeekDays(days);
            setShifts(shifts);
            setLoading(false);
        }

        // Called on all subsiquent calls (apart from first).
        if (didMountRef.current) 
            getDateChangeData(true);
        // Called only on the first call.
        else {
            getDepartments();
            getLoadData();
        }

        didMountRef.current = true;
    // We update when the date is changed or the department is changed.
    }, [currentDate, department]);

    // We need to check the employee roles every time it changes.
    useEffect(() => {
        if (user.hasRoles(['employee'])) 
            setShowEmployeeDashboard(true);
    }, [user]);

    // Get all the days the company operates for showing as the x axis of the dashboard.
    const getDays = useCallback(async () => {
        const days = await api.fetchDays();
        setDayDates(days, currentDate); // Set the weekly dates.
        return days;
    }, [currentDate]);

    // Get all the shifts to show within the dashboard.
    const getShifts = async (days) => {
        if (days.length <= 0) return [];
        const [weekStart, weekEnd] = helpers.getWeekStartEnd(days);        
        const shifts = await api.fetchShifts(weekStart, weekEnd, department.id);
        return shifts;
    }

    // Each time the departments or weekdays change, we call this again.
    const getDateChangeData = async (updateEmployeeShifts = false) => {
        const days = await getDays();
        const shifts = await getShifts(days);

        setWeekDays(days);
        setLoading(false);

        if (showEmployeeDashboard || updateEmployeeShifts) {
            const employeesShifts = await fetchEmployeeShifts(shifts, employees);
            setEmployees(employeesShifts);
        }

        setShifts(shifts);
    }

    const fetchEmployeeShifts = async (shifts, employeeShifts) => {
        // Shallow copy.
        if (employeeShifts.length <= 0) return [];

        employeeShifts.forEach((employee) => {
            // Clear before reasign.
            employee.shifts = [];
            shifts.forEach((shift) => {
                if (shift.employee_ids != null && shift.employee_ids.split(',').includes(employee.id.toString())) {
                    employee.shifts.push(shift);
                }
            })
        });

        return employeeShifts;
    };

    const openEmployeeShifts = useCallback(async () => {
        const employeeShifts = await fetchEmployeeShifts(shifts, employees);
        setEmployees(employeeShifts);
        setShowEmployeeDashboard(true);
    }, [employees, shifts]);

    const currentMonth = () => {
        if (weekDays.length <= 0) return "";
        const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        const start = weekDays[0].date.getMonth();
        const end = weekDays[weekDays.length - 1].date.getMonth();

        if (start === end)
            return months[start];
        return months[start] + ' - ' + months[end];
    }

    const assign = async (e, reassign=false) => {
        e.preventDefault();
        const [weekStart, weekEnd] = helpers.getWeekStartEnd(weekDays);
        
        setLoading(true);
        const res = await api.assignShifts({
            start: weekStart,
            end: weekEnd
        }, reassign);

        if (res.success) {
            if (res.data > 0) 
            getDateChangeData(true);

            setTextModalShow(true);
            setTextModalContent({
                'title': 'Assignment successful',
                'content': res.message
            });
            
            // Even if the response is quicker, we settimout so the animation doesn't flicker.
            setTimeout(() => {
                setLoading(false);
            }, 200);
        } else {
            console.log(res);
            // Even if the response is quicker, we settimout so the animation doesn't flicker.
            setTimeout(() => {
                setLoading(false);
            }, 200);
        }
    }

    const deleteAssign = async (e) => {
        e.preventDefault();
        const [weekStart, weekEnd] = helpers.getWeekStartEnd(weekDays);

        setLoading(true);
        const res = await api.deleteAssigned(weekStart, weekEnd);
        if (res.success) {
            if (res.data > 0) 
                getDateChangeData(true);

            setTextModalShow(true);
            setTextModalContent({
                'title': 'Assignments removes',
                'content': res.message
            });

            setTimeout(() => {
                setLoading(false);
            }, 200);
        } else {
            setTimeout(() => {
                setLoading(false);
            }, 200);
        }
    }

    return (    
        <div id='dashboard'>
            <LoadingOverlay
                active={loading}
                spinner={<HashLoader color={"#ffed4a"} size={50}/>}
            >
                <DashboardTopBar 
                    weekDays={weekDays}
                    setCurrentDate={setCurrentDate}
                    department={department}
                    departments={departments}
                    setDepartment={setDepartment} 
                    employees={employees}
                    showEmployeeDashboard={showEmployeeDashboard}
                    onShowEmployeeDashboard={openEmployeeShifts}
                    onHideEmployeeDashboard={() => setShowEmployeeDashboard(false)}
                    onAssign={assign}
                    onDeleteAssign={deleteAssign}
                    currentMonth={currentMonth}
                    getDateChangeData={getDateChangeData}
                />
                
                {user !== undefined && (
                    <>
                        {/* If they are an adin we show the admin dashboard. */}
                        {(!showEmployeeDashboard && user.hasRoles(['admin'])) ? (
                            <ShiftDashboard 
                                weekDays={weekDays} // X axis
                                hours={hours} // Y axis
                                shifts={shifts}
                                setShifts={setShifts}
                                department={department}
                                departments={departments}
                                employees={employees}
                            />
                        ) : (
                            // Otherwise we show the employee dashboard.
                            <EmployeeDashboard
                                weekDays={weekDays}
                                employees={employees}
                                shifts={shifts}
                                setShifts={setShifts}
                                departments={departments}
                            />
                        )}
                    </>
                )}
                
            </LoadingOverlay>

            <TextModal 
                title={textModalContent.title}
                content={textModalContent.content}
                show={textModalShow}
                onHide={() => setTextModalShow(false)}
            />
        </div>
    );
}

