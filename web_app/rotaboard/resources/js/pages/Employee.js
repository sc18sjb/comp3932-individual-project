import { useContext, useEffect, useState } from "react";
import api from '../utils/api';
import SvgEdit from '../svg/Edit';
import CloseButton from 'react-bootstrap/CloseButton';
import CreateEmployeeModal from "../components/CreateEmployeeModal";
import SubDashboardTopBar from "../components/SubDashboardTopBar";
import LoadingOverlay from 'react-loading-overlay';
import EmployeeContext from "../auth/EmployeeContext";
LoadingOverlay.propTypes = undefined
import HashLoader from 'react-spinners/HashLoader'

export default function Employee() {
    const [employee, setEmployee] = useState({});
    const [employees, setEmployees] = useState([]);
    const [departments, setDepartments] = useState([]);
    const [availability, setAvailability] = useState([]);
    const [createEmployeeModalShow, setCreateEmployeeModalShow] = useState(false);
    const [loading, setLoading] = useState(true);
    const user = useContext(EmployeeContext);

    useEffect(() => {
        const getData = async () => {
            // We get the departments and availability in parallell, then get the employees.
            const [departments, availability] = await Promise.all([
                api.fetchDepartments(),
                api.fetchAvailability()
            ]);
            const employees = await api.fetchEmployees(departments, availability)

            setEmployees(employees);
            setDepartments(departments);
            setAvailability(availability);
            setLoading(false);
        }

        getData();
    }, []);

    const handleNewEmployee = () => {
        setEmployee({});
        setCreateEmployeeModalShow(true);
    };

    const handleEditEmployee = (e, employee) => {
        e.preventDefault();
        setEmployee(employee)
        setCreateEmployeeModalShow(true);
    };

    // Delete an employee from the list.
    const handleDeleteEmployee = async (e, employee) => {
        e.preventDefault();
        const res = await api.deleteEmployee(employee.id);

        if (res.success) {
            const remainingEmployees = employees.filter((e) => e.id !== employee.id);
            setEmployees(remainingEmployees);
        } else {
            console.log(res);
        }
    };

    return (
        <div id='employees'>
            <LoadingOverlay
                active={loading}
                spinner={<HashLoader color={"#ffed4a"} size={50}/>}
            >
                <SubDashboardTopBar
                    addNew={handleNewEmployee}
                />

                <table className='table data-table sub-table'>
                    <thead>
                        <tr>
                            <th className='sub-table__cell'>Name</th>
                            <th className='sub-table__cell'>Email</th>
                            <th className='sub-table__cell'>Departments</th>
                            <th className='sub-table__cell'>Availability</th>
                            <th className='sub-table__cell'>Role</th>
                            <th className='sub-table__cell sub-table__cell--edit'></th>
                            <th className='sub-table__cell sub-table__cell--delete'></th>
                        </tr>
                    </thead>

                    <tbody>
                        {employees.map((employee, i) => {
                            return (
                                <tr key={i}>
                                    <th className='sub-table__cell'>
                                        {employee.fname} {employee.lname}
                                    </th>
                                    <th className='sub-table__cell'>
                                        {employee.email}
                                    </th>
                                    <th className='sub-table__cell'>
                                        {employee.departments.map((department, i) => {
                                            return (
                                                <span style={{padding: '0 5px'}} key={i}>{department.name}</span>
                                            );
                                        })}
                                    </th>
                                    <th className='sub-table__cell'>
                                        {employee.availability.map((available, i) => {
                                            return (
                                                <span style={{padding: '0 5px'}} key={i}>{available.name}</span>
                                            );
                                        })}
                                    </th>
                                    <th className='sub-table__cell'>
                                        {employee.role}
                                    </th>
                                    <th className='sub-table__cell sub-table__cell--edit'>
                                        <div className='d-flex justify-content-center align-items-center'>
                                            <SvgEdit className='sub-table__edit-btn' onClick={(e) => handleEditEmployee(e, employee)} />
                                        </div>
                                    </th>
                                    <th className='sub-table__cell sub-table__cell--delete'>
                                        {(employee.id !== user.id) && (
                                            <div className='d-flex justify-content-center align-items-center'>
                                                <CloseButton className='sub-table__delete-btn' onClick={(e) => handleDeleteEmployee(e, employee)} />
                                            </div>
                                        )}
                                    </th>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </LoadingOverlay>

            <CreateEmployeeModal
                employee={employee}
                employees={employees}
                setEmployees={setEmployees}
                departments={departments}
                availability={availability}
                show={createEmployeeModalShow}
                onHide={() => setCreateEmployeeModalShow(false)}
            />
        </div>
    );
}