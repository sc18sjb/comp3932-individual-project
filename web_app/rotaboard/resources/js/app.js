/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import React, { useEffect, createContext, useState } from 'react';
import ReactDOM from 'react-dom';
import Dashboard from './pages/Dashboard';
import Employee from './pages/Employee';
import Department from './pages/Department';
import SideBar from './components/SideBar';
import api from './utils/api';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import EmployeeContext from './auth/EmployeeContext';
import 'react-bootstrap-range-slider/dist/react-bootstrap-range-slider.css';


/**
 * Create the root layout including the routes and sidebar that is shown on every page in the app/
 */
function App() {
    const [employee, setEmployee] = useState({})
    const contextVal = {
        ...employee,
        hasRoles: (roles) => {
            return roles.includes(employee.role);
        }
    }

    // First thing we need to do is get the current employee on load.
    useEffect(() => {
        const getCurrentEmployee = async () => {
            const employee = await api.fetchCurrentEmployee();
            setEmployee(employee);
        }

        getCurrentEmployee();
    }, []);

    return (
        <EmployeeContext.Provider value={contextVal}>
            <BrowserRouter>
                <SideBar 
                    position={'side-bar--fixed'}
                />

                <div id='container'>
                    <Routes>
                        {/* Routes everyone has access to */}
                        <Route name="dashboard" exact path='/app' element={<Dashboard />} />

                        {/* Routes only admins have access to */}
                        {contextVal.hasRoles(['admin']) && (
                            <>
                                <Route name="employee" exact path='/app/employees' element={<Employee />} />     
                                <Route name="department" exact path='/app/departments' element={<Department />} />
                            </>
                        )}
                        
                    </Routes>
                </div>
            </BrowserRouter>
        </EmployeeContext.Provider>
    );
};

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}

