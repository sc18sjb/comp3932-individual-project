import moment from 'moment';

// Helper functions.

const handleChange = (e, setState) => {
    const {name, value} = e.target;
    setState(prevState => ({
        ...prevState,
        [name]: value
    }));
};

/**
 * Get the objects from a group concat array and maintain order.
 * 
 * @param {array} arr 
 * @param {array} concatArr 
 * @returns 
 */
const matchConcatOrder = (arr, concatArr) => {
    var data = [];
    concatArr.forEach((e) => {
        data.push((arr.find((i) => i.id == e)));
    });
    return data;
};


/*
* From an array of weekdays return the first and last weekday (start and end).
*/
const getWeekStartEnd = (weekDays) => {
    const weekStart = weekDays[0].date.toISOString().split('T')[0];
    const weekEnd = weekDays[weekDays.length - 1].date.toISOString().split('T')[0];

    return [
        weekStart,
        weekEnd
    ];
};

/*
* Work out how many weeks forward or backwward (between) two different dates.
*/
const weeksBetween = (d1, d2) => {
    return Math.round((d2 - d1) / (7 * 24 * 60 * 60 * 1000));
};

const checkCurrentDate = (date) => {
    return date.toDateString() === new Date().toDateString();
}

/* 
* Calculate the length in hours between two times.
*/
const calculateLength = (startTime, endTime) => {
    const start = moment(startTime, 'HH:mm');
    const end = moment(endTime, 'HH:mm');

    if (end.isBefore(start))
        end.add(1, 'day');

    return moment.duration(end.diff(start)).asHours();
};

/*
* Convert seconds to hours (if any) and minutes.
*/
const secondsToHours = (seconds) => {
    let h = Math.floor(seconds / 3600),
        m = Math.floor(seconds % 3600 / 60);
    return (h > 0 ? h + 'h ' : '') + (m > 0 ? m + 'm' : '');
};

/*
* If a shift goes past midnight onto the next day,
* split the shift into two seperate shifts, one on the day before and one on the day after.
*/
const checkShiftPastMidnight = (shift) => {
    const start = moment.duration(shift.display_start).asSeconds();
    const end = moment.duration(shift.display_end).asSeconds();

    /* If the shift goes past midnight, we want to split it at midnight,
    creating two seperate shifts, one on the next day. */
    if (end < start && end >= 300) {
        let shiftBeforeMidnight = Object.assign({}, shift),
            shiftAfterMidnight = Object.assign({}, shift);
        shiftBeforeMidnight.display_end = '00:00:00'; // Set end of first shift to midnight.
        shiftAfterMidnight.display_start = '00:00:00'; // Set start of second shift to midnight on the next day.
        shiftAfterMidnight.display_date = moment(shift.date, 'YYYY-MM-DD').add(1, 'days').format('YYYY-MM-DD');

        return [
            shiftBeforeMidnight,
            shiftAfterMidnight
        ];
    }
    return [shift];
};

// Create an object of helper functions that can be accessed and used throughout the application.
const helpers = {
    handleChange,
    matchConcatOrder,
    getWeekStartEnd,
    weeksBetween,
    checkCurrentDate,
    calculateLength,
    secondsToHours,
    checkShiftPastMidnight
};

export default helpers;