import axios from 'axios';
import helpers from './helpers';

const apiInstance = axios.create({
    baseURL: '/api',
    headers: {
        'Accept': 'application/json'
    },
    withCredentials: true
});


apiInstance.interceptors.response.use(response => response, err => {
    // We need to catch user's pressing the back button after logging out, so we redirect them.    
    if (err.response.status === 401) {
        window.location.href = '/login';
        return Promise.reject();
    }
    // Return the current error (for normal error handling).
    return Promise.reject(err);
});

const defaultGetRequest = async (url) => {
    const res = await apiInstance({
        mehtod: 'GET',
        url: url
    });
    const data = await res.data;

    if (!data.success)
        return [];

    return data.data;
};

const defaultPostRequest = async (url, state) => {
    try {
        const res = await apiInstance({
            method: 'POST',
            url: url,
            data: state
        });
        const data = await res.data;
        if (!data.success)
            return {
                errors: ['Something went wrong, please try again later.']
            };
        return data;
    } catch (e) {
        const data = e.response.data;
        const res = {
            errors: []
        };
        if (data.errors) {
            Object.values(data.errors).map((error, i) => {
                res.errors.push(error[0]);
            })
        } else {
            res.errors.push('Something went wrong, please try again later.');
        }
        return res;
    }
};

const defaultDeleteRequest = async (url) => {
    const res = await apiInstance({
        method: 'DELETE',
        url: url
    });
    const data = await res.data;

    if (!data.success)
        return {
            errors: ['Something went wrong, please try again later.']
        };
    return data;
}

const api = {
    fetchCurrentEmployee: async () => {
        const res = await apiInstance({
            mehtod: 'GET',
            url: '/employee',
            transformResponse: [(data) => {
                data = JSON.parse(data);
    
                var employee = {};
                if (data.success) {
                    employee = data.data;
                    employee.fname = employee.fname.charAt(0).toUpperCase() + employee.fname.slice(1);
                    employee.lname = employee.lname.charAt(0).toUpperCase() + employee.lname.slice(1);
                }

                return employee;
            }]
        });
        const data = await res.data;
        return data;
    },
    fetchEmployees: async (departments, availability) => {
        const res = await apiInstance({
            method: 'GET',
            url: '/employees',
            transformResponse: [(data) => {
                data = JSON.parse(data);

                var employees = [];

                if (data.success) {
                    data.data.forEach(employee => {
                        var employee_departments = employee.departments !== null ? employee.departments : [];
                        var employee_availability = employee.availability_preference !== null ? employee.availability_preference : [];

                        if (employee_departments.length > 0)
                            employee_departments = departments.filter((e) => employee.departments.split(',').includes(e.id.toString()));
                        if (employee_availability.length > 0)
                            employee_availability = helpers.matchConcatOrder(availability, employee.availability_preference.split(','));

                        employees.push({
                            id: employee.id,
                            fname: employee.fname.charAt(0).toUpperCase() + employee.fname.slice(1),
                            lname: employee.lname.charAt(0).toUpperCase() + employee.lname.slice(1),
                            email: employee.email,
                            departments: employee_departments,
                            availability: employee_availability,
                            shifts: [],
                            role: employee.role,
                            role_id: employee.role_id
                        });
                    });
                }

                return employees;
            }]
        });
        const data = await res.data;
        return data;
    },
    fetchDepartments: () => {
        return defaultGetRequest('/departments');
    },
    fetchAvailability: () => {
        return defaultGetRequest('/availability');
    },
    fetchDays: async () => {
        const res = await apiInstance({
            method: 'GET',
            url: '/days',
            transformResponse: [(data) => {
                data = JSON.parse(data);

                var days = [];
                if (data.success) {
                    data.data.forEach((day) => {
                        days.push({
                            id: day.id,
                            name: day.name.charAt(0).toUpperCase() + day.name.slice(1),
                            day: day.day + 1 < 7 ? day.day + 1 : 0
                        });
                    });
                }

                return days;
            }]
        });
        const data = await res.data;

        return data;
    },
    fetchShifts: (start, end, departmentId) => {
        // Fetch all the shifts within a given range.
        var url = '/shifts?start=' + start + '&end=' + end;

        if (departmentId > 0)
            url += '&department=' + departmentId;

        return defaultGetRequest(url);
    },
    fetchShiftRatings: (shiftIds, specificUser=false) => {
        var url = '/shift/ratings';

        //Create an array of all the shifts to get ratings for in the URL.
        shiftIds.forEach((shiftId, i) => {
            if (i == 0)
                url += '?ids[]=' + shiftId;
            else
                url += '&ids[]=' + shiftId;
        });

        if (specificUser)
            url += '&me=1';

        return defaultGetRequest(url);
    },
    fetchRotas: async (start='', end='') => {
        var url = '/shift/rotas';

        if (start !== '' && end !== '')
            url += '?start=' + start + '&end=' + end;

        return defaultGetRequest(url);
    },
    assignShifts: (data, reassign) => {
        var url = '/assign';
        if (reassign) url += '?reassign=1';
        return defaultPostRequest(url, data);
    },
    createDepartment: (data) => {
        return defaultPostRequest('/department/create', data);
    },
    updateDepartment: async(id, state) => {
        try {
            const res = await apiInstance({
                method: 'PUT',
                url: '/department/update/' + id,
                data: state
            });
            const data = await res.data;

            if (!data.success)
                return {
                    errors: ['Something went wrong, please try again later.']
                };
            return data;
        } catch (e) {
            const data = e.response.data;
            const res = {
                errors: []
            };
            if (data.errors) {
                Object.values(data.errors).map((error, i) => {
                    res.errors.push(error[0]);
                })
            } else {
                res.errors.push('Something went wrong, please try again later.');
            }
            return res;
        }
    },
    createEmployee: (data) => {
        return defaultPostRequest('/employee/create', data);
    },
    updateEmployee: async (id, state) => {
        try {
            const res = await apiInstance({
                method: 'PUT',
                url: '/employee/update/' + id,
                data: state
            });
            const data = await res.data;

            if (!data.success)
                return {
                    errors: ['Something went wrong, please try again later.']
                };
            return data;
        } catch (e) {
            const data = e.response.data;
            const res = {
                errors: []
            };
            if (data.errors) {
                Object.values(data.errors).map((error, i) => {
                    res.errors.push(error[0]);
                })
            } else {
                res.errors.push('Something went wrong, please try again later.');
            }
            return res;
        }
    },
    createShift: (data) => {
        return defaultPostRequest('/shift/create', data);
    },
    updateShift: async (id, state) => {
        try {
            const res = await apiInstance({
                method: 'PUT',
                url: '/shift/update/' + id,
                data: state
            });
            const data = await res.data;

            if (!data.success)
                return {
                    errors: ['Something went wrong, please try again later.']
                };
            return data;
        } catch (e) {
            const data = e.response.data;
            const res = {
                errors: []
            };
            if (data.errors) {
                Object.values(data.errors).map((error, i) => {
                    res.errors.push(error[0]);
                })
            } else {
                res.errors.push('Something went wrong, please try again later.');
            }
            return res;
        }
    },
    createManyRatings: (data) => {
        return defaultPostRequest('/shift/rating/create/many', data);
    },
    createRating: (data) => {
        return defaultPostRequest('/shift/rating/create', data);
    },
    createRota: (data) => {
        return defaultPostRequest('/shift/rota/create', data);
    },
    duplicateRota: (data) => {
        return defaultPostRequest('/shift/rota/duplicate', data);
    },
    deleteRota: (id) => {
        return defaultDeleteRequest('/shift/rota/delete/' + id);
    },
    deleteShifts: (start, end) => {
        return defaultDeleteRequest('/shift/date/delete/' + start + '/' + end);
    },
    deleteShift: (id) => {
        return defaultDeleteRequest('/shift/date/delete/' + id);
    },
    deleteDepartment: (id) => {
        return defaultDeleteRequest('/department/delete/' + id);
    },
    deleteEmployee: (id) => {
        return defaultDeleteRequest('/employee/delete/' + id);
    },
    deleteAssigned: (start, end) => {
        return defaultDeleteRequest('/employee/shifts/delete/' + start + '/' + end);
    }

};

export default api;