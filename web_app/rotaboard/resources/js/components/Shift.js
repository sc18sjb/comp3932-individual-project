import { useCallback, useContext, useEffect, useState } from "react";
import ShiftModal from "./ShiftModal";
import CreateShiftModal from '../components/CreateShiftModal';
import Tooltip from 'react-bootstrap/Tooltip';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import EmployeeContext from "../auth/EmployeeContext";
import helpers from "../utils/helpers";

export default function Shift(props) {
    // Default height 100% the height of the row.
    const [height, setHeight] = useState('100%');
    const [top, setTop] = useState((100 / 60) * parseInt(props.shift.start.substring(3, 5)) + '%');
    const [shiftModalShow, setShiftModalShow] = useState(false);
    // If its a new shift, we automatically show the create shfit modal (to set the content for this shift).
    const [createShiftModalShow, setCreateShiftModalShow] = useState(props.shift.newShift ? true : false);
    const [assigned, setAssigned] = useState([]);
    const [rating, setRating] = useState(-1);
    const user = useContext(EmployeeContext);
    const widgetWidth = 90;

    // If the shift has been assigned to or not
    var assignedClass = '';
    if (props.shift.assigned === 0)
        assignedClass = 'shift--unassigned';
    else if (props.shift.min > props.shift.assigned)
        assignedClass = 'shift--partsigned';
    else if (props.shift.assigned >= props.shift.min)
        assignedClass = 'shift--filled'

    // Adaptive styles depending on how many shifts are next to one another.
    const shiftStyle = {
        width: (widgetWidth / props.n) + '%',
        left: ((widgetWidth / props.n) * props.shiftIndex) + '%',
        height: height,
        top: top,
        backgroundColor: props.shift.department_color
    };

    // When the shift changes, we want to update its height.
    useEffect(() => {
        // Get the height of one of the table rows (second row as this is the first non header row).
        const row = document.getElementsByClassName('shift-table__row')[1];
        if (row !== undefined)
            setHeight(row.clientHeight * helpers.calculateLength(props.shift.display_start, props.shift.display_end));
    }, [props.shift]);

    // For a given set of employees and their shifts, figure out which of those shifts are this current shift (assigned to this).
    const fetchAssignedRequest = useCallback(() => {
        var assigned = [];
        props.employees.some((employee, i) => {
            const foundShifts = employee.shifts.filter((e) => e.date_id === props.shift.date_id)
            if (foundShifts.length > 0) {
                assigned.push(employee);
                return;
            }        
        });

        setAssigned(assigned);
    }, [props.shift]);

    const openShiftModal = (e) => {
        e.stopPropagation();

        // If the rating hasn't already been set, figure out the rating for this shift.
        if (rating === -1) {
            var newRating = props.ratings.find((e) => e.shift_id === props.shift.id && e.provider === 'EMPLOYEE');
            if (newRating) {
                newRating = (Math.round(newRating.rating) > 5) ? 5 : Math.round(newRating.rating); 
                setRating(newRating);
            }
        }

        // We want to show the create shift modal if its an admin and there isn't any employees assigned to the shift.
        if (user.hasRoles(['admin']) && props.shift.assigned <= 0) {
            setCreateShiftModalShow(true);   
        } else {
            setShiftModalShow(true);
            fetchAssignedRequest();
        }
    }

    const closeShiftModal = (e) => {
        if (e !== undefined)
            e.stopPropagation();
        setShiftModalShow(false);
    }

    const hideCreateShiftModal = () => {
        // We need to close the modal before we can remove the shift
        setCreateShiftModalShow(false);

        if (props.shift.newShift) { 
            // We pop and remove the last shift (that is the shift we just added) after the close animation has finished.
            setTimeout(() => {
                const tempShifts = [...props.shifts];
                tempShifts.pop();
                props.setShifts(tempShifts);
            }, 100);    
        }
    }

    const displayShiftLength = (startTime, endTime) => {
        const seconds = 3600 * helpers.calculateLength(startTime, endTime);
        return startTime.substring(0, 5) + ' - ' + endTime.substring(0, 5) + ' (' + helpers.secondsToHours(seconds) + ' Shift)';
    };

    return (
        <>
            <div 
                onClick={openShiftModal} 
                style={shiftStyle} 
                className={'shift ' + assignedClass}
            >
                {!props.shift.newShift && 
                    <>
                        {/* Show the number of employees assigned to the shift */}
                        <OverlayTrigger
                            placement='right'
                            overlay={
                                <Tooltip >
                                    {props.shift.assigned} / {props.shift.min <= 0 ? props.shift.assigned : props.shift.min} Assigned
                                </Tooltip>
                            }
                        >
                            <div className='shift-assigned'></div>
                        </OverlayTrigger>
                        <div className='shift-content'>
                            {
                                <>
                                    <span className='shift-content__department-name'>{props.shift.department_name.charAt(0).toUpperCase() + props.shift.department_name.slice(1)}</span>
                                    <span className='shift-content__length'>
                                        {displayShiftLength(props.shift.start, props.shift.end)}
                                    </span>
                                    
                                </>
                            }
                        </div>
                    </>
                }
            </div>

            {/* Need to at least have 1 department to show a shift modal */}
            {props.departments.length > 0 &&        
                <ShiftModal 
                    shift={props.shift}
                    assigned={assigned}
                    rating={rating}
                    setRating={setRating}
                    employeeDashboard={props.employeeDashboard}
                    show={shiftModalShow}
                    onHide={closeShiftModal}
                />
            }

            {/* We only show the create shift modal if its a new shift or the employee is an admin */}
            {(props.shift.newShift || user.hasRoles(['admin'])) &&
                <CreateShiftModal
                    shift={props.shift}
                    shifts={props.shifts}
                    setShifts={props.setShifts}
                    departments={props.departments}
                    setHide={setCreateShiftModalShow}
                    show={createShiftModalShow}
                    onHide={hideCreateShiftModal}
                />
            }
            
        </>
    );
}