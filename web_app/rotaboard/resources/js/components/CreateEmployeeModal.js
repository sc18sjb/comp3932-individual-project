import { useEffect, useState } from "react";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import { Link } from "react-router-dom";
import helpers from "../utils/helpers";
import api from "../utils/api";

const defaultState = {
    fname: '',
    lname: '',
    email: '',
    password: '',
    availability_ids: [],
    department_ids: [],
    role: 'employee',
    role_id: 2,
    errors: []
};

export default function CreateEmployeeModal({employee, employees, setEmployees, departments, availability, ...props}) {
    const [state, setState] = useState(defaultState);
    const [departmentSelect, setDepartmentSelect] = useState({});
    const [availableSelect, setAvailableSelect] = useState({});

    // When we change employee, we want to update the state to be the new employee, or be empty.
    useEffect(() => {
        if (Object.keys(employee).length !== 0) {
            // Set the state to be the employee's details (autofill).
            setState({
                fname: employee.fname,
                lname: employee.lname,
                email: employee.email,
                password: '',
                department_ids: employee.departments.map((e) => e.id),
                availability_ids: employee.availability.map((e) => e.id),
                role: employee.role,
                role_id: employee.role_id,
                errors: []
            });

            var departments = {};
            var availability = {};

            employee.departments.forEach((department, i) => {
                departments[department.id] = {
                    style: {
                        backgroundColor: '#3dd598',
                        borderColor: '#3dd598'
                    },
                    text: (i + 1) + ') ' + department.name.charAt(0).toUpperCase() + department.name.slice(1)
                }
            }); 

            employee.availability.forEach((available, i) => {
                availability[available.id] = {
                    style: {
                        backgroundColor: '#3dd598',
                        borderColor: '#3dd598'
                    },
                    text: (i + 1) + ') ' + available.name.charAt(0).toUpperCase() + available.name.slice(1)
                }
            });
            
            setDepartmentSelect(departments);
            setAvailableSelect(availability);
        } else {
            setState(defaultState);
            setDepartmentSelect({});
            setAvailableSelect({});
        }
    }, [employee]);

    const handleButtonChange = (setTarget, id, color, text) => {
        setTarget(prevState => ({
            ...prevState,
            [id]: {
                style: {
                    backgroundColor: color,
                    borderColor: color
                },
                text: text
            }
        }));
    }

    const handleButtonToggle = (e, target, setTarget, targetName) => {
        const id = parseInt(e.target.dataset.id);

        // Remove.
        if (target.includes(id)) {
            setState(prevState => ({
                ...prevState,
                [targetName]: target.filter((e) => e !== id)
            }));
            const text = e.target.innerText.substring(3, e.target.innerText.length);
            handleButtonChange(setTarget, id, '#0062ff', text);
        // Add.
        } else {            
            setState(prevState => ({
                ...prevState,
                [targetName]: [...target, id]
            }));
            const text = (target.length + 1) + ') ' + e.target.innerText;
            handleButtonChange(setTarget, id, '#3dd598', text);
        }
    }

    const setEmployeeFinalState = (finalState, id) => {
        var employee_departments = finalState.department_ids;
        var employee_availability = finalState.availability_ids;

        // Get the department and availability obejects.
        if (employee_departments.length > 0)
            employee_departments = departments.filter((e) => finalState.department_ids.includes(e.id));
        if (employee_availability.length > 0)
            employee_availability = helpers.matchConcatOrder(availability, finalState.availability_ids);

        // Since we have the objects, we can now delete the ids.
        delete finalState.department_ids;
        delete finalState.availability_ids;

        finalState.departments = employee_departments;
        finalState.availability = employee_availability;
        finalState.id = id; // Set the id of the employee to the insert id.
        return finalState;
    };

    const handleSaveEmployee = async (e) => {
        e.preventDefault();
        
        const res = await api.createEmployee(state);
        if (res.success) {
            // Close the modal.
            props.onHide();
            const finalState = setEmployeeFinalState(state, res.data);

            // Insert the new employee.
            setEmployees([...employees, finalState]);

            // Reset the states
            setState(defaultState);
            setDepartmentSelect({});
            setAvailableSelect({});
        } else {
            // We set the errors
            setState(prevState => ({
                ...prevState,
                ['errors']: res.errors
            }));
        }
    };

    const handleUpdateEmployee = async (e) => {
        e.preventDefault();

        const res = await api.updateEmployee(employee.id, state);
        if (res.success) {
            props.onHide();
            const finalState = setEmployeeFinalState(state, employee.id);

            // Update the existing employee.
            const exists = employees.findIndex((e) => e.id === employee.id);
            setEmployees((employees) => {
                return [
                    ...employees.slice(0, exists),
                    {
                        ...finalState
                    },
                    ...employees.slice(exists + 1)
                ]
            });
        } else {
            // We set the errors
            setState(prevState => ({
                ...prevState,
                ['errors']: res.errors
            }));
        }
    }

    const handleRoleChange = (e) => {
        helpers.handleChange(e, setState);

        const i = e.nativeEvent.target.selectedIndex;
        setState(prevState => ({
            ...prevState,
            ['role']: e.nativeEvent.target[i].text.toLowerCase()
        }));
    };

    return (
        <Modal
            {...props}
            size="md"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <form className='modal-content__form'>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        {(Object.keys(employee).length === 0) ? (
                            <span>New Employee</span>
                        ) : (
                            <span>Update Employee</span>
                        )}                        
                    </Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    {(state.errors.length > 0) && (
                        <div className='alert alert--full alert-warning mb-4'>
                            <ul>
                                {state.errors.map((error, i) => {
                                    return (
                                        <li key={i}>{error}</li>
                                    );  
                                })}
                            </ul>
                        </div>
                    )}

                    <Row>
                        <Col className='d-flex flex-column modal-content__group'>
                            <h4>First Name</h4>
                            <input className='modal-content__input' type='text' name='fname' onChange={(e) => helpers.handleChange(e, setState)} value={state.fname} />
                        </Col>
                        <Col className='d-flex flex-column modal-content__group'>
                            <h4>Last Name</h4>
                            <input className='modal-content__input' type='text' name='lname' onChange={(e) => helpers.handleChange(e, setState)} value={state.lname} />                    
                        </Col>
                    </Row>

                    <Row>
                        <Col className='d-flex flex-column modal-content__group'>
                            <h4>Email</h4>
                            <input className='modal-content__input' type='email' name='email' onChange={(e) => helpers.handleChange(e, setState)} value={state.email} />
                        </Col>
                        <Col className='d-flex flex-column modal-content__group'>
                           <h4>Password</h4>
                           <input className='modal-content__input' type='password' name='password' onChange={(e) => helpers.handleChange(e, setState)} value={state.password} />
                        </Col>
                    </Row>

                    <div className='modal-content__group'>
                        <h4>Role</h4>

                        <Form.Select name='role_id' onChange={handleRoleChange} value={state.role_id}>
                            <option value='1'>admin</option>
                            <option value='2'>employee</option>
                            {/* past employee?? */}
                        </Form.Select>
                    </div>

                    <div className='modal-content__group'>
                        <h4>Select Departments</h4>
                        {departments.length > 0 ? (
                            <div className='d-flex flex-wrap'>
                                {departments.map((department, i) => {
                                    const style = department.id in departmentSelect ? departmentSelect[department.id].style : {};
                                    const text = department.id in departmentSelect ? departmentSelect[department.id].text : department.name.charAt(0).toUpperCase() + department.name.slice(1);

                                    return (
                                        <Button 
                                            className='mx-2 my-1' 
                                            style={style} 
                                            key={i} 
                                            data-id={department.id} 
                                            onClick={(e) => handleButtonToggle(e, state.department_ids, setDepartmentSelect, 'department_ids')}
                                        >
                                            {text}
                                        </Button>
                                    );
                                })}
                            </div>
                        ) : (
                            <div className='modal-content__group'>
                                <Link 
                                    to='/app/departments'
                                    state={{modal: true}}
                                >
                                    <Button className='btn--full btn-success'>Add Departments</Button>
                                </Link>
                            </div>
                        )}
                    </div>
                        
                    {/* need to replace this with start and end range for when an employee can work */}
                    {/* -> create a form that somehow allows user to specify range with breaks i.e. 10 -> 12 then 2-> 3 etc. */}
                    <div className='modal-content__group'>
                        <h4>Select Availability</h4>
                        {availability.length > 0 ? (
                            <div className="d-flex flex-wrap">
                                {availability.map((available, i) => {
                                    const style = available.id in availableSelect ? availableSelect[available.id].style : {};
                                    const text = available.id in availableSelect ? availableSelect[available.id].text : available.name.charAt(0).toUpperCase() + available.name.slice(1);

                                    return (
                                        <Button 
                                            className='mx-2 my-1'
                                            style={style}
                                            key={i}
                                            data-id={available.id}
                                            onClick={(e) => handleButtonToggle(e, state.availability_ids, setAvailableSelect, 'availability_ids')}
                                        >
                                            {text}
                                        </Button>
                                    );
                                })}
                            </div>
                        ) : (
                            <div className='modal-content__group'>
                                <Link><Button className='btn--full btn-success'>Add Availability</Button></Link>
                            </div>
                        )}
                    </div>


                </Modal.Body>

                <Modal.Footer>
                    {(Object.keys(employee).length === 0) ? (
                        <Button onClick={handleSaveEmployee}>Save</Button>
                    ) : (
                        <Button onClick={handleUpdateEmployee}>Update</Button>
                    )}
                </Modal.Footer>
            </form>
        </Modal>
    );
}