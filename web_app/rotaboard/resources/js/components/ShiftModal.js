import { useContext, useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row'
import EmployeeContext from '../auth/EmployeeContext';
import RatingFaces from './RatingFaces';
import api from '../utils/api';

export default function ShiftModal({shift, assigned, rating, setRating, employeeDashboard, ...props}) {
    const user = useContext(EmployeeContext);

    const handleRateShifts = async (e, rating) => {
        e.preventDefault();

        const res = await api.createRating({
            employee_id: user.id,
            shift_id: shift.id,
            rating: rating
        });

        if (res.success) {
            setRating(rating);
        }
    };

    // Apply styles for the current user who's not an admin.
    const bodyStyle = () => {
        if (user.hasRoles(['employee']) && assigned.find((e) => e.id === user.id))
            return 'pb-0';
        return '';
    };

    return (
        <Modal
            {...props}
            size="md"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            className='shift-modal'
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    {shift.department_name.charAt(0).toUpperCase() + shift.department_name.slice(1)}
                </Modal.Title>
            </Modal.Header>

            <Modal.Body className={'show-grid ' + bodyStyle()}>
                <div className='modal-content__group'>
                    <Row>
                        <Col>
                            <h4>Start</h4>
                            <span className='modal-content__value'>{shift.start}</span>
                        </Col>
                        <Col>
                            <h4>End</h4>
                            <span className='modal-content__value'>{shift.end}</span>
                        </Col>
                    </Row>
                </div>

                <div className='modal-content__group'>
                    <Row>
                        <Col>
                            <h4>Min Workers</h4>
                            <span className='modal-content__value'>{shift.min} Needed</span>
                        </Col>
                        <Col>
                            <h4>Max Workers</h4>
                            <span className='modal-content__value'>{shift.max} Allowed</span>
                        </Col>
                    </Row>
                </div>
            
                <div className='modal-content__group'>
                    <h4>Assigned To</h4>
                    {assigned.length > 0 ? (
                        <div className='shift-modal__assigned'>
                            {assigned.map((assignee, i) => {
                                return (
                                    <div className='shift-modal__assignee' key={i} md={8}>
                                        {assignee.fname} {assignee.lname}
                                    </div>
                                );
                            })}
                        </div>
                    ) : (
                        <span className='modal-content__value'>None yet!</span>
                    )}
                </div>
                
                {/* show rating faces for the employees shifts only */}
                {(employeeDashboard && assigned.find((e) => e.id === user.id)) && (     
                    <div className='modal-content__group'>
                        <h4>How would you rate this shift?</h4>
                        <div className='mt-3'>
                            <RatingFaces
                                handleRateShifts={handleRateShifts}
                                currentRating={rating}
                            />
                        </div>
                    </div>
                )}

            </Modal.Body>

            <Modal.Footer>
                <Button onClick={(e) => props.onHide(e)}>Close</Button>
            </Modal.Footer>
        </Modal>
    );
}