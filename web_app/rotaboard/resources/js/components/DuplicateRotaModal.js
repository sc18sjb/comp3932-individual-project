import { useState, useEffect } from "react";
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import helpers from "../utils/helpers";
import api from "../utils/api";

export default function DuplicateRotaModal({rotas, weekDays, getDateChangeData, ...props}) {
    const [state, setState] = useState({
        rota_id: (rotas.length <= 0) ? '' : rotas[0].id,
        errors: []
    });

    useEffect(() => {
        if (rotas.length > 0)
            setState({
                rota_id: rotas[0].id,
                errors: []
            });
    // When the rotas array changes, we want to update the first id in the list
    }, [rotas]);

    const handleSubmit = async (e) => {
        e.preventDefault();

        const rota = rotas.filter((e) => e.id === parseInt(state.rota_id))
        // No rota exists that matches the ID provided, we return;
        if (rota.length <= 0) return

        var [weekStart, _] = helpers.getWeekStartEnd(weekDays);
        weekStart = new Date(Date.parse(weekStart));

        const rotaStart = new Date(Date.parse(rota[0].start));
        const difference = helpers.weeksBetween(rotaStart, weekStart);

        const res = await api.duplicateRota({
            rota_id: rota[0].id,
            forward: difference
        });

        if (res.success) {
            getDateChangeData();
            setTimeout(() => {
                props.onHide();
            }, 100);
        } else {
            // We set the errors
            setState(prevState => ({
                ...prevState,
                ['errors']: res.errors
            }));
        }
    }

    const handleRotaChange = (e) => {
        helpers.handleChange(e, setState);
    }

    return (
        <Modal
            {...props}
            size="md"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <form className='modal-content__form'>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Duplicate Rota
                    </Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    {(state.errors.length > 0) && (
                        <div className='alert alert--full alert-warning mb-4'>
                            <ul>
                                {state.errors.map((error, i) => {
                                    return (
                                        <li key={i}>{error}</li>
                                    );  
                                })}
                            </ul>
                        </div>
                    )}

                    <div className='modal-content__group'>
                        <h4>Rota</h4>

                        {(rotas.length <= 0) ? (
                            <span className='modal-content__value'>No Rotas to duplicate</span>
                        ) : (
                            <Form.Select name='rota_id' onChange={handleRotaChange} value={state.rota_id}>
                                {rotas.map((rota, i) => {
                                    return (
                                        <option key={i} value={rota.id}>{rota.name}</option>
                                    );
                                })}
                            </Form.Select>
                        )}
                    </div>
                </Modal.Body>

                <Modal.Footer>
                    {(rotas.length <= 0) ? (
                        <Button onClick={props.onHide}>Close</Button>
                    ) : (
                        <Button onClick={handleSubmit} >Duplicate</Button>
                    )}
                </Modal.Footer>
            </form>
        </Modal>
    );
}