import Shift from '../components/Shift';
import EmployeeContext from '../auth/EmployeeContext';
import { useContext, useEffect, useState } from 'react';
import CreateRatingModal from './CreateRatingModal';
import api from '../utils/api.js';
import helpers from '../utils/helpers';

export default function EmployeeDashboard(props) {
    const user = useContext(EmployeeContext);
    const [createRatingModalShow, setCreateRatingModalShow] = useState(false)
    const [emptyRatings, setEmptyRatings] = useState([]);
    const [ratings, setRatings] = useState([]);

    useEffect(() => {        
        const getShiftIds = async () => {
            var shiftIds = [];
            props.shifts.forEach((shift, i) => {
                // Only want to get ratings for shifts with assignments.
                if (shift.employee_ids === null) return;

                // Only want shifts assigned to the specific user.
                const assigned = shift.employee_ids.split(',');
                if (!assigned.find((e) => parseInt(e) === user.id)) return;

                if (!shiftIds.includes(shift.id))
                    shiftIds.push(shift.id);
            });
            
            if (shiftIds.length <= 0) return;

            const ratings = await api.fetchShiftRatings(shiftIds, true);
            setRatings(ratings);

            const emptyRating = ratings.filter((e) => e.rating === 0);
            if (emptyRating.length > 0) {
                setEmptyRatings(emptyRating);
                setCreateRatingModalShow(true);
            }
        }

        getShiftIds();
    }, [user, props.shifts])

    return (
        <>
            <table className='table data-table shift-table'>
                <thead>
                    <tr className='shift-table__row shift-table__row--header'>
                        <th>Employee</th>
                        {props.weekDays.map((day, dayIndex) => {
                            const today = helpers.checkCurrentDate(day.date);
                            return (
                                <th 
                                    className={'shift-table__cell shift-table__cell--header text-center ' + (today ? 'shift-table__cell--selected' : '')}
                                    key={dayIndex}
                                    scope='col'
                                >
                                    <span className='header-day'>{day.name}</span>
                                    <span className={'header-date ' + (today ? 'header-date--selected' : '')}>
                                        {day.date.getDate()}
                                    </span>
                                </th>
                            );
                        })}
                    </tr>
                </thead>

                <tbody>
                    {props.employees.map((employee, employeeIndex) => {
                        const username = (user.id !== employee.id) ? employee.fname + ' ' + employee.lname : 'You';

                        return (
                            <tr key={employeeIndex}>
                                <th className='shift-table__cell'>{username}</th>
                                {props.weekDays.map((day, dayIndex) => {
                                    const today = helpers.checkCurrentDate(day.date);
                                    const dayString = day.date.toISOString().split('T')[0];
                                    var foundShifts = [];
                                    
                                    employee.shifts.some((shift) => {
                                        if (shift.date === dayString) {
                                            foundShifts.push(shift);
                                            return;
                                        }
                                    });

                                    if (foundShifts.length > 0) {
                                        return (
                                            <th 
                                                className={'shift-table__cell shift-table__cell--shift ' + (today ? 'shift-table__cell--selected' : '')}
                                                key={dayIndex}
                                            >
                                                {foundShifts.map((shift, shiftIndex) => {
                                                    return (
                                                        <Shift
                                                            key={shiftIndex}
                                                            n={foundShifts.length}
                                                            shiftIndex={shiftIndex}
                                                            shifts={props.shifts}
                                                            shift={shift}
                                                            setShifts={props.setShifts}
                                                            departments={props.departments}
                                                            employees={props.employees}
                                                            ratings={ratings}
                                                            employeeDashboard={true}
                                                        />
                                                    )
                                                })}
                                            </th>
                                        )
                                    } else {
                                        return (
                                            <th 
                                                className={'shift-table__cell ' + (today ? 'shift-table__cell--selected' : '')}
                                                key={dayIndex}
                                            ></th>
                                        );
                                    }
                                })}
                            </tr>
                        );
                    })}
                </tbody>
            </table>

            <CreateRatingModal
                emptyRatings={emptyRatings}
                setRatings={setRatings}
                show={createRatingModalShow}
                onHide={() => setCreateRatingModalShow(false)}
            />
        </>
    );
}