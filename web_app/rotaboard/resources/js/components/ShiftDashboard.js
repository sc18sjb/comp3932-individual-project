import Shift from '../components/Shift';
import { useEffect, useState } from 'react'
import helpers from "../utils/helpers";

export default function ShiftDashboard(props) {    
    const [displayShifts, setDisplayShifts] = useState([]);

    useEffect(() => {
        const shifts = [];
        props.shifts.forEach((shift) => {
            shifts.push(...helpers.checkShiftPastMidnight(shift));
        });
        setDisplayShifts(shifts);
    }, [props.shifts]);

    const createShift = (e, day, hour) => {
        //  We cannot stop bubbling from the onHide of the shift modal since it does not give as an event
        //  therefore, we need to check if the target is correct
        if (e.target.nodeName !== 'TH') return;
        e.stopPropagation();

        var end = hour + 1 < 24 ? hour + 1 : 0;
        const start = hour.toString().length < 2 ? '0' + hour.toString() : hour.toString();   
        end = end.toString().length < 2 ? '0' + end.toString() : end.toString();   

        // Default new shift values.
        var newShift = {
            start: start + ':00:00',
            display_start: start + ':00:00',
            end: end + ':00:00',
            display_end: end + ':00:00',
            date: day.date.toISOString().split('T')[0],
            display_date: day.date.toISOString().split('T')[0],
            day_id: day.id,
            assigned: 0,
            min: 1,
            max: 1,
            newShift: true // We want to open the create shift modal by default.
        };

        // If there are departments, and a specific department is selected on the dashboard,
        // use this department as the shift department, otherwise use the 1st department.
        if (props.departments.length > 0) {
            newShift.department_id = (props.department.id > 0) ? props.department.id : props.departments[0].id;
            newShift.department_name = (props.department.id > 0) ? props.department.name : props.departments[0].name;
        }

        // Add the new shift to the end of the shifts list.
        setDisplayShifts([...displayShifts, newShift]);    
    }

    return (
        <table className='table data-table shift-table'>
            <thead>
                <tr className='shift-table__row shift-table__row--header'>
                    <th>Hour</th>
                    {props.weekDays.map((day, dayIndex) => {
                        const today = helpers.checkCurrentDate(day.date);
                        return (
                            <th 
                                className={'shift-table__cell shift-table__cell--header text-center ' + (today ? 'shift-table__cell--selected' : '')}
                                key={dayIndex}
                                scope='col'
                            >
                                <span className='header-day'>{day.name}</span>
                                <span className={'header-date ' + (today ? 'header-date--selected' : '')}>
                                    {day.date.getDate()}
                                </span>
                            </th>
                        );
                    })}

                </tr>
            </thead>

            <tbody>
                {props.hours.map((hour, hourIndex) => {
                    return (
                        <tr className='shift-table__row' key={hourIndex}>
                            <th className='shift-table__cell'>{hour}</th>
                            {props.weekDays.map((day, dayIndex) => {
                                const today = helpers.checkCurrentDate(day.date);
                                const dayString = day.date.toISOString().split('T')[0];
                                var foundShifts = [];

                                // Get all the shifts on that day, at that time
                                displayShifts.some((shift) => {
                                    const start = parseInt(shift.display_start.substring(0, 2));
                                    if (shift.display_date === dayString && start == hourIndex) {                                           
                                        foundShifts.push(shift);
                                        return;
                                    }
                                });

                                if (foundShifts.length > 0) {
                                    return (
                                        <th 
                                            onClick={(e) => createShift(e, day, hourIndex)}
                                            className={'shift-table__cell shift-table__cell--shift ' + (today ? 'shift-table__cell--selected' : '')}
                                            key={dayIndex}
                                        >
                                            {foundShifts.map((shift, shiftIndex) => {
                                                return (
                                                    <Shift 
                                                        key={shiftIndex}
                                                        n={foundShifts.length}
                                                        shiftIndex={shiftIndex}
                                                        shifts={displayShifts}
                                                        shift={shift}
                                                        setShifts={setDisplayShifts}
                                                        departments={props.departments}
                                                        employees={props.employees}
                                                        ratings={[]}
                                                        employeeDashboard={false}
                                                    />
                                                )
                                            })}
                                        </th>
                                    );
                                } else {
                                    return (
                                        // when we click on an empty cell, we want to create a new shift.
                                        <th 
                                            draggable='true'
                                            onDrag={(e) => {console.log(e.clientY)}}
                                            onClick={(e) => createShift(e, day, hourIndex)}
                                            className={'shift-table__cell ' + (today ? 'shift-table__cell--selected' : '')}
                                            key={dayIndex}
                                        ></th>
                                    );
                                }
                            })}
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}