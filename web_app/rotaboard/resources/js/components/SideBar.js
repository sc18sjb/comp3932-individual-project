
import { Link, useLocation } from 'react-router-dom';
import RotaboardLogo from '../../../public/images/rotaboard.png';
import SvgEmployee from '../svg/Employee';
import SvgDepartment from '../svg/Department';
import SvgSettings from '../svg/Settings';
import LogoutSvg from '../svg/Logout';
import axios from 'axios';
import { useCallback, useContext } from 'react';
import EmployeeContext from '../auth/EmployeeContext';

export default function SideBar({position=''}) {
    const user = useContext(EmployeeContext);
    const location = useLocation().pathname;

    const logout = useCallback((e) => {
        e.preventDefault();

        // We need to get a csrf cookie since this is not a protected route.
        axios.get('/sanctum/csrf-cookie').then(response => {
            axios.post('/logout')
                .then(res => {
                    window.location.href = '/login';
                })
                .catch(err => {
                    console.log(err);
                });
        });
    }, []);

    return (
        <div className={'side-bar ' + position}>
            <ul>
                <Link to="/app/">
                    <li className='side-bar__element side-bar__element--company-logo'>
                        <img 
                            className='side-bar__company-logo'
                            src={RotaboardLogo}
                        />
                    </li>
                </Link>

                {(user.hasRoles(['admin'])) && (
                    <>
                        <Link to="/app/employees">
                            <li className={'side-bar__element side-bar__element--link ' + (location == '/app/employees' ? 'side-bar__element--selected' : '')}>
                                <SvgEmployee className="side-bar__svg" />
                                <span>Employees</span>
                            </li>
                        </Link>

                        <Link to="/app/departments">
                            <li className={'side-bar__element side-bar__element--link ' + (location == '/app/departments' ? 'side-bar__element--selected' : '')}>
                                <SvgDepartment className="side-bar__svg" />
                                <span>Departments</span>
                            </li>
                        </Link>
                    </>
                )}
                
                <Link to="/app/departments" className='side-bar__link side-bar__link--bottom' onClick={logout}>
                    <li className={'side-bar__element side-bar__element--link' + (location == '/app/settings' ? 'side-bar__element--selected' : '')}>
                        {/* <SvgSettings className="side-bar__svg" /> */}
                        <LogoutSvg className='side-bar__svg' />
                        <span>Logout</span>
                    </li>
                </Link>
            </ul>
        </div>
    );
}