import { useEffect, useState } from "react";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import RangeSlider from 'react-bootstrap-range-slider';
import helpers from "../utils/helpers";
import { Link } from "react-router-dom";
import moment from 'moment';
import api from "../utils/api";

// Set the variables of the final state that is displayed on the dashboard.
function setFinalState(state, date_id, department_color, assigned = 0, employee_ids = '') {
    const finalState = state;
    finalState.date_id = date_id;
    finalState.assigned = assigned;
    finalState.department_color = department_color;
    finalState.employee_ids = employee_ids;

    // Copy for display purposes (for splut shifts past midnight).
    finalState.display_start = finalState.start;
    finalState.display_end = finalState.end;
    finalState.display_date = finalState.date;
    return finalState;
}

const defaultState = {
    start: '',
    end: '',
    min: 0,
    max: 0,
    department_id: 0,
    department_name: '',
    day_id: 0,
    date: '',
    length: 0,
    errors: []
};

export default function CreateShiftModal({shift, shifts, setShifts, departments, setHide, ...props}) {
    const [state, setState] = useState(defaultState);

    useEffect(() => {
        setState({
            start: shift.start.substring(0, 5),
            end: shift.end.substring(0, 5),
            min: shift.min,
            max: shift.max,
            department_id: shift.department_id,
            department_name: shift.department_name,
            day_id: shift.day_id,
            date: shift.date,
            length: 3600 * helpers.calculateLength(shift.start, shift.end),
            errors: []
        });
    }, [shift]);

    const handleDepartmentChange = (e) => {
        helpers.handleChange(e, setState);

        // We set the department name to the name of the selection (selected department name).
        const i = e.nativeEvent.target.selectedIndex;
        setState(prevState => ({
            ...prevState,
            ['department_name']: e.nativeEvent.target[i].text.toLowerCase()
        }));
    }

    const handleHourChange = (e) => {
        helpers.handleChange(e, setState);
        // Add the seconds to the start time and format.
        const newTime = moment(state.start, 'HH:mm').add(Number(e.target.value), 'seconds').format('HH:mm');
        setState(prevState => ({
            ...prevState,
            ['end']: newTime
        }));
    }

    const handleSaveShift = async (e) => {
        e.preventDefault();

        const res = await api.createShift(state);
        if (res.success) {
            const department = departments.find((d) => d.id === parseInt(state.department_id));
            const finalState = setFinalState(state, res.data, department.color);
            
            // Hide the modal.
            setHide(false);
            
            // Wait for the modal to hide before proceeding.
            setTimeout(() => {
                const tempShifts = [...shifts]
                tempShifts.pop(); // First we remove the temp placeholder shift.
                tempShifts.push(...helpers.checkShiftPastMidnight(finalState)); // Then we add the actual shift
                setShifts(tempShifts);
            }, 100);
        } else {
            // We set the errors
            setState(prevState => ({
                ...prevState,
                ['errors']: res.errors
            }));
        }
    };

    const handleUpdateShift = async (e) => {
        e.preventDefault();
        // TODO: currently we create a new shift when we update a shift, allowing the previous shift's ratings to be kept,
        // implement an option to allow users to update a shift (which will then update ALL instances of that shift) and
        // clear the existing ratings of the shift (essentially as if it is a fresh, new shift).
        const res = await api.updateShift(shift.date_id, state);

        if (res.success) {
            const department = departments.find((d) => d.id === parseInt(state.department_id));
            const finalState = setFinalState(state, shift.date_id, department.color, shift.assigned, shift.employee_ids);
            const prevShifts = shifts.filter((e) => e.date_id === shift.date_id);

            // Make sure the errors are empty.
            setState(prevState => ({
                ...prevState,
                ['errors']: []
            }));

            // Hide the modal.
            setHide(false); 

            // Wait for the modal to hide before proceeding.
            setTimeout(() => {
                setShifts((shifts) => {
                    const exists = shifts.indexOf(prevShifts[0]);
                    return [
                        ...shifts.slice(0, exists),
                        ...helpers.checkShiftPastMidnight(finalState),
                        ...shifts.slice(exists + (prevShifts.length > 1 ? 2 : 1))
                    ];
                });
            }, 100);
        } else {
            // We set the errors
            setState(prevState => ({
                ...prevState,
                ['errors']: res.errors
            }));
        }
    }

    const handleDeleteShift = async (e) => {
        e.preventDefault();
        const res = await api.deleteShift(shift.date_id);

        if (res.success) {
            props.onHide();
            
            setTimeout(() => {
                // Get all shifts except the one we just deleted.
                const remaininShifts = shifts.filter((e) => e.date_id !== shift.date_id);
                setShifts(remaininShifts);
            }, 100);
        } else {
            console.log(res);
        }
    }

    const formatLabel = (seconds) => {
        seconds = Number(seconds);
        const newTime = moment(state.start, 'HH:mm').add(Number(seconds), 'seconds').format('HH:mm');            
        return newTime + ' (' + helpers.secondsToHours(seconds) + ')';
    }
 
    return (
        <Modal
            {...props}
            size="md"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            {departments.length > 0 ? (
                <form className='modal-content__form'>
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-vcenter">
                            {(shift.newShift) ? (
                                <span>Add Shift</span>
                            ) : (
                                <span>Update Shift</span>
                            )}
                        </Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        {(state.errors.length > 0) && (
                            <div className='alert alert--full alert-warning mb-4'>
                                <ul>
                                    {state.errors.map((error, i) => {
                                        return (
                                            <li key={i}>{error}</li>
                                        );  
                                    })}
                                </ul>
                            </div>
                        )}

                        <div className='modal-content__group'>
                            <h4>Length</h4>

                            <RangeSlider 
                                name='length'
                                onChange={handleHourChange}
                                value={state.length}
                                tooltipLabel={currentLength => formatLabel(currentLength)}
                                max={28800}
                            />
                        </div>

                        <Row>
                            <Col className='d-flex flex-column modal-content__group'>
                                <h4>Min Workers</h4>
                                <input className='modal-content__input' type='number' min='0' name='min' onChange={(e) => helpers.handleChange(e, setState)} value={state.min} />
                            </Col>

                            <Col className='d-flex flex-column modal-content__group'>
                                <h4>Max Workers</h4>
                                <input className='modal-content__input' type='number' min='0' name='max' onChange={(e) => helpers.handleChange(e, setState)} value={state.max} />
                            </Col>
                        </Row>

                        <div className='modal-content__group'>
                            <h4>Department</h4>

                            <Form.Select name='department_id' onChange={handleDepartmentChange} value={state.department_id}>
                                {departments.map((department, i) => {
                                    return (
                                        <option key={i} value={department.id}>{department.name}</option>
                                    );
                                })}
                            </Form.Select>
                        </div>
                    </Modal.Body>

                    <Modal.Footer>
                        {shift.newShift ? (
                            <Button onClick={handleSaveShift} >Save</Button>
                        ) : (
                            <>
                                <Button onClick={handleDeleteShift} variant='danger' className='mx-2'>Delete</Button>
                                <Button onClick={handleUpdateShift} >Update</Button>
                            </>
                        )}
                    </Modal.Footer>
                </form>
            ) : (
                <>
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-vcenter">
                            Add Shift
                        </Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <h4>You currently have no departments to assign shifts to.</h4>

                        <div className='modal-content__group'>
                            <Link 
                                to='/app/departments'
                                state={{modal: true}}
                            >
                                <Button className='btn--full btn-success'>Add Departments</Button>
                            </Link>
                        </div>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button onClick={props.onHide} >Close</Button>
                    </Modal.Footer>
                </>
            )}

        </Modal>
    );
}