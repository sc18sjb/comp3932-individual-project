import Plus from "../svg/Plus";
import Button from 'react-bootstrap/Button';

export default function SubDashboardTopBar({addNew}) {
    return (
        <div className='top-bar top-bar--sub'>
            <div className='top-bar__section'>
                <Button onClick={addNew}>
                    <Plus style={{transform: "scale(0.7)"}} />
                    <span>Add new</span>
                </Button>
            </div>
        </div>
    );
}