import Button from 'react-bootstrap/Button';
import Dropdown from 'react-bootstrap/Dropdown';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Popover from 'react-bootstrap/Popover';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import SideBar from "./SideBar";
import Bookmark from "../svg/Bookmark";
import Assign from "../svg/Assign";
import Department from "../svg/Department";
import Calender from "../svg/Calender";
import Tool from "../svg/Tools";
import DuplicateRotaModal from "./DuplicateRotaModal";
import EmployeeContext from "../auth/EmployeeContext";
import { useContext, useState, useCallback, useEffect, useMemo } from "react";
import helpers from "../utils/helpers";
import api from "../utils/api";

const defaultState = {
    name: '',
    errors: []
};

export default function DashboardTopBar(props) {
    const user = useContext(EmployeeContext);
    const [rota, setRota] = useState({}); // The specific rota for this page.
    const [rotas, setRotas] = useState([]); // All the rotas.
    const [popoverState, setPopoverState] = useState(defaultState);
    const [duplicateRotaModalShow, setDuplicateRotaModalShow] = useState(false);

    useEffect(() => {
        const getRota = () => {
            if (rotas.length <= 0 || props.weekDays.length <= 0) return;
            const [weekStart, weekEnd] = helpers.getWeekStartEnd(props.weekDays);
            const rota = rotas.filter((e) => e.start === weekStart && e.end === weekEnd);

            // If rota does not exist we set to default state.
            if (rota.length <= 0) {
                setRota({});
                setPopoverState(defaultState);
            // If rota exists we set it as the rota state. 
            } else {
                setRota(rota[0]);
                setPopoverState({
                    name: rota[0].name,
                    errors: []
                })
            }   
        }

        getRota();
    }, [rotas, props.weekDays]);

    useEffect(() => {
        const getRotas = async () => {
            const rotas = await api.fetchRotas();
            setRotas(rotas);         
        }

        if (user.hasRoles(['admin']))
            getRotas();
    }, [user]);

    // Go to the last week.
    const datePrev = () => {
        if (props.weekDays.length <= 0)
            return false;

        const newDate = new Date(props.weekDays[0].date);
        newDate.setDate(newDate.getDate() - 7);
        props.setCurrentDate(new Date(newDate));
    }

    // Go to the next week.
    const dateNext = () => {
        if (props.weekDays.length <= 0)
            return;
            
        const newDate = new Date(props.weekDays[0].date);
        newDate.setDate(newDate.getDate() + 7)
        props.setCurrentDate(new Date(newDate));
    }

    const handleSaveRota = useCallback(async (e) => {
        if (props.weekDays.length <= 0) return;
        e.preventDefault();

        const [weekStart, weekEnd] = helpers.getWeekStartEnd(props.weekDays);
        const rota = {
            'start': weekStart,
            'end': weekEnd,
            'name': popoverState.name
        };

        const res = await api.createRota(rota);
        
        if (res.success) {
            // We click to close the popover.
            document.body.click();

            // Set the curret rota
            setRota(rota);

            const exists = rotas.findIndex((e) => e.start === rota.start && e.end === rota.end);

            // Update if the index already exists.
            if (exists !== -1) {
                setRotas((rotas) => {
                    return [
                        ...rotas.slice(0, exists),
                        {
                            ...rotas[exists],
                            name: rota.name
                        },
                        ...rotas.slice(exists + 1)
                    ]
                });
            // Otherwise we just add the new rota.
            } else {
                // If we're adding it as a new rota, we need to add the id of the rota.
                rota.id = res.data;
                setRotas([...rotas, rota]);
            }
        } else {
            // We set the errors
            setPopoverState(prevState => ({
                ...prevState,
                ['errors']: res.errors
            }));
        }
    }, [props.weekDays, popoverState]);

    const handleDuplicateRota = useCallback(async (e) => {
        e.preventDefault();
        setDuplicateRotaModalShow(true);
    }, []);


    const handleDeleteRota = useCallback(async (e) => {
        if (Object.keys(rota).length === 0) return;
        e.preventDefault();
        const res = await api.deleteRota(rota.id);

        if (res.success) {
            // Close the popover.
            document.body.click();

            setRota({});
            setPopoverState(defaultState);

            const remainingRotas = rotas.filter((e) => e.id !== rota.id)
            setRotas(remainingRotas);
        } else {
            // We set the errors
            setPopoverState(prevState => ({
                ...prevState,
                ['errors']: res.errors
            }));
        }
    }, [rota]);

    const handleDeleteShifts = useCallback(async (e) => {
        if (props.weekDays.length <= 0) return;
        e.preventDefault();

        const [weekStart, weekEnd] = helpers.getWeekStartEnd(props.weekDays);
        const res = await api.deleteShifts(weekStart, weekEnd);

        if (res.success) {
            props.getDateChangeData();
        } else {
            console.log(res)
        }
    }, [props.weekDays]);

    const savePopover = useMemo(() => {
        return (
            <Popover id='popover-positioned-bottom'>
                <form className='popover__form'>
                    <Popover.Body>
                        <div className='popover__group'>
                            {(popoverState.errors.length > 0) && (
                                <div className='alert alert--full alert-warning mb-4'>
                                    <ul>
                                        {popoverState.errors.map((error, i) => {
                                            return (
                                                <li key={i}>{error}</li>
                                            );  
                                        })}
                                    </ul>
                                </div>
                            )}

                            <h4>Rota name</h4>
                            <input 
                                className='popover__input'
                                text='text'
                                name='name'
                                onChange={(e) => helpers.handleChange(e, setPopoverState)}
                                value={popoverState.name} 
                            />
                        </div>

                        <div className='d-flex justify-content-end'>
                            {(Object.keys(rota).length > 0) ? (
                                <>
                                    <Button onClick={handleDeleteRota} variant='danger' className='mx-2'>Delete</Button>
                                    <Button onClick={handleSaveRota}>Update</Button>              
                                </>
                            ) : (
                                <Button onClick={handleSaveRota}>Save</Button>
                            )}
                        </div>
                    </Popover.Body>
                </form>
            </Popover>
        );
    }, [rota, popoverState]);

    return (
        <>
            <div className='top-bar'>
                {/* <Navbar bg="light" expand={false}>
                    <Navbar.Toggle aria-controls="offcanvasNavbar" />

                    <Navbar.Offcanvas
                        id="offcanvasNavbar"
                        aria-labelledby="offcanvasNavbarLabel"
                        placement="start"
                        className='side-bar side-bar--collapsed'
                    >
                        <SideBar />
                    </Navbar.Offcanvas>
                </Navbar> */}
                
                <div className='top-bar__section top-bar__section--date'>
                    <a onClick={datePrev} className='date-arrow date-arrow--prev'></a>
                    <strong>{props.currentMonth()}</strong>
                    <a onClick={dateNext} className='date-arrow date-arrow--next'></a>
                </div>

                <div className='top-bar__section'>
                    <Dropdown>
                        <Dropdown.Toggle id="dropdown-basic">
                            <Department style={{transform: "scale(0.7)"}} />
                            {props.department.name.charAt(0).toUpperCase() + props.department.name.slice(1)}
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                            {props.departments.map((department, i) => {
                                return (
                                    <Dropdown.Item key={i} onClick={() => props.setDepartment(department)}>
                                        {department.name.charAt(0).toUpperCase() + department.name.slice(1)}
                                    </Dropdown.Item>
                                );
                            })}
                            <Dropdown.Item onClick={() => props.setDepartment({
                                'id': 0,
                                'name': 'All Departments'
                            })}>
                                All
                            </Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </div>
                
                {(props.employees.length > 0 && user.hasRoles(['admin'])) && (
                    <>
                        <div className='top-bar__section'>  
                            <Dropdown>
                                <Dropdown.Toggle id="dropdown-basic">
                                    <Tool style={{transform: "scale(0.8)"}} />
                                    <span>Tools</span>
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <Dropdown.Item onClick={handleDuplicateRota}>Duplicate existing rota</Dropdown.Item>
                                    <Dropdown.Item onClick={handleDeleteShifts}>Clear week</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </div>

                        <div className='top-bar__section'>
                            <Dropdown as={ButtonGroup}>
                                <Button onClick={(e) => props.onAssign(e)}>
                                    <Assign style={{transform: "scale(0.8)"}} />
                                    <span>Match all</span>
                                </Button>

                                <Dropdown.Toggle split id="dropdown-split-basic" />

                                <Dropdown.Menu>
                                    <Dropdown.Item onClick={(e) => props.onAssign(e, true)}>Re-match all</Dropdown.Item>
                                    <Dropdown.Item onClick={(e) => props.onDeleteAssign(e)}>Remove all matchings</Dropdown.Item>

                                </Dropdown.Menu>
                            </Dropdown>
                        </div>
                        
                        <div className='top-bar__section'>                
                            {/* <Button onClick={handleSaveRota}>Save</Button> */}
                            <OverlayTrigger 
                                trigger="click"
                                rootClose
                                placement="bottom"
                                overlay={savePopover}
                            >
                                <Button>
                                    <Bookmark style={{transform: "scale(0.8)", position: "relative", left: "-1px"}} />
                                    {(Object.keys(rota).length === 0) ? (
                                        <span>Save</span>
                                    ) : (
                                        <span>Saved</span>
                                    )}
                                </Button>
                            </OverlayTrigger>
                        </div>

                        
                        <div className='top-bar__section'>
                            {!props.showEmployeeDashboard ? (
                                <Button onClick={props.onShowEmployeeDashboard}>
                                    <Calender style={{ marginRight: "7px" }}/>
                                    Employee Dashboard
                                </Button>
                            ) : (
                                <Button onClick={props.onHideEmployeeDashboard}>
                                    <Calender style={{ marginRight: "7px" }}/>
                                    Dashboard
                                </Button>
                            )}
                        </div>
                    </>
                )}
            </div>

            <DuplicateRotaModal 
                rotas={rotas}
                weekDays={props.weekDays}
                getDateChangeData={props.getDateChangeData}
                show={duplicateRotaModalShow}
                onHide={() => setDuplicateRotaModalShow(false)}
            />
        </>
    );
}