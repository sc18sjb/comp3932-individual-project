import Crying from '../../../public/images/crying.png';
import Sad from '../../../public/images/sad.png';
import Neutral from '../../../public/images/neutral.png';
import Smile from '../../../public/images/smile.png';
import Smiling from '../../../public/images/smiling.png';
import { useState } from 'react';

export default function RatingFaces({handleRateShifts, currentRating=-1}) {
    const faces = [Crying, Sad, Neutral, Smile, Smiling];
    const [selected, setSelected] = useState(currentRating - 1);

    // Intersept the current selection to set the current selected.
    const interceptRating = (e, rating) => {
        setSelected(rating);
        handleRateShifts(e, rating + 1)
    };

    // If a face is selected, add the correct class to it.
    const isSelected = (rating) => {
        if (rating === selected)
            return 'rating-faces__selectable--selected';
        return '';
    };

    return (
        <div className='rating-faces d-flex justify-content-center w-100'>
            {faces.map((face, i) => {
                return (
                    <div className={'rating-faces__selectable ' + isSelected(i)}
                        key={i}
                        onClick={(e) => interceptRating(e, i)}
                    >
                        <img 
                            src={face}
                            className='rating-faces__img'
                        />
                    </div>
                )
            })}
        </div>
    );  
}