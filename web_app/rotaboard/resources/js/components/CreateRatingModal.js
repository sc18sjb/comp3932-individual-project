import Modal from 'react-bootstrap/Modal';
import EmployeeContext from '../auth/EmployeeContext';
import RatingFaces from "./RatingFaces";
import { useContext, useCallback } from "react";
import api from '../utils/api';

export default function CreateRatingModal({emptyRatings, setRatings, ...props}) {
    const user = useContext(EmployeeContext);

    const handleRateShifts = useCallback(async (e, rating) => {
        e.preventDefault();
        const ids = emptyRatings.map((e) => e.shift_id);

        const res = await api.createManyRatings({
            employee_id: user.id,
            shift_ids: ids,
            rating: rating
        });

        if (res.success) {
            var ratings = [];
            ids.forEach(id => {
                ratings.push({
                    employee_id: user.id,
                    shift_id: id,
                    rating: rating,
                    provider: 'EMPLOYEE' // An employee created the rating.
                });
            });

            setRatings(ratings);

            props.onHide();
        } else {
            console.log(res);

            // We hide the popup anyway, even if there is an error.
            props.onHide();
        }
    }, [user, emptyRatings]);

    return (
        <Modal
            {...props}
            size="md"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    How would you rate your shifts?
                </Modal.Title>
            </Modal.Header>
                
            <Modal.Body style={{paddingTop: "0"}}>
                <div className='modal-content__value mb-3'>You can rate individual shifts by viewing the shift details</div>

                <RatingFaces 
                    handleRateShifts={handleRateShifts}
                />
            </Modal.Body>
        </Modal>
    ); 
}