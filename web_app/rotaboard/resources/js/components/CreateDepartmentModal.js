import { useState, useMemo, useEffect } from "react";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import helpers from "../utils/helpers";
import api from "../utils/api";

const defaultState = {
    name: '',
    color: '',
    errors: []
};

export default function CreateDepartmentModal({department, departments, setDepartments, ...props}) {
    const [state, setState] = useState(defaultState);
    const colors = ['#3dd598', '#4dc0b5', '#6cb2eb', '#ffc542', '#ff6961', '#f66d9b', '#9561e2', '#6574cd', '#0062ff', '#ced4da'];

    // When the department changes, we want to update the state to be the new department, or be empty.
    useEffect(() => {
        if (Object.keys(department).length !== 0) {
            setState({
                name: department.name,
                color: department.color,
                errors: []
            });
        } else {
            setState(defaultState);            
        }
    }, [department])

    const handleSaveDepartment = async (e) => {
        e.preventDefault();

        const res = await api.createDepartment(state);
        if (res.success) {
            props.onHide();

            const finalState = state;
            finalState.id = res.data; // Set the id of the department to the insert id.

            // Insert the new department.
            setDepartments([...departments, state]);

            // Reset the state.
            setState(defaultState);
        } else {
            // We set the errors
            setState(prevState => ({
                ...prevState,
                ['errors']: res.errors
            }));
        }
    };

    const handleUpdateDepartment = async (e) => {
        e.preventDefault();

        const res = await api.updateDepartment(department.id, state);
        if (res.success) {
            props.onHide();
            
            const finalState = state;
            finalState.id = department.id
            
            // Update the existing department.
            const exists = departments.findIndex((e) => e.id === department.id);
            setDepartments((departments) => {
                return [
                    ...departments.slice(0, exists),
                    {
                        ...finalState
                    },
                    ...departments.slice(exists + 1)
                ]
            });
        } else {
            // We set the errors
            setState(prevState => ({
                ...prevState,
                ['errors']: res.errors
            }));
        }
    };

    const selectColor = (color) => {
        setState(prevState => ({
            ...prevState,
            ['color']: color
        }));
    };

    const colorSelected = (color) => {
        if (color === state.color)
            return 'modal-content__color--selected';
        return '';
    };

    return (
        <Modal
            {...props}
            size="md"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <form className='modal-content__form'>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        {(Object.keys(department).length === 0) ? (
                            <span>Add Department</span>
                        ) : (
                            <span>Update Department</span>
                        )}
                    </Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    {(state.errors.length > 0) && (
                        <div className='alert alert--full alert-warning mb-4'>
                            <ul>
                                {state.errors.map((error, i) => {
                                    return (
                                        <li key={i}>{error}</li>
                                    );  
                                })}
                            </ul>
                        </div>
                    )}

                    <div className='modal-content__group'>
                        <h4>Name</h4>
                        <input className='modal-content__input' type='text' name='name' onChange={(e) => helpers.handleChange(e, setState)} value={state.name} />
                    </div>

                    <div className='modal-content__group'>
                        <h4>Color</h4>
                        <div className='d-flex flex-wrap justify-content-center modal-content__color-group'>
                            {colors.map((color, i) => {
                                return (
                                    <div 
                                        className={'modal-content__color ' + colorSelected(color)}
                                        key={i}
                                        style={{backgroundColor: color}}
                                        onClick={() => selectColor(color)}
                                    >
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </Modal.Body>

                <Modal.Footer>
                    {(Object.keys(department).length === 0) ? (
                        <Button onClick={handleSaveDepartment}>Save</Button>
                    ) : (
                        <Button onClick={handleUpdateDepartment}>Update</Button>
                    )}
                </Modal.Footer>
            </form>
        </Modal>
    );
}