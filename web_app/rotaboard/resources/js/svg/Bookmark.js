import * as React from "react";

const SvgBookmark = (props) => (
    <svg xmlns="http://www.w3.org/2000/svg" width={30} height={30}  {...props}>
        <path d="m23 27-8-7-8 7V5a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v22z" />
    </svg>
);

export default SvgBookmark;

