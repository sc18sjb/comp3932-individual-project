import * as React from "react";

const SvgDepartment = (props) => (
    <svg width={50} height={50} viewBox="5 20 85 55" xmlns="http://www.w3.org/2000/svg" {...props}>
        <rect x={19} y={18.92} width={60} height={16} rx={4} ry={4} />
        <rect x={19} y={40.92} width={27} height={16} rx={4} ry={4} />
        <rect x={19} y={62.92} width={27} height={16} rx={4} ry={4} />
        <rect x={52} y={40.92} width={27} height={16} rx={4} ry={4} />
        <rect x={52} y={62.92} width={27} height={16} rx={4} ry={4} />
    </svg>
);

export default SvgDepartment;

