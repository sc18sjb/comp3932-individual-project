import * as React from "react";

const SvgPlus = (props) => (
    <svg xmlns="http://www.w3.org/2000/svg" width={30} height={30} {...props}>
        <path
            style={{
                stroke: "none",
                fillRule: "nonzero",
                fillOpacity: 1,
            }}
            d="M15 0C6.715 0 0 6.715 0 15s6.715 15 15 15 15-6.715 15-15S23.285 0 15 0Zm7.64 17.23h-5.3v5.407h-4.613V17.23H7.363v-4.613h5.364V7.36h4.613v5.258h5.297v4.613Zm0 0"
        />
    </svg>
);

export default SvgPlus;

