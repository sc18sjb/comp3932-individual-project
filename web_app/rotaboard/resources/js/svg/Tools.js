import * as React from "react";

const SvgTools = (props) => (
    <svg xmlns="http://www.w3.org/2000/svg" width={30} height={30} {...props}>
        <path
            style={{
                stroke: "none",
                fillRule: "nonzero",
                fillOpacity: 1,
            }}
            d="M25.352 7.168c-.008-.008-1.793 1.754-5.336 5.293l-2.489-2.488 5.301-5.301a7.105 7.105 0 0 0-8.695 1.078 7.158 7.158 0 0 0-1.86 6.906l.055.196-8.305 8.304a.464.464 0 0 0 0 .66l4.141 4.145c.184.18.48.18.664 0l8.3-8.3.196.05a7.147 7.147 0 0 0 6.914-1.852 7.155 7.155 0 0 0 1.114-8.691Zm0 0"
        />
    </svg>
);

export default SvgTools;

