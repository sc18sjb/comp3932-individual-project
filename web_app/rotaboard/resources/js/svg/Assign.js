import * as React from "react";

const SvgAssign = (props) => (
    <svg xmlns="http://www.w3.org/2000/svg" width={30} height={30} {...props}>
        <path
            style={{
                stroke: "none",
                fillRule: "nonzero",
                fillOpacity: 1,
            }}
            d="M20.668 8.633a5.678 5.678 0 0 1-5.676 5.676 5.678 5.678 0 0 1-5.676-5.676 5.678 5.678 0 0 1 5.676-5.676 5.678 5.678 0 0 1 5.676 5.676ZM10 22.207a2.334 2.334 0 0 1 4.043-1.5l3.215 3.461 5.699-6.36A14.017 14.017 0 0 0 15 15.458 13.455 13.455 0 0 0 4.582 20a.823.823 0 0 0-.164.508V25c0 .902.715 1.64 1.617 1.668h7.14l-2.558-2.75A2.362 2.362 0 0 1 10 22.207ZM23.965 26.668A1.667 1.667 0 0 0 25.582 25v-3.133l-4.273 4.801Zm0 0"
        />
        <path
            style={{
                stroke: "none",
                fillRule: "nonzero",
                fillOpacity: 1,
            }}
            d="M28.14 15.516a.84.84 0 0 0-1.184.066l-9.683 10.836-4.332-4.66a.835.835 0 0 0-1.175-.09.834.834 0 0 0 0 1.184l5.566 6 10.867-12.184a.834.834 0 0 0-.058-1.152Zm0 0"
        />
    </svg>
);

export default SvgAssign;

