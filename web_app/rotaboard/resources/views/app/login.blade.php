@extends('layouts.layout')

@section('pageTitle', '| Login')

@section('content')

    <div id='login'>
        <div class='login__return d-flex justify-content-center'>
            <a href="{{ url('/') }}" class='login__return-link'>
                Go back to Rotaboard.co.uk
            </a>
        </div>

        <div class='login__content d-flex justify-content-center align-items-center flex-column'>
            <div class='login__container'>
                <div class='login__logo d-flex align-items-center mb-4'>
                    <img src="{{ asset('images/rotaboard.png') }}" alt='rotaboard logo' />
                    <h2>RotaBoard</h2>
                </div>

                <div class='login__divider'></div>

                <h3 class='mb-5'>Log in to your account</h3>


                @if($errors->any())
                    <div class='alert alert--full alert-warning mb-4'>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class='login__form w-100' method='POST' action="{{ url('/login') }}">
                    {{ csrf_field() }}

                    <div class='login__form-group'>
                        <h4>Email</h4>
                        <input class='login__form-input' type='email' name='email' value="{{ old('email') }}" />
                    </div>

                    <div class='login__form-group'>
                        <h4>Password</h4>
                        <input class='login__form-input' type='password' name='password' />
                    </div>

                    <input type='submit' class='btn btn-primary btn--full' value='Log in' />
                </form>

                <div class='login__divider'></div>
                <span>Don't have an account? <a href="{{ url('/register') }}" class='link'>Register</a></span>
            </div>
        </div>
    </div>



@endsection