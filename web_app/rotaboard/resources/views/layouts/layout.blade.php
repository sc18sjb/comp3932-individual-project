<!DOCTYPE html>
<html> 
    <head lang="en">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie-edge">
        <title>Rotaboard @yield('pageTitle')</title>
        
        <link rel="icon" href="{{ asset('images/rotaboard.png') }}">
        <link rel="stylesheet" type="text/css" href="{{ mix('css/app.css') }}" >
    </head>

    <body>
        @yield('content')

        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>