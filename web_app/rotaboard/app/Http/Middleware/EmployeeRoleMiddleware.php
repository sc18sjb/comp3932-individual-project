<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class EmployeeRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $roles)
    {
        if ($request->user()->hasRoles(explode('|', $roles)))
            return $next($request);
        return new JsonResponse([
            'message' => 'Forbidden.'
        ], 403);
    }
}
