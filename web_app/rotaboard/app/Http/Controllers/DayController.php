<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Day;
use Symfony\Component\HttpFoundation\JsonResponse;

class DayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return new JsonResponse([
            'success' => true,
            'data' => Day::where('company_id', $request->user()->company_id)->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'day' => 'required|integer|min:0|max:6'
        ]);

        $day = new Day();
        $day->name = Day::$days[$request->day];
        $day->day = $request->day;
        $day->company_id = $request->user()->company_id;
        $day->save();

        return new JsonResponse([
            'success' => true,
            'message' => 'Day created successfully'
        ]);
    }

    public static function createDefaultDays(Request $request)
    {
        $data = array();
        $day = new Day();

        for ($d = 0; $d <= 4; $d++) {
            array_push($data, [
                'name' => Day::$days[$d],
                'day' => $d,
                'company_id' => $request->user()->company_id,
                'created_at' => $day->freshTimestamp(),
                'updated_at' => $day->freshTimestamp()
            ]);
        }

        $day::insert($data);

        return new JsonResponse([
            'success' => true,
            'message' => 'Days created successfully'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
