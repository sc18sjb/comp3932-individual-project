<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ShiftDate;
use App\Models\ShiftRota;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;


class ShiftDateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function store(Request $request)
    {
        // Validate the request fields.
        $request->validate([
            'shift_id' => 'required|exists:shifts,id',
            'date' => 'required|date_format:Y-m-d'
        ]);

        $existingShiftDate = ShiftDate::where('shift_id', $request->shift_id)
                                ->where('date', $request->date)
                                ->first();
        
        // We have a unique constraint of shift_id and date so we check if that has been violated first.
        if ($existingShiftDate)
            return new JsonResponse([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'insertError' => ['Existing shift for given date.']
                ]
            ], 409);

        $shiftDate = new ShiftDate();
        $shiftDate->shift_id = $request->shift_id;
        $shiftDate->date = $request->date;
        $shiftDate->save();
        $shiftDateId = $shiftDate->id;

        return new JsonResponse([
            'success' => true,
            'message' => 'Shift date created successfully.',
            'data' =>  $shiftDateId
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    public static function updateSingle(Request $request)
    {
        $request->validate([
            'id' => 'required|integer|exists:shift_dates,id',
            'shift_id' => 'required|exists:shifts,id',
            'date' => 'required|date_format:Y-m-d'
        ]);

        $existingShiftDate = ShiftDate::where('shift_id', $request->shift_id)
                                ->where('date', $request->date)
                                ->first();

        // We have a unique constraint of shift_id and date so we check if that has been violated first.
        if ($existingShiftDate)
            return new JsonResponse([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'insertError' => ['Existing shift for given date.']
                ]
            ], 409);

        $shiftDate = ShiftDate::where('id', $request->id)->first();
        $shiftDate->shift_id = $request->shift_id;
        $shiftDate->date = $request->date;
        $shiftDate->save();

        return new JsonResponse([
            'success' => true,
            'message' => 'Shift date created successfully.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->merge(['id' => $request->route('id')]);
        $request->validate([
            'id' => 'required|integer|exists:shift_dates,id'
        ]);

        ShiftDate::where('id', $request->id)->delete();

        return new JsonResponse([
            'success' => true,
            'message' => 'Successfully deleted shift'
        ]);
    }
    
    public function destroyMany(Request $request)
    {
        $request->merge(['start' => $request->route('start'), 'end' => $request->route('end')]);
        $request->validate([
            'start' => 'required|date_format:Y-m-d',
            'end' => 'required|date_format:Y-m-d'
        ]);

        ShiftDate::leftJoin('shifts', 'shifts.id', '=', 'shift_dates.shift_id')
                            ->leftJoin('departments', 'departments.id', '=', 'shifts.department_id')
                            ->where('departments.company_id', $request->user()->company_id)
                            ->whereBetween('date', [$request->start, $request->end])
                            ->delete();

        return new JsonResponse([
            'success' => true,
            'message' => 'Shift dates deleted successfully.'
        ]);
    }
}
