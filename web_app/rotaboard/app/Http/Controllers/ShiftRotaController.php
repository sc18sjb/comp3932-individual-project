<?php

namespace App\Http\Controllers;

use App\Models\ShiftRota;
use App\Models\ShiftDate;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShiftRotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'start' => 'date_format:Y-m-d',
            'end' => 'required_with:start|date_format:Y-m-d'
        ]);

        $start = $request->get('start');
        $end = $request->get('end');

        $shiftRotas = ShiftRota::where('company_id', $request->user()->company_id);

        if ($start != null && $end != null) {
            $shiftRotas->where('start', $start);
            $shiftRotas->where('end', $end);
        }

        return new JsonResponse([
            'success' => true,
            'data' => $shiftRotas->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'start' => 'required|date_format:Y-m-d',
            'end' => 'required|date_format:Y-m-d',
            'name' => 'required'
        ]);
        

        // Check if a rota for the given week already exists.
        $existingRota = ShiftRota::where('start', $request->start)
                            ->where('end', $request->end)
                            ->where('company_id', $request->user()->company_id)->first();
        
        // If one does exist, we will just update the name if different
        if ($existingRota && $existingRota->name != $request->name) {
            $existingRota->name = $request->name;
            $existingRota->save();

            return new JsonResponse([
                'success' => true,
                'Shift rota updated successfully.'
            ]);
        } else if ($existingRota)     
            return new JsonResponse([
                    'message' => 'Rota already exists'
                ], 409);

        try {
            $shiftRota = new ShiftRota();
            $shiftRota->company_id = $request->user()->company_id;
            $shiftRota->start = $request->start;
            $shiftRota->end = $request->end;
            $shiftRota->name = $request->name;
            $shiftRota->save();
            $shiftRotaId = $shiftRota->id;

            return new JsonResponse([
                'success' => true,
                'message' => 'Shift rota created successfully.',
                'data' => $shiftRotaId
            ]);
        } catch (Exception $e) {
            return new JsonResponse([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'insertError' => [$e->getMessage()]
                ]
            ], 500);
        }
    }

    /**
     * Duplicate an existing range of shift dates to a new week.
     * 
     * @return \Illuminate\Http\Response
     */
    public function duplicate(Request $request)
    {
        $request->validate([
            'rota_id' => 'required|exists:shift_rotas,id',
            'forward' => 'required|integer|min:1'
        ]);

        $rota = ShiftRota::where('id', $request->rota_id)->where('company_id', $request->user()->company_id)->first();
        if (!$rota)
            return new JsonResponse([
                'message' => 'Unauthorized.'
            ], 401);

        $shiftDates = DB::table('shift_dates')
            ->leftJoin('shifts', 'shifts.id', '=', 'shift_dates.shift_id')
            ->leftJoin('departments', 'departments.id', '=', 'shifts.department_id')
            ->where('departments.company_id', $request->user()->company_id)
            ->whereBetween('date', [$rota->start, $rota->end])
            ->get();

        $newShiftDates = array();
        $shiftDateObj = new ShiftDate();

        foreach ($shiftDates as $shiftDate) {
            // Bring the shift forward by n number of weeks
            $date = strtotime($shiftDate->date);
            $date = strtotime("+" . ($request->forward * 7) . " days", $date);
            $date = date('Y-m-d', $date);

            array_push($newShiftDates, [
                'shift_id' => $shiftDate->shift_id,
                'date' => $date,
                'created_at' => $shiftDateObj->freshTimestamp(),
                'updated_at' => $shiftDateObj->freshTimestamp()
            ]);
        }

        try {
            $shiftDateObj::insert($newShiftDates);

            return new JsonResponse([
                'success' => true,
                'message' => 'Shifts duplicated successfully.'
            ]);
        } catch (Exception $e) {
            return new JsonResponse([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'insertError' => [$e->getMessage()]
                ]
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->merge(['id' => $request->route('id')]);
        $request->validate([
            'id' => 'required|integer|exists:shift_rotas,id'
        ]);

        ShiftRota::where('id', $request->id)->delete();

        return new JsonResponse([
            'success' => true,
            'message' => 'Successfully deleted rota.'
        ]);
    }
}
