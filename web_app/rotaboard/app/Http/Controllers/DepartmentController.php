<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Department;
use Illuminate\Http\JsonResponse;
use Exception;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function index(Request $request)
    {
        return new JsonResponse([
            'success' => true,
            'data' => Department::where('company_id', $request->user()->company_id)->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the request fields.
        $request->validate([
            'name' => 'required|max:50',
            'color' => 'required|max:30'
        ]);

        $existingDepartmentName = Department::where('name', $request->name)
                                ->where('company_id', $request->user()->company_id)
                                ->first();
        if ($existingDepartmentName)
            return new JsonResponse([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'insertError' => ['Department name already taken.']
                ]
            ], 409);

        $existingDepartmentColor = Department::where('color', $request->color)
                                    ->where('company_id', $request->user()->company_id)
                                    ->first();
        if ($existingDepartmentColor)
            return new JsonResponse([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'insertError' => ['Department color already taken.']
                ]
            ], 409);

        $department = new Department();
        $department->name = $request->name;
        $department->color = $request->color;
        $department->company_id = $request->user()->company_id;
        $department->save();
        $departmentId = $department->id;

        return new JsonResponse([
            'success' => true,
            'message' => 'Department created successfully.',
            'data' => $departmentId
        ]);      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->merge(['id' => $request->route('id')]);
        $request->validate([
            'id' => 'required|integer|exists:departments,id',
            'name' => 'required|max:50',
            'color' => 'required|max:30'
        ]);

        $existingDepartmentName = Department::where('name', $request->name)
                                ->where('company_id', $request->user()->company_id)
                                ->first();
        if ($existingDepartmentName && $existingDepartmentName->id != $request->id)
            return new JsonResponse([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'insertError' => ['Department name already taken.']
                ]
            ], 409);

        $existingDepartmentColor = Department::where('color', $request->color)
                                    ->where('company_id', $request->user()->company_id)
                                    ->first();
        if ($existingDepartmentColor && $existingDepartmentColor->id != $request->id)
            return new JsonResponse([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'insertError' => ['Department color already taken.']
                ]
            ], 409);                      

        $department = Department::where('id', $request->id)->first();
        $department->name = $request->name;
        $department->color = $request->color;
        $department->save();

        return new JsonResponse([
            'success' => true,
            'message' => 'Department updated successfully.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->merge(['id' => $request->route('id')]);
        $request->validate([
            'id' => 'required|integer|exists:departments,id'
        ]);

        Department::where('id', $request->id)->delete();

        return new JsonResponse([
            'success' => true,
            'message' => 'Successfully deleted department.'
        ]);
    }
}
