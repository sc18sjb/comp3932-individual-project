<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\EmployeeRole;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\JsonResponse;

class ApiAuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $employee = Employee::where('email', $request->email)->first();
        $role = EmployeeRole::where('id', $employee->role_id)->first();

        if (!$employee || !Hash::check($request->password, $employee->password)) {
            return new JsonResponse([
                'message' => 'The given data was invalid.',
                'errors' => 'The provided credentials are incorrect.'
            ], 500);
        }
        
        $employee->token = $employee->createToken('rotaboard-token')->plainTextToken;

        return new JsonResponse([
            'success' => true,
            'message' => 'Successfully logged in.',
            'data' => $employee
        ]);
    }

    public function register(Request $request) {
        $request->validate([
            'company' => 'required',
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|email|unique:employees',
            'password' => 'required'
        ]);

        try {
            DB::beginTransaction();

            // First we create the new company.
            $company = new Company();
            $company->name = $request->company;
            $company->save();
            $companyId = $company->id;

            // Then we create the new employee (admin) for that company.
            $employee = new Employee();
            $employee->fname = $request->fname;
            $employee->lname = $request->lname;
            $employee->email = $request->email;
            $employee->password = Hash::make($request->password);
            $employee->role_id = 1; // Default make whoever creates the company an admin.
            $employee->company_id = $companyId;
            $employee->save();

            DB::commit();
        } catch(Exception $e) {
            DB::rollBack();
            return new JsonResponse([
                'message' => 'There was an error creating your account.'
            ], 400);
        }

        $employee->token = $employee->createToken('rotaboard-token')->plainTextToken;

        return new JsonResponse([
            'success' => true,
            'message' => 'Successfully created employee, logged in.',
            'data' => $employee
        ]);
    }

    public function logout(Request $request) 
    {
        // Delete the user's token to they are no longer authenticated
        $request->user()->currentAccessToken()->delete();
        return new JsonResponse([
            'success' => true,
            'message' => "Successfully logged out."
        ]);
    }

    public function getCurrentEmployee(Request $request) {
        $employee = $request->user();
        $employee->role = EmployeeRole::where('id', $employee->role_id)->first()->name;

        return new JsonResponse([
            'success' => true,
            'data' => $employee
        ]);
    }
}
