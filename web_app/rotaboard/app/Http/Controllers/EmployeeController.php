<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function index(Request $request)
    {
        $employees = DB::table('employees')
            ->selectRaw('
                employees.id, employees.fname, employees.lname, employees.email, employees.role_id, employee_roles.name AS role,
                IFNULL(GROUP_CONCAT(DISTINCT employee_departments.department_id), NULL) AS departments,
                IFNULL(GROUP_CONCAT(DISTINCT employee_availabilities.availability_id 
                    ORDER BY employee_availabilities.preference_order ASC), NULL) AS availability_preference')
            ->leftJoin('employee_departments', 'employee_departments.employee_id', '=', 'employees.id')
            ->leftJoin('employee_availabilities', 'employee_availabilities.employee_id', '=', 'employees.id')
            ->leftJoin('employee_roles', 'employee_roles.id', '=', 'employees.role_id')
            ->where('employees.company_id', $request->user()->company_id)
            ->groupBy('employees.id')
            ->orderByRaw(DB::raw('CASE WHEN employees.id = ' . $request->user()->id . ' THEN 0 ELSE 1 END'))
            ->get();

        return new JsonResponse([
            'success' => true,
            'data' => $employees
        ]);
    }

    public function setEmployeeAvailability(Request $request, $employeeId, $availabilityIds)
    {
        // Create a new post request to the employee availability controller.
        $response = EmployeeAvailabilityController::store($this->createRequest($request, [
            'employee_id' => $employeeId,
            'availability_ids' => $availabilityIds
        ]));

        return $response;
    }

    public function setEmployeeDepartments(Request $request, $employeeId, $departmentIds)
    {
        $response = EmployeeDepartmentController::store($this->createRequest($request, [
            'employee_id' => $employeeId,
            'department_ids' => $departmentIds
        ]));

        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // If we are on production we want to use the +10 id scheme
        $request->role_id = app()->environment('production') ? $request->role_id + (9 * ($request->role_id - 1)) : $request->role_id;

        // Validate the request fields.
        $request->validate([
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|email|unique:employees', // Validate email as unique
            'password' => 'required',
            'role_id' => 'required|integer|exists:employee_roles,id'
        ]);

        DB::beginTransaction();
        // Create and save the employee
        $employee = new Employee();
        $employee->fname = $request->fname;
        $employee->lname = $request->lname;
        $employee->email = $request->email;
        $employee->password = Hash::make($request->password);
        $employee->role_id = $request->role_id;
        $employee->company_id = $request->user()->company_id;
        $employee->save();
        $employeeId = $employee->id; // get the last inset ID.

        // We want to set the employees availability as well.
        if ($request->availability_ids != null) {
            $response = $this->setEmployeeAvailability($request, $employeeId, $request->availability_ids);

            if ($response->status() != 200 || !isset($response->getData()->success)) {
                DB::rollBack();
                return $response;
            }

            unset($response);
        }

        // We want to set the employees departments as well.
        if ($request->department_ids != null) {
            $response = $this->setEmployeeDepartments($request, $employeeId, $request->department_ids);

            if ($response->status() != 200 || !isset($response->getData()->success)) {
                DB::rollBack();
                return $response;
            }

            unset($response);
        }

        DB::commit();
        return new JsonResponse([
            'success' => true,
            'message' => 'Employee created successfully.',
            'data' => $employeeId
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // show an emplpoyee
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // If we are on production we want to use the +10 id scheme
        $request->role_id = app()->environment('production') ? $request->role_id + (9 * ($request->role_id - 1)) : $request->role_id;

        $request->merge(['id' => $request->route('id')]);
        $request->validate([
            'id' => 'required|integer|exists:employees,id',
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|email|unique:employees,email,' . $request->id,
            'role_id' => 'required|integer|exists:employee_roles,id'
        ]);

        DB::beginTransaction();
        $employee = Employee::where('id', $request->id)->first();
        $employee->fname = $request->fname;
        $employee->lname = $request->lname;
        $employee->email = $request->email;
        $employee->role_id = $request->role_id;

        // We might not want to update their password so we check if it is given.
        if ($request->password != null)
            $employee->password = Hash::make($request->password);
        $employee->save();

        //TODO: We only need to do this is the department or availability ids change.

        // We delete all the existing employee availabilities.
        $response = EmployeeAvailabilityController::destroy($this->createRequest($request, [
            'id' => $request->id
        ]));

        if ($response->status() != 200 || !isset($response->getData()->success)) {
            DB::rollBack();
            return $response;
        }

        // We delete all the existing employee departments.
        $response = EmployeeDepartmentController::destroy($this->createRequest($request, [
            'id' => $request->id
        ]));

        if ($response->status() != 200 || !isset($response->getData()->success)) {
            DB::rollBack();
            return $response;
        }

        if ($request->availability_ids != null) {
            // Then we assign the new employee availabilities.
            $response = $this->setEmployeeAvailability($request, $request->id, $request->availability_ids);

            if ($response->status() != 200 || !isset($response->getData()->success)) {
                DB::rollBack();
                return $response;
            }

            unset($response);
        }

        if ($request->department_ids != null) {
            // Then we assign the new employee departments.
            $response = $this->setEmployeeDepartments($request, $request->id, $request->department_ids);

            if ($response->status() != 200 || !isset($response->getData()->success)) {
                DB::rollBack();
                return $response;
            }

            unset($response);
        }

        DB::commit();
        return new JsonResponse([
            'success' => true,
            'message' => 'Employee updated successfully.'
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->merge(['id' => $request->route('id')]);
        $request->validate([
            'id' => 'required|integer|exists:employees,id'
        ]);

        if ($request->id == $request->user()->id)
            return new JsonResponse([
                'Forbidden.'
            ], 403);

        Employee::where('id', $request->id)->delete();

        return new JsonResponse([
            'success' => true,
            'message' => 'Successfully deleted employee.'
        ]);
    }
}
