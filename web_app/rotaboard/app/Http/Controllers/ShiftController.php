<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Shift;
use App\Models\Department;
use App\Models\ShiftDate;
use DateTime;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class ShiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function index(Request $request)
    {   
        $request->validate([
            'start' => 'date_format:Y-m-d',
            'end' => 'required_with:start|date_format:Y-m-d',
        ]);

        $start = $request->get('start');
        $end = $request->get('end');
        $department = $request->get('department');
        $ratings = $request->get('ratings');

        $shifts = DB::table('shifts')
                ->selectRaw('
                    shifts.id, shifts.department_id, shifts.day_id, shifts.start, shifts.end,
                    shifts.start as display_start, shifts.end as display_end, shifts.min, shifts.max,
                    shift_dates.date, shift_dates.date as display_date, shift_dates.id AS date_id, 
                    departments.name AS department_name, 
                    departments.color AS department_color, 
                    COUNT(employee_shifts.employee_id) AS assigned, 
                    IFNULL(GROUP_CONCAT(DISTINCT employee_shifts.employee_id), NULL) AS employee_ids')
                ->leftJoin('shift_dates', 'shift_dates.shift_id', '=', 'shifts.id')
                ->leftJoin('departments', 'departments.id', '=', 'shifts.department_id')
                ->leftJoin('employee_shifts', 'employee_shifts.shift_date_id', '=', 'shift_dates.id')
                ->where('departments.company_id', $request->user()->company_id);

        if ($start != null || $end != null) 
            $shifts = $shifts->whereBetween('shift_dates.date', [$start, $end]);
        
        // Filter by department id
        if ($department != null)
            $shifts = $shifts->where('shifts.department_id', $department);

        $shifts = $shifts
            ->groupBy('shift_dates.id')
            ->orderBy('shifts.start', 'asc')
            ->get();

        return new JsonResponse([
            'success' => true,
            'data' => $shifts
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the request fields.
        $request->validate([
            'department_id' => 'required|exists:departments,id',
            'day_id' => 'required|exists:days,id',
            'start' => 'required|date_format:H:i',
            'end' => 'required|date_format:H:i|time_length_field:start',
            'min' => 'required|integer|less_than_field:max',
            'max' => 'required|integer|min:1|greater_than_field:min'
        ]);

        // We want to prevent the possibility of users creating a shift from a department of another company.
        $departmentCheck = Department::where('id', $request->department_id)->where('company_id', $request->user()->company_id)->first();
        if (!$departmentCheck)
            return new JsonResponse([
                'Unauthorized.'
            ], 401);

        // 1) We need to check if the same shift already exists and if so, get the ID.
        $sameShift = DB::table('shifts')
                    ->where('department_id', $request->department_id)
                    ->where('day_id', $request->day_id)
                    ->where('start', $request->start)
                    ->where('end', $request->end)
                    ->where('min', $request->min)
                    ->where('max', $request->max)
                    ->first();       

        // 2) There is no existing shift so we create one.
        DB::beginTransaction();
        if (!$sameShift) {  
            $shift = new Shift();
            $shift->department_id = $request->department_id;
            $shift->day_id = $request->day_id;
            $shift->start = $request->start;
            $shift->end = $request->end;
            $shift->min = $request->min;
            $shift->max = $request->max;
            $shift->save();
            $shiftId = $shift->id; // Get the last insert ID.
            $shiftDateId = -1;
        // 3) The shift already exists so we use the existing shift id.
        } else {
            // We assume there is only one record since it is impossible to create multiple of the same.
            $shiftId = $sameShift->id;
        }
        
        $shiftDateId = -1;
        if ($request->date != null) {
            // We need to make a seperate request for the shift date
            $response = ShiftDateController::store($this->createRequest($request, [
                'shift_id' => $shiftId,
                'date' => $request->date
            ]));

            if ($response->status() != 200 || !isset($response->getData()->success)) {
                DB::rollBack();
                return $response;
            }

            $shiftDateId = $response->getData()->data;
            unset($response);
        }
        
        DB::commit();

        return new JsonResponse([
            'success' => true,
            'message' => 'Shift created successfully.',
            'data' => $shiftDateId
        ]);
  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->merge(['id' => $request->route('id')]);
        $request->validate([
            'id' => 'required|integer|exists:shift_dates,id',
            'department_id' => 'required|exists:departments,id',
            'day_id' => 'required|exists:days,id',
            'start' => 'required|date_format:H:i',
            'end' => 'required|date_format:H:i|time_length_field:start',
            'min' => 'required|integer|less_than_field:max',
            'max' => 'required|integer|min:1|greater_than_field:min'
        ]);

        if ($request->date != null)
            unset($request['date']);

        DB::beginTransaction();
        $response = $this->store($request);

        if ($response->status() != 200 || !isset($response->getData()->success)) {
            return $response;
            DB::rollBack();
        }


        // 1) We need to check if an existing shift matches the new data.
        $sameShift = DB::table('shifts')
            ->where('department_id', $request->department_id)
            ->where('day_id', $request->day_id)
            ->where('start', $request->start)
            ->where('end', $request->end)
            ->where('min', $request->min)
            ->where('max', $request->max)
            ->first();

        // // 2) There is no existing shift so we update the current one.
        // if (!$sameShift) {
        //     $shiftId = ShiftDate::leftJoin('shifts', 'shifts.id', '=', 'shift_dates.shift_id')
        //                     ->where('shift_dates.id', $request->id)
        //                     ->first()->id;
        //     $shift = Shift::where('id', $shiftId)->first();

        //     $shift->department_id = $request->department_id;
        //     $shift->start = $request->start;
        //     $shift->end = $request->end;
        //     $shift->min = $request->min;
        //     $shift->max = $request->max;
        //     $shift->save();
        // // 3) The shift already exists so we update the existing shift_date to use the new shift_id. 
        // } else {
        if ($sameShift) {
            $response = ShiftDateController::updateSingle($this->createRequest($request, [
                'id' => $request->id,
                'shift_id' => $sameShift->id,
                'date' => ShiftDate::where('id', $request->id)->first()->date
            ]));

            if ($response->status() != 200 || !isset($response->getData()->success)) {
                DB::rollBack();
                return $response;
            }
        }

        DB::commit();

        return new JsonResponse([
            'success' => true,
            'message' => 'Shift updated successfully.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
