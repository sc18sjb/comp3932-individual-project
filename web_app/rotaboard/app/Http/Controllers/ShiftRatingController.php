<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\ShiftRating;
use App\Models\EmployeeShift;
use Illuminate\Http\JsonResponse;

class ShiftRatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function index(Request $request)
    {
        $request->validate([
            'ids' => 'array',
            'ids.*' => 'integer',
            'me' => 'bool' // Only show specific user ratings
        ]);

        // If shift ids aren't provided we will just return all of the shift ratings in order
        if ($request->get('ids') == null) {
            return new JsonResponse([
                'success' => true,
                'data' => ShiftRating::orderBy('employee_id', 'asc')
                    ->orderBy('shift_id', 'asc')
                    ->get()
            ]);
        }

        $ids = $request->get('ids');

        // If a shift hasn't been rated by an employee we want to create a default rating of 0.
        $first = DB::table('shifts')
            ->join('employees', 'shifts.id', '=', 'shifts.id')
            ->selectRaw('employees.id AS employee_id, shifts.id AS shift_id, 0 AS rating, "EMPLOYEE" as provider')
            ->whereNotIn('shifts.id', function($query) { // Get shifts an employee HAS NOT rated.
                $query->select('shift_id')
                    ->from(with(new ShiftRating())->getTable())
                    ->whereRaw('employee_id = employees.id');
            })
            ->whereIn('shifts.id', $ids)
            ->where('employees.company_id', $request->user()->company_id);

        // Then, we get the shifts the employee HAS rated, and their associated ratings
        $second = DB::table('shift_ratings')
            ->select('employee_id', 'shift_id', 'rating', 'provider')
            ->leftJoin('employees', 'employees.id', '=', 'shift_ratings.employee_id')
            ->whereIn('shift_id', $ids)
            ->where('employees.company_id', $request->user()->company_id);

        if ($request->user()->hasRoles(['employee']) || $request->get('me') != null) {
            $first->where('employees.id', $request->user()->id);
            $second->where('employees.id', $request->user()->id);
        }

        // We use two queries and join them together.
        $final = $second
            ->unionAll($first)
            ->orderBy('employee_id', 'asc')
            ->orderBy('shift_id', 'asc')
            ->get();

        return new JsonResponse([
            'success' => true,
            'data' => $final
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $systemProvider=false)
    {
        // Validate the request fields.
        $request->validate([
            'employee_id' => 'required|exists:employees,id',
            'shift_id' => 'required|exists:shifts,id',
            'rating' => 'required|numeric|min:0|max:5'
        ]);

        if ($request->user()->hasRoles(['employee'])) {
            // Employee's should not be able to rate other users shifts.
            if ($request->user()->id != $request->employee_id) 
                return new JsonResponse([
                    'message' => 'Forbidden.'
                ], 403);

            $assignedShift = EmployeeShift::leftJoin('shift_dates', 'shift_dates.id', '=', 'employee_shifts.shift_date_id')
                                ->leftJoin('shifts', 'shifts.id', '=', 'shift_dates.shift_id')
                                ->where('shifts.id', $request->shift_id)
                                ->where('employee_shifts.employee_id', $request->employee_id)
                                ->first();
            
            // Employees should only be able to rate shifts assigned to them.
            if (!$assignedShift)
                return new JsonResponse([
                    'message' => 'Forbidden.'
                ], 403);
        }

        $existingRating = ShiftRating::where('employee_id', $request->employee_id)
            ->where('shift_id', $request->shift_id)->first();

        // We update the rating if it already exists.
        if ($existingRating) {
            $existingRating->rating = $request->rating;
            $existingRating->save();

            return new JsonResponse([
                'success' => true,
                'message' => 'Shift rating updated successfully.'
            ]);
        }

        $shiftRating = new ShiftRating();
        $shiftRating->employee_id = $request->employee_id;
        $shiftRating->shift_id = $request->shift_id;
        $shiftRating->rating = $request->rating;
        // We don't want the provider to be a request param since we don't want any user to be able to change it.
        if ($systemProvider)
            $shiftRating->provider = 'SYSTEM';
        $shiftRating->save();

        return new JsonResponse([
            'success' => true,
            'message' => 'Shift rating created successfully'
        ]);
    }

    public function storeMany(Request $request)
    {
        $request->validate([
            'employee_id' => 'required|exists:employees,id',
            'shift_ids' => 'required|array|min:1',
            'shift_ids.*' => 'required|integer|exists:shifts,id',
            'rating' => 'required|numeric|min:0|max:5'
        ]);

        DB::beginTransaction();
        $inserted = 0;
        foreach($request->shift_ids as $shiftId) {
            $response = $this->store($this->createRequest($request, [
                'employee_id' => $request->employee_id,
                'shift_id' => $shiftId,
                'rating' => $request->rating
            ]));

            if ($response->status() != 200 || !isset($response->getData()->success)) {
                DB::rollBack();
                return $response;
            }
            $inserted++;
        }
        DB::commit();
        return new JsonResponse([
            'success' => true,
            'message' => 'Successfully created ' . $inserted . ' shift ratings.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($ids)
    {
        return $ids;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
