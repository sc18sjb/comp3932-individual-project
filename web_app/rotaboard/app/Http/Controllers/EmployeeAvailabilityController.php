<?php

namespace App\Http\Controllers;

use App\Models\EmployeeAvailability;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class EmployeeAvailabilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function store(Request $request)
    {
        // Validate the request fields.
        $request->validate([
            'employee_id' => 'required|integer|exists:employees,id',
            'availability_ids' => 'required|array|min:1',
            'availability_ids.*' => 'required|integer|exists:availabilities,id'
        ]);

        $data = array();
        $employeeAvailability = new EmployeeAvailability();

        foreach ($request->availability_ids as $preference => $availability) {
            array_push($data, [
                'employee_id' => $request->employee_id,
                'availability_id' => $availability,
                'preference_order' => $preference + 1, // Account for i = 0,
                'created_at' => $employeeAvailability->freshTimestamp(),
                'updated_at' => $employeeAvailability->freshTimestamp()
            ]);
        }
        
        try {
            $employeeAvailability::insert($data);

            return new JsonResponse([
                'success' => true,
                'message' => 'Employee availability preference created successfully.'
            ]);
        } catch (Exception $e) {
            return new JsonResponse([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'insertError' => [$e->getMessage()]
                ]
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function destroy(Request $request)
    {
        $request->validate([
            'id' => 'required|integer|exists:employees,id'
        ]);

        EmployeeAvailability::where('employee_id', $request->id)->delete();

        return new JsonResponse([
            'success' => true,
            'message' => 'Employee availability deleted successfully.'
        ]);
    }
}
