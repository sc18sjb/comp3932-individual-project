<?php

namespace App\Http\Controllers;

use App\Models\EmployeeDepartment;
use App\Models\Department;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class EmployeeDepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function store(Request $request)
    {
        // Validate the requests fields.
        $request->validate([
            'employee_id' => 'required|integer|exists:employees,id',
            'department_ids' => 'required|array|min:1',
            'department_ids.*' => 'required|integer|exists:departments,id'
        ]);


        $departmentCheck = Department::whereIn('id', $request->department_ids)->where('company_id', $request->user()->company_id)->get();
        if ($departmentCheck->isEmpty())
            return new JsonResponse([
                'message' => 'Unauthorized.'
            ], 401);

        $data = array();
        $employeeDepartment = new EmployeeDepartment();
    
        foreach ($request->department_ids as $department) {
            array_push($data, [
                'employee_id' => $request->employee_id,
                'department_id' => $department,
                'created_at' => $employeeDepartment->freshTimestamp(),
                'updated_at' => $employeeDepartment->freshTimestamp()
            ]);
        }

        try {
            $employeeDepartment::insert($data);

            return new JsonResponse([
                'success' => true,
                'message' => 'Employee department created successfully.'
            ]);
        } catch (Exception $e) {
            return new JsonResponse([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'insertError' => [$e->getMessage()]
                ]
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function destroy(Request $request)
    {
        $request->validate([
            'id' => 'required|integer|exists:employees,id'
        ]);

        EmployeeDepartment::where('employee_id', $request->id)->delete();

        return new JsonResponse([
            'success' => true,
            'message' => 'Employee departments deleted successfully.'
        ]);
    }
}
