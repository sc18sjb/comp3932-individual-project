<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Company;
use Dotenv\Exception\ValidationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Exception;


class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'company' => 'required',
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|email|unique:employees',
            'password' => 'required'
        ]);

        try {
            DB::beginTransaction();

            // First we create the new company.
            $company = new Company();
            $company->name = $request->company;
            $company->save();
            $companyId = $company->id;

            // Then we create the new employee (admin) for that company.
            $employee = new Employee();
            $employee->fname = $request->fname;
            $employee->lname = $request->lname;
            $employee->email = $request->email;
            $employee->password = Hash::make($request->password);
            $employee->role_id = 1; // Default make whoever creates the company an admin.
            $employee->company_id = $companyId;
            $employee->save();

            DB::commit();
        } catch(Exception $e) {
            DB::rollBack();
            return Redirect::back()->withInput()->withErrors('There was an error creating your account.');
        }

        // Log the user in after registration.
        if (Auth::guard()->attempt($request->only('email', 'password'))) {
            $request->session()->regenerate();

            DayController::createDefaultDays($request);

            // We can now load the app!
            return redirect('app'); 
        }

        return Redirect::back()->withInput()->withErrors('The provided credentials are incorrect.');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (Auth::guard()->attempt($request->only('email', 'password'))) {
            $request->session()->regenerate();

            // We can now load the app!
            return redirect('app'); 
        }

        return Redirect::back()->withInput()->withErrors('The provided credentials are incorrect.');
    }

    public function logout(Request $request)
    {
        Auth::guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();
    }
}
