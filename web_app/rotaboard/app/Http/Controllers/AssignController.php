<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class AssignController extends Controller
{
    public function assign(Request $request)
    {
        $request->validate([
            'start' => 'required|date_format:Y-m-d',
            'end' => 'required|date_format:Y-m-d',
            'reassign' => 'bool'
        ]);

        // We reassign by removing all the existing assignments first.
        if ($request->get('reassign') != null) 
            EmployeeShiftController::reassign($request);
        
        $data = array();

        // Get and validate the employees
        $employees = EmployeeController::index($request)->original['data'];
        // TODO: this should be done automatically by the query to save processing time.
        foreach ($employees as $employee) {
            if ($employee->departments == null)
                $employee->departments = "";
            if ($employee->availability_preference == null)
                $employee->availability_preference = "";
        }
        $data['employees'] = $employees;
        $data['availability'] = AvailabilityController::index()->original['data'];
        $data['departments'] = DepartmentController::index($request)->original['data'];

        // Get and validate the shifts and shift ratings
        $shifts = ShiftController::index($this->createRequest($request, [
            'start' => $request->start,
            'end' => $request->end
        ]))->original['data'];

        // If we don't have any shifts to assign, we don't need to assign anything
        if (count($shifts) <= 0) {
            return new JsonResponse([
                'message' => 'Cannot assign shifts when 0 shifts present.',
                'errors' => [
                    'insertError' => ['Cannot assign shifts when 0 shifts present.']
                ]
            ], 422);
        }

        $ids = array();
        foreach($shifts as $shift) {
            array_push($ids, $shift->id);
            if ($shift->employee_ids == null)
                $shift->employee_ids = ""; // Set it to an empty string
        }
        
        $shiftRatings = ShiftRatingController::index($this->createRequest($request, [
            'ids' => $ids
        ]))->original['data'];

        $data['shifts'] = $shifts;
        $data['shift_ratings'] = $shiftRatings;

        // Use CF to predict user ratings for shifts they have not yet rated.
        $response = Http::post(config('app.matching_url') . '/calculate/ratings', [
                'shift_ratings' => $shiftRatings
            ]);
        if ($response->status() != 200 || !$response['success'])
            return $response;
        
        $ratings = $response['data'];
        // Add the new ratings if there is any.
        if (count($ratings) > 0) {
            $shiftRatingController = new ShiftRatingController();
            DB::beginTransaction();
            foreach ($ratings as $rating) {
                $ratingResponse = $shiftRatingController->store($this->createRequest($request, $rating), true);
                if ($ratingResponse->status() != 200 || !isset($ratingResponse->getData()->success)) {
                    DB::rollBack();
                    return $ratingResponse;
                }
            }
            DB::commit();
        }

        // Once we have predicted the ratings, we can then assign the shifts based on these ratings and preferences.
        $response = Http::post(config('app.matching_url') . '/assign', $data);

        if ($response->status() != 200 || !$response['success'])
            return $response;

        $responseData = $response['data'];

        if (count($responseData['employee_ids']) <= 0 && count($responseData['shift_date_ids']) <= 0)
            return new JsonResponse([
                'success' => True,
                'message' => 'Successfully created 0 employee shifts',
                'data' => 0
            ]);

        return EmployeeShiftController::store($this->createRequest($request, $responseData));
    }

}
