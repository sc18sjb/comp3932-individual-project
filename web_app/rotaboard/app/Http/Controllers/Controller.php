<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function createRequest($request, $data = array())
    {
        $user = $request->user();
        $newRequest = new Request();
        $newRequest->merge(['user' => $user]);
        $newRequest->setUserResolver(function () use ($user) {
            return $user;
        });
        $newRequest->replace($data);
        return $newRequest;
    }
}
