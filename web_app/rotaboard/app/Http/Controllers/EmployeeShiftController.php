<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmployeeShift;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class EmployeeShiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new JsonResponse([
            'success' => true,
            'data' => EmployeeShift::get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function store(Request $request)
    {
        // Validate the request fields.
        $request->validate([
            'employee_ids' => 'required|array|min:1|array_length_equal:shift_date_ids',
            'employee_ids.*' => 'required|integer|exists:employees,id',
            'shift_date_ids' => 'required|array|min:1|array_length_equal:employee_ids',
            'shift_date_ids.*' => 'required|integer|exists:shift_dates,id'
        ]);

        $data = array();
        $employeeShift = new EmployeeShift();

        // Since employeeIds and shiftDateIds are the same length, it doesn't matter what we use as n.
        for ($x = 0; $x < count($request->employee_ids); $x++) {
            array_push($data, [
                'employee_id' => $request->employee_ids[$x],
                'shift_date_id' => $request->shift_date_ids[$x],
                'created_at' => $employeeShift->freshTimestamp(),
                'updated_at' => $employeeShift->freshTimestamp()
            ]);
        }

        try {
            $inserted = DB::table('employee_shifts')->insertOrIgnore($data);

            return new JsonResponse([
                'success' => true,
                'message' => 'Successfully created ' . $inserted . ' employee shifts.',
                'data' => $inserted
            ]);
        } catch (Exception $e) {
            return new JsonResponse([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'insertError' => [$e->getMessage()]
                ]
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $shift = DB::table('employee_shifts')
            ->selectRaw('employee_shifts.*, employees.fname, employees.lname, employees.email')
            ->leftJoin('employees', 'employees.id', '=', 'employee_shifts.employee_id')
            ->where('shift_date_id', '=', $id)
            ->get();

        return new JsonResponse([
            'success' => true,
            'data' => $shift
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function destroyMany(Request $request) {
        $request->merge(['start' => $request->route('start'), 'end' => $request->route('end')]);
        return EmployeeShiftController::reassign($request);
    }

    public static function reassign(Request $request)
    {
        $request->validate([
            'start' => 'required|date_format:Y-m-d',
            'end' => 'required|date_format:Y-m-d'
        ]);

        $deleted = DB::table('employee_shifts')
            ->leftJoin('shift_dates', 'shift_dates.id', '=', 'employee_shifts.shift_date_id')
            ->whereBetween('date', [$request->start, $request->end])
            ->delete();

        return new JsonResponse([
            'success' => True,
            'message' => 'Successfully removed ' . $deleted . ' employee shifts.',
            'data' => $deleted
        ]);
    }
}
