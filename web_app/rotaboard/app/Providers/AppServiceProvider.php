<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use App\Validators\CustomValidators;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // Validator::extend('less_than_field', [CustomValidators::class, 'validateLessThanField']);
        // Validator::extend('greater_than_field', [CustomValidators::class, 'validateGreaterThanField']);   
        // Validator::extend('array_length_equal', [CustomValidators::class, 'validateArrayLengthEqual']);
        // Validator::extend('time_length_field', [CustomValidators::class, 'validateRangeLength']);

        Validator::extend('greater_than_field', function($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();
            return $value >= $data[$parameters[0]];
        });

        Validator::extend('less_than_field', function($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();
            return $value <= $data[$parameters[0]]; 
        });

        Validator::extend('array_length_equal', function($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();
            return count($value) == count($data[$parameters[0]]); // Check the arrays are the same length
        });

        Validator::extend('time_length_field', function($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();
            $start = strtotime($data[$parameters[0]]);
            $end = strtotime($value);
    
            return ($start > $end || (($end - $start) / 60) > 0);
        });
        
        // We use the replacer method to replace :field in the validation message
        Validator::replacer('greater_than_field', function($message, $attribute, $rule, $parameters) {
            return str_replace(':field', $parameters[0], $message);
        });

        Validator::replacer('less_than_field', function($message, $attribute, $rule, $parameters) {
            return str_replace(':field', $parameters[0], $message);
        });

        Validator::replacer('array_length_equal', function($message, $attribute, $rule, $parameters) {
            return str_replace(':field', $parameters[0], $message);
        });

        Validator::replacer('time_length_field', function($message, $attribute, $rule, $parameters) {
            return str_replace(':field', $parameters[0], $message);
        });

        if($this->app->environment('production')) {
            \URL::forceScheme('https');
        }
    }
}
