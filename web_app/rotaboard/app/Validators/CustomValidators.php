<?php

namespace App\Validators;

use Illuminate\Support\Facades\Validator;

class CustomValidators extends Validator
{
    public function validateGreaterThanField($attribute, $value, $parameters, $validator)
    {
        $data = $validator->getData();
        return $value >= $data[$parameters[0]];
    }

    public function validateLessThanField($attribute, $value, $parameters, $validator) {
        $data = $validator->getData();
        return $value <= $data[$parameters[0]]; 
    }

    public function validateArrayLengthEqual($attribute, $value, $parameters, $validator) {
        $data = $validator->getData();
        return count($value) == count($data[$parameters[0]]); // Check the arrays are the same length
    }

    public function validateRangeLength($attribute, $value, $parameters, $validator) {
        $data = $validator->getData();
        $start = strtotime($data[$parameters[0]]);
        $end = strtotime($value);

        return ($start > $end || (($end - $start) / 60) > 0);
    }
}