<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Plugin\GetWithMetadata;

class CreateDatabaseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:database {dbname?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {

            $dbname = $this->hasArgument('dbname') && $this->argument('dbname') ? $this->argument('dbname') : env('DB_DATABASE', 'rotaboard');

            $dbnameExists = DB::select('SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = :db', array(':db' => $dbname));

            if (empty($dbnameExists)) {
                DB::statement('CREATE DATABASE ' . $dbname);
                $this->info("Database '$dbname' created successfully!");
            } else {
                $this->info("Database '$dbname' already exists");
            }


        } catch(\Exception $e) {
            $this->error($e->getMessage());
        }

        // return Command::SUCCESS;
    }
}
