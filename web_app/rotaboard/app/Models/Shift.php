<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    use HasFactory;

    protected $fillable = [
        'department_id',
        'day_id',
        'start',
        'end',
        'min',
        'max'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
