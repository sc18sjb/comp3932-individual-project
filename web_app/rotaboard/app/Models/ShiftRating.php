<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShiftRating extends Model
{
    use HasFactory;

    protected $fillable = [
        'employee_id',
        'shift_id',
        'rating'
    ];

    protected $hidden = [
        'updated_at',
        'created_at'
    ];
}
