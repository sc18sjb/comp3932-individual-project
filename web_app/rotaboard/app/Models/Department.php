<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_id',
        'name',
        'color'
    ];

    protected $hidden = [
        'company_id',
        'created_at',
        'updated_at'
    ];
}
