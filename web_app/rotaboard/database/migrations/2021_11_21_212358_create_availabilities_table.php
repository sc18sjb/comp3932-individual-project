<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use App\Models\Availability;

class CreateAvailabilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('availabilities', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable(false)->unique();
            $table->time('start')->nullable(false);
            $table->time('end')->nullable(false);
            $table->timestamps();
        });

        // Populate with default data for now (allow the user to specify this eventually though)
        Availability::create(array('name' => 'morning', 'start' => '06:00:00', 'end' => '12:00:00'));
        Availability::create(array('name' => 'afternoon', 'start' => '12:00:00', 'end' => '17:00:00'));
        Availability::create(array('name' => 'evening', 'start' => '17:00:00', 'end' => '20:00:00'));
        Availability::create(array('name' => 'night', 'start' => '20:00:00', 'end' => '06:00:00'));
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('availabilities');
    }
}
