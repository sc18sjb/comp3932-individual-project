<?php

use App\Models\ShiftDate;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class CreateShiftDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shift_dates', function (Blueprint $table) {
            $nowDate = Carbon::now()->format('Y-m-d');

            $table->id();
            $table->foreignId('shift_id')
                ->constrained('shifts')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->date('date')->nullable(false)->default($nowDate);
            $table->timestamps();

            $table->unique(['shift_id', 'date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shift_dates');
    }
}
