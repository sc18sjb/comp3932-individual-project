<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\ShiftRating;

class CreateShiftRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shift_ratings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('employee_id')
                ->constrained('employees')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('shift_id')
                ->constrained('shifts')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            // We want the default to be 1 not 0 since if a shift doesn't exist in the shift ratings then that counts as 0
            $table->double('rating')->nullable(false)->default(1);
            $table->enum('provider', ['SYSTEM', 'EMPLOYEE'])->default('EMPLOYEE');
            $table->timestamps();

            $table->unique(['employee_id', 'shift_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shift_ratings');
    }
}
