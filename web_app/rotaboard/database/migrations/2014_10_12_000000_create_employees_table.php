<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Employee;
use Illuminate\Support\Facades\Hash;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('fname')->nullable(false);
            $table->string('lname')->nullable(false);
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable(false);
            $table->foreignId('role_id')
                ->constrained('employee_roles')
                ->onUpdate('cascade')
                ->onDelete('cascade')
                ->default(2);
            $table->foreignId('company_id')
                ->constrained('companies')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            // $table->rememberToken();
            $table->timestamps();
        });

        // Default admin
        Employee::create([
            'fname' => 'sam',
            'lname' => 'barnes',
            'email' => 'sc18sjb@leeds.ac.uk',
            'password' => Hash::make('test123'),
            'role_id' => 1,
            'company_id' => 1
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
