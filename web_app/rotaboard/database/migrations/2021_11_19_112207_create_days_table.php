<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Day;

class CreateDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('days', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id')
                ->constrained('companies')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('name')->nullable(false);
            $table->integer('day')->nullable(false);
            $table->timestamps();

            $table->unique(['company_id', 'day']);
        });

        // days from 0 => 6 where 0 = sunday and 6 = saturday (same convention as PHP and JS)
        Day::create(array('company_id' => 1, 'name' => 'monday', 'day' => 0));
        Day::create(array('company_id' => 1, 'name' => 'tuesday', 'day' => 1));
        Day::create(array('company_id' => 1, 'name' => 'wednesday', 'day' => 2));
        Day::create(array('company_id' => 1, 'name' => 'thursday', 'day' => 3));
        Day::create(array('company_id' => 1, 'name' => 'friday', 'day' => 4));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('days');
    }
}
