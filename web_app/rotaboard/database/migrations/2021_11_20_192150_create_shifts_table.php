<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Shift;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class CreateShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shifts', function (Blueprint $table) {
            $nowTime = Carbon::now()->format('H:i:s');

            $table->id();
            $table->foreignId('department_id')
                ->constrained('departments')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('day_id') # Define which day the shift is on (since we might have the same shift on different days that need different ratings).
                ->constrained('days')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->time('start')->nullable(false)->default($nowTime);
            $table->time('end')->nullable(false)->default($nowTime); // Do I need this anymore if I have the length of a shift?
            $table->integer('min')->nullable(false)->default(1); // Min No. employees needed to work the shift.
            $table->integer('max')->nullable(false)->default(1); // Max No. employees needed to work the shift.
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shifts');
    }
}
