# Rotaboard web app

### Running instructions

copy .env.example to a new file called .env

`composer install`

`php artisan key:generate`

`npm install`

`php artisan make:database`

`php artisan migrate`

`php artisan serve`

`access at localhost:8000`

On udpate:
`npm install`

run with a local server (such as WAMP)

- Change the following settings in .env

```
APP_URL=http://rotaboard.test
SESSION_DOMAIN=rotaboard.test
SANCTUM_STATEFUL_DOMAINS=rotaboard.test
```

- Add the following to bin\apache\\{apache_version}\conf\extra\httpd-vhosts.conf

```
<VirtualHost *:80>
	ServerName rotaboard.test
	DocumentRoot C:\Users\barne\Documents\gitlab\comp3931\web_app\rotaboard\public
	<Directory "C:\Users\barne\Documents\gitlab\comp3931\web_app\rotaboard\public">
		Options Indexes FollowSymLinks MultiViews
		AllowOverride all
		Order Deny,Allow
		Allow from all
		Require all granted
	</Directory>
</VirtualHost>
```

- then make sure to register the hosts in the hosts file under C:\Windows\System32\drivers\etc\hosts

`127.0.0.1 rotaboard.test`