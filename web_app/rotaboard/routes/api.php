<?php

use App\Http\Controllers\ApiAuthController;
use App\Http\Controllers\AssignController;
use App\Http\Controllers\AvailabilityController;
use App\Http\Controllers\DayController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\EmployeeAvailabilityController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\EmployeeDepartmentController;
use App\Http\Controllers\EmployeeShiftController;
use App\Http\Controllers\ShiftController;
use App\Http\Controllers\ShiftDateController;
use App\Http\Controllers\ShiftRatingController;
use App\Http\Controllers\ShiftRotaController;
use App\Models\Employee;
use App\Models\EmployeeShift;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Public routes.
Route::post('login', [ApiAuthController::class, 'login'])->name('login');
Route::post('register', [ApiAuthController::class, 'register'])->name('register');

// Protected routes.
Route::group(['middleware' => ['auth:sanctum']], function() {
    Route::post('logout', [ApiAuthController::class, 'logout'])->name('logout');

    // Get the current employee information.
    Route::get('employee', [ApiAuthController::class, 'getCurrentEmployee'])->name('getEmployee');
    
    // Get all days the company is operating
    Route::get('days', [DayController::class, 'index'])->name('getDays');

    // Admin routes.
    Route::middleware('rolecheck:admin')->group(function() {
        // Assign shifts to employees.
        Route::post('assign', [AssignController::class, 'assign'])->name('assign');

        // Create an employee.
        Route::post('employee/create', [EmployeeController::class, 'store'])->name('createEmployee');
        Route::put('employee/update/{id}', [EmployeeController::class, 'update'])->name('updateEmployee');
        Route::delete('employee/delete/{id}', [EmployeeController::class, 'destroy'])->name('deleteEmployee');

        // Create an an employee availability preference.
        Route::post('employee/availability/create', function(Request $request) {
            return EmployeeAvailabilityController::store($request);
        })->name('CreateEmployeeAvailability');

        // Create an employee department.
        Route::post('employee/department/create', function(Request $request) {
            return EmployeeDepartmentController::store($request);
        })->name('createEmployeeDepartment');

        // Create an employee shift.
        Route::post('employee/shift/create', function(Request $request) {
            return EmployeeShiftController::store($request);
        })->name('createEmployeeShift');

        Route::delete('employee/shifts/delete/{start}/{end}', [EmployeeShiftController::class, 'destroyMany'])->name('deleteEmployeeShifts');

        // Create a department.
        Route::post('department/create', [DepartmentController::class, 'store'])->name('createDepartment');
        Route::put('department/update/{id}', [DepartmentController::class, 'update'])->name('updateDepartment');
        Route::delete('department/delete/{id}', [DepartmentController::class, 'destroy'])->name('deleteDepartment');

        // Create a shift.
        Route::post('shift/create', [ShiftController::class, 'store'])->name('createShift');
        Route::put('shift/update/{id}', [ShiftController::class, 'update'])->name('updateShift');

        // Assign a shift to a date.
        Route::post('shift/date/create', function(Request $request) {
            return ShiftDateController::store($request);
        })->name('createShiftDate');
        Route::delete('shift/date/delete/{id}', [ShiftDateController::class, 'destroy'])->name('deleteShiftDates');
        Route::delete('shift/date/delete/{start}/{end}', [ShiftDateController::class, 'destroyMany'])->name('deleteManyShiftDates');

        // Duplicate a shift rota.
        Route::post('shift/rota/duplicate', [ShiftRotaController::class, 'duplicate'])->name('duplicateShiftDate');

        // Get or create a shift rota.
        Route::get('shift/rotas', [ShiftRotaController::class, 'index'])->name('getShiftRotas');
        Route::post('shift/rota/create', [ShiftRotaController::class, 'store'])->name('createShiftRota');
        Route::delete('shift/rota/delete/{id}', [ShiftRotaController::class, 'destroy'])->name('deleteShiftRota');

        // Create an availability.
        Route::post('availability/create', [AvailabilityController::class, 'store'])->name('createAvailability');

        // Create an operating day.
        Route::post('days/create', [DayController::class, 'store'])->name('createDay');
    });

    // Admin and employee routes
    Route::middleware('rolecheck:admin|employee')->group(function() {
        // Get all employees.
        Route::get('employees', function(Request $request) {
            return EmployeeController::index($request);
        })->name('getEmployees');

        // Get all employee shifts
        Route::get('employee/shifts', [EmployeeShiftController::class, 'index'])->name('getEmployeeShifts');

        // Get an employee shift by shift_date id
        Route::get('employee/shift/{id}', [EmployeeShiftController::class, 'show'])->name('getEmployeeShiftByDateId');

        // Get all departments.
        Route::get('departments', function(Request $request) {
            return DepartmentController::index($request);
        })->name('getDepartments');

        // Get all shifts.
        Route::get('shifts', function(Request $request) {
            return ShiftController::index($request);
        })->name('getShifts');

        // Get all shift ratings
        Route::get('shift/ratings', function(Request $request) {
            return ShiftRatingController::index($request);
        })->name('getShiftRatings');

        // Rate a shift
        Route::post('shift/rating/create', [ShiftRatingController::class, 'store'])->name('createShiftRating');
        Route::post('shift/rating/create/many', [ShiftRatingController::class, 'storeMany'])->name('createManyShiftRatings');

        // Get all availability types.
        Route::get('availability', function() {
            return AvailabilityController::index();
        })->name('getAvailability');
    });
});