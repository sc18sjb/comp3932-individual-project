<?php

use App\Http\Controllers\EmployeeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Login, Register and logout POST routes.
Route::post('login', [AuthController::class, 'login'])->name('login');
Route::post('register', [AuthController::class, 'register'])->name('register');
Route::post('logout', [AuthController::class, 'logout'])->name('logout');


// Show the login form if not logged in.
Route::get('login', function(Request $request) {
    if ($request->user())
        return Redirect::to('/app');
    return view('app.login');   
});

// Show the register form if not logged in.
Route::get('register', function(Request $request) {
    if ($request->user())
        return Redirect::to('/app');
    return view('app.register');
}); 

// Regirect to login after logout.
Route::get('logout', function() {
    return Redirect::to('login');
});

Route::get('/', function() {
    return Redirect::to('login');
});

// The app is a SPA using react client-side routing.
Route::group(['middleware' => ['auth']], function() {
    Route::get('/app/{path?}', function() {
        return view('app.dashboard');
    })->where('path', '.*');
});


// Everything else is done using laravel server-side routing.