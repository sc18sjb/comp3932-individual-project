import math
from unittest import result
import numpy as np
from numpy import linalg as la

# Base helper functions
def cosineSim(x, y):
    xy = np.sum(np.multiply(x, y))
    normx = la.norm(x)
    normy = la.norm(y)
    return xy / (normx * normy) if (normx * normy) > 0 else 0


def weightedSum(ratings, predictions):
    xy = np.sum(np.multiply(ratings, predictions))
    return xy / np.sum(predictions) if np.sum(predictions) > 0 else 0


def getK(k, results):
    if k > 0:
        return k
    n = results.size - 1
    return int(math.sqrt(n))    


class CollaborativeFiltering:
    method: int = 'ITEM' # Based from results we want to use UserUser CF
    adjustment: bool = True # Based from results we want to use adjusted CF

    def __init__(self, shiftRatings) -> None:
        self.shiftRatings = shiftRatings

        # Set the initial matrix of user-shift ratings.
        self.createMatrix()

        # Remove any sparce columns.
        self.removeSparceColumns()

    def createMatrix(self):
        if len(self.shiftRatings) <= 0:
            return np.empty((0, 0), int)
        
        rows = {}
        employees = []
        shifts = []
        for shiftRating in self.shiftRatings:
            employees.append(shiftRating.employee_id)
            shifts.append(shiftRating.shift_id)
            rows.setdefault(shiftRating.shift_id, []).append(shiftRating.rating)

        # Count the number of unique employees and shifts in the matrix
        employees = set(employees)
        shifts = set(shifts)

        matrix = np.empty((0, len(employees)), int)
        for shift, row in rows.items():
            matrix = np.append(matrix, np.array([row]), axis=0)

        self.matrix = matrix
        self.employees = list(employees)
        self.shifts = list(shifts)
    
    def removeSparceColumns(self):
        '''Remove any columns that are all 0's since these make no difference to the calculation anyway'''
        cols = np.where(~self.matrix.any(axis=0))[0]
        self.matrix = np.delete(self.matrix, cols, axis=1)
        # We want to remove the employee id as well
        for col in sorted(cols, reverse=True):
            del self.employees[col]

    def CFzeros(self):
        if self.matrix.size == 0:
            return []
        # TODO: only calculate the ratings of an employee who can actually work the given shift
        zeros = np.argwhere(self.matrix == 0)
        ratings = []
        for xy in zeros:
            shift = xy[0]
            employee = xy[1]

            # Decide which method to use: USER as default.
            if self.method == 'USER':
                rating = CollaborativeFiltering.userUserCF(self.matrix, shift, employee, self.adjustment)
            else:
                rating = CollaborativeFiltering.itemItemCF(self.matrix, shift, employee, self.adjustment)

            # No need to add 0 ratings.
            if rating <= 0:
                continue

            ratings.append({
                'employee_id': self.employees[employee],
                'shift_id': self.shifts[shift],
                'rating': round(rating, 2)
            })

        return ratings

    @staticmethod
    def userUserCF(matrix: np.array, shiftIndex: int, employeeIndex: int, adjustment: bool, k:int=0) -> int:
        if matrix[shiftIndex][employeeIndex] > 0: return matrix[shiftIndex][employeeIndex]
        
        # We don't want to include the row we are trying to calculate
        cfMatrix = np.delete(matrix, shiftIndex, 0)

        y = cfMatrix[:,employeeIndex]

        # We don't want to compare y with y so we remove it from the array
        cfSimMatrix = np.delete(cfMatrix, employeeIndex, 1)
        
        results = np.empty([0, 0], int)
        for row in cfSimMatrix.T:
            mean_col = ((sum(sum(row, y)) / y.size) if y.size > 0 else 0) if adjustment else 0
            results = np.append(results, cosineSim(row - mean_col, y - mean_col))
        
        # We re-insert 0 on the y column to get the index of the other columns
        results = np.insert(results, employeeIndex, 0.0)

        # If k is undefined we take k as the squareroot of n or n^1/2
        k = getK(k, results)

        # Sort the results in DESC order and take the first k items
        indexes = np.argsort(results)[::-1][:k]
        results = np.sort(results)[::-1][:k]

        # Get the ratings from each index of the k nearest neightbors
        iratings = np.take(matrix[shiftIndex], indexes)

        # Return the weighted sum
        return weightedSum(iratings, results)

    @staticmethod
    def itemItemCF(matrix: np.array, shiftIndex: int, employeeIndex: int, adjustment: bool, k:int=0) -> int:
        if matrix[shiftIndex][employeeIndex] > 0: return matrix[shiftIndex][employeeIndex]
        
        # We don't want to include the column we are trying to calculate
        cfMatrix = np.delete(matrix, employeeIndex, 1)
        
        x = cfMatrix[shiftIndex]

        # We don't want to compare x with x so we remove it from the array
        cfSimMatrix = np.delete(cfMatrix, shiftIndex, 0)

        results = np.empty([0, 0], int)
        for col in cfSimMatrix:
            mean_col = ((sum(sum(col, x)) / x.size) if x.size > 0 else 0) if adjustment else 0
            results = np.append(results, cosineSim(col - mean_col, x - mean_col))

        # We re-insert 0 on the x column to get the index of the other columns
        results = np.insert(results, shiftIndex, 0.0)

        # If k is undefined we take k as the squareroot of n or n^1/2
        k = getK(k, results)
        
        # Sort the results in DESC order and take the first k items
        indexes = np.argsort(results)[::-1][:k]
        results = np.sort(results)[::-1][:k]

        # # Get the ratings from each index of the k nearest neightbors
        iratings = np.take(matrix[:,employeeIndex], indexes)
        
        return weightedSum(iratings, results)


if __name__ == "__main__":
    a = np.array([2.0,1.0])  
    b = np.array([5.0,3.0])
    c = np.array([5.0,4.0])

    mean_ab = sum(sum(a, b)) / 4
    mean_cb = sum(sum(c, b)) / 4

    print(cosineSim(a - 0, b - 0))