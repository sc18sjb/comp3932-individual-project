import requests
from datetime import datetime
from typing import List, Tuple, Dict
from models import Employee, Shift, Department, Availability, DFM, TFM
from general_parser import Parser
from werkzeug.exceptions import Unauthorized

class DBParser(Parser):
    def __init__(self, url: str='', token: str='') -> None:
        self.baseUrl = self.__checkUrl(url) + '/api'
        self.session = requests.Session()
        self.session.headers.update({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token
        })

    def __checkResponseStatus(self, response):
        if response.status_code == 401:
            raise Unauthorized(response.json()['message'])
        elif response.status_code != 200:
            raise Exception(response.json())

    def __checkUrl(self, url):
        if url is None or url.strip() == '':
            raise AttributeError('Url of service is undefined')

        if 'http://' not in url:
            return 'http://' + url
        return url

    def parseDepartments(self) -> List[Department]:
        """Get all the departments from the database and create department objects for each row."""
        response = self.session.get(f'{self.baseUrl}/departments')

        self.__checkResponseStatus(response)

        departments = {}
        for department in response.json()['data']:
            d = Department(department['id'], department['name'])
            departments[department['id']] = d
        
        return departments

    def parseAvailability(self) -> List[Availability]:
        """Get all the different availability types from the database and create availability objects for each row."""
        response = self.session.get(f'{self.baseUrl}/availability')

        self.__checkResponseStatus(response)

        availabilities = {}
        for availability in response.json()['data']:
            start = datetime.strptime(str(availability['start']), TFM).time()
            end = datetime.strptime(str(availability['end']), TFM).time()

            a = Availability(availability['name'], start, end)
            availabilities[availability['id']] = a
        
        return availabilities

    def parseEmployees(self, departments: List[Department]) -> List[Employee]:
        """Get all the employees in the company and their respective departments and availability preferences.
            Create employee objects for each row."""
        availabilities = self.parseAvailability()
        response = self.session.get(f'{self.baseUrl}/employees')

        self.__checkResponseStatus(response)

        employees = []
        for employee in response.json()['data']:
            d = [departments[int(department)] for department in employee['departments'].split(',')]
            a = [availabilities[int(availability)] for availability in employee['availability_preference'].split(',')]
            e = Employee(employee['id'], employee['fname'] + ' ' + employee['lname'], d, a)
            employees.append(e)
        
        return employees

    def parseShiftRatings(self, shifts: List[int]) -> List[int]:
        """Get a list of employee ratings for a list of given shifts (id's)"""
        ratings = {}
        if len(shifts) < 1:
            return ratings

        response = self.session.get(f"{self.baseUrl}/shift/ratings", params={'ids[]': shifts})

        self.__checkResponseStatus(response)

        for shift in response.json()['data']:
            if shift['shift_id'] not in ratings:
                ratings[shift['shift_id']] = {
                    shift['employee_id']: shift['rating']
                }
            else:
                ratings[shift['shift_id']][shift['employee_id']] = shift['rating']
        
        return ratings

    def parseShifts(self, departments: List[Department], employees: List[Employee], day: datetime=datetime.now()) -> List[Shift]:
        """Get all the shifts from the database and create shift objects for each row."""
        weekStart, weekEnd = self.getWeekRange(day)
        response = self.session.get(f"{self.baseUrl}/shifts?start={weekStart.strftime(DFM)}&end={weekEnd.strftime(DFM)}")

        self.__checkResponseStatus(response)
        
        shifts = {}
        ratings = self.parseShiftRatings([s['id'] for s in response.json()['data']])

        for shift in response.json()['data']:
            start = datetime.strptime(str(shift['start']), TFM).time()
            end = datetime.strptime(str(shift['end']), TFM).time()
            date = datetime.strptime(str(shift['date']), DFM)
            assigned = []

            if shift['assigned'] > 0:
                assigned = [e for e in employees if e.eid in map(int, shift['employee_ids'].split(','))]

            # We use the date id since this is what we attribute to each employee
            s = Shift(shift['date_id'], start, end, date, departments[shift['department_id']], shift['min'], shift['max'])            
            s.ratings = ratings[shift['id']]  
            s.employees = assigned

            for employee in s.employees:
                employee.shifts.append(s)

            shifts.setdefault(date, []).append(s)

        return shifts

    def createEmployeeShifts(self, matches: Dict[datetime, List[Tuple[int, int]]]) -> None:        
        employeeIds = []
        shiftDateIds = []

        # Flatten the array
        for k, day in matches.items():
            for match in day:
                employeeIds.append(match[0])
                shiftDateIds.append(match[1])
                
        if len(employeeIds) <= 0:
            return 0

        response = self.session.post(f"{self.baseUrl}/employee/shift/create", json={
            'employee_ids': employeeIds,
            'shift_date_ids': shiftDateIds
        })
        self.__checkResponseStatus(response)

        print(f"{len(employeeIds)} was inserted.")
        return len(employeeIds)


    # Override
    def parse(self, date: datetime=datetime.now()):
        departments = self.parseDepartments()

        employees = self.parseEmployees(departments)

        shifts = self.parseShifts(departments, employees, date)

        return departments, employees, shifts

        