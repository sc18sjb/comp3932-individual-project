## Schedular API

This API contains the 'brunt-work' of the schedular algorithm, including similarity detection and creating the stable matching adhering to the
constraint satisfaction problem. The API provides a simple interface to allow employees to be scheduled to a weekly rota; provided for the web
application, CMD or mobile application.

### Install
`py -m venv env`

`pip install -r requirements.txt`

`env\Scripts\activate`

`uvicorn working:app --reload`