from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from gale_shapely import GaleShapely
from request_parser import RequestParser
from collaborative_filtering import CollaborativeFiltering
from models import DFM
from typing import List

app = FastAPI()

class Shift(BaseModel):
    id: int
    day_id: int
    date_id: int
    department_id: int
    assigned: int
    date: str
    min: int
    max: int
    start: str
    end: str
    employee_ids: str
    department_color: str
    department_name: str
    

class ShiftRating(BaseModel):
    employee_id: int
    shift_id: int
    rating: float


class Employee(BaseModel):
    id: int
    fname: str
    lname: str
    email: str
    departments: str
    availability_preference: str


class Department(BaseModel):
    id: int
    color: str
    name: str


class Availability(BaseModel):
    id: int
    start: str
    end: str
    name: str
    

class AssignRequest(BaseModel):
    availability: List[Availability]
    departments: List[Department]
    employees: List[Employee]
    shifts: List[Shift]
    shift_ratings: List[ShiftRating]


class CalculateRatingRequest(BaseModel):
    shift_ratings: List[ShiftRating]


class ApiException(Exception):
    def __init__(self, status: int, message: str):
        self.status = status
        self.message = message


@app.exception_handler(ApiException)
async def apiExceptionHandler(request: Request, exc: ApiException):
    return JSONResponse(
        status_code = exc.status,
        content = {"message": exc.message}
    )


@app.post('/calculate/ratings')
def calculateRatings(request: CalculateRatingRequest):
    print(request.shift_ratings)
    cf = CollaborativeFiltering(request.shift_ratings)
    ratings = cf.CFzeros()

    return {
        'success': True,
        'data': ratings
    }


@app.post('/assign')
def assign(request: AssignRequest):
    parser = RequestParser(request)
    departments, employees, shifts = parser.parse()

    shapely = GaleShapely(shifts, employees)
    pairs = shapely.assignWeek()
    employeeIds = []
    shiftDateIds = []

    # Flatten the array
    for k, day in pairs.items():
        for match in day:
            employeeIds.append(match[0])
            shiftDateIds.append(match[1])

    return {
        'success': True,
        'data': {
            'employee_ids': employeeIds,
            'shift_date_ids': shiftDateIds
        }
    }