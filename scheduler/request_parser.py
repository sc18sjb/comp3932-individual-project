from datetime import datetime
from typing import List
from models import Employee, Shift, Department, Availability, DFM, TFM
from general_parser import Parser

class RequestParser(Parser):
    def __init__(self, request) -> None:
        self.departments = request.departments
        self.availability = request.availability
        self.employees = request.employees
        self.shifts = request.shifts
        self.shiftRatings = request.shift_ratings

    def parseDepartments(self) -> List[Department]:
        """Get all the departments from the database and create department objects for each row."""
        departments = {}
        for department in self.departments:
            d = Department(department.id, department.name)
            departments[department.id] = d
        
        return departments

    def parseAvailability(self) -> List[Availability]:
        """Get all the different availability types from the database and create availability objects for each row."""
        availabilities = {}
        for availability in self.availability:
            start = datetime.strptime(str(availability.start), TFM).time()
            end = datetime.strptime(str(availability.end), TFM).time()

            a = Availability(availability.name, start, end)
            availabilities[availability.id] = a
        
        return availabilities

    def parseEmployees(self, departments: List[Department]) -> List[Employee]:
        """Get all the employees in the company and their respective departments and availability preferences.
            Create employee objects for each row."""
        availabilities = self.parseAvailability()
        employees = []
        for employee in self.employees:
            d = [departments[int(department)] for department in employee.departments.split(',') if department != '']
            a = [availabilities[int(availability)] for availability in employee.availability_preference.split(',') if availability != '']
            e = Employee(employee.id, employee.fname, d, a)
            employees.append(e)
        
        return employees

    def parseShiftRatings(self, shifts: List[int]) -> List[int]:
        """Get a list of employee ratings for a list of given shifts (id's)"""
        ratings = {}
        if len(shifts) < 1:
            return ratings

        for shift in self.shiftRatings:
            if shift.shift_id not in ratings:
                ratings[shift.shift_id] = {
                    shift.employee_id: shift.rating
                }
            else:
                ratings[shift.shift_id][shift.employee_id] = shift.rating
        
        return ratings

    def parseShifts(self, departments: List[Department], employees: List[Employee]) -> List[Shift]:
        """Get all the shifts from the database and create shift objects for each row."""
        shifts = {}
        ratings = self.parseShiftRatings([s.id for s in self.shifts])

        for shift in self.shifts:
            start = datetime.strptime(str(shift.start), TFM).time()
            end = datetime.strptime(str(shift.end), TFM).time()
            date = datetime.strptime(str(shift.date), DFM)
            assigned = []

            if shift.assigned > 0:
                assigned = [e for e in employees if e.eid in map(int, shift.employee_ids.split(','))]

            # We use the date id since this is what we attribute to each employee
            s = Shift(shift.date_id, start, end, date, departments[shift.department_id], shift.min, shift.max)            
            s.ratings = ratings[shift.id]  
            s.employees = assigned

            for employee in s.employees:
                employee.shifts.append(s)

            shifts.setdefault(date, []).append(s)

        return shifts


    # Override
    def parse(self):
        departments = self.parseDepartments()

        employees = self.parseEmployees(departments)

        shifts = self.parseShifts(departments, employees)

        return departments, employees, shifts

        