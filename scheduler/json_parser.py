import json
from datetime import datetime, time
from models import Employee, Shift, Department, Availability, Day, DFM, TFM
from typing import Dict, List, Tuple, Union
from general_parser import Parser

class JSONParser(Parser):
    config: dict = {}

    def __init__(self, data: Union[str, dict]) -> None:
        self.config = data if isinstance(data, dict) else self.setConfig(data)

    def setConfig(self, filepath: str) -> dict:
        """Set the config to be the read json file."""
        if type(filepath) != str:
            raise TypeError("Filepath must be a string")

        with open(filepath) as f:
            config = json.load(f)
        return config

    def parseDepartments(self) -> Dict[str, Department]:
        """Parse the departments from the config file and return a dictionary of department, ID mappings."""
        if "departments" not in self.config:
            raise IndexError("No departments found in data")

        departments = {}
        for index, department in enumerate(self.config['departments']):
            d = Department(index + 1, department['name'])
            departments[d.name] = d

        return departments

    def parseEmployees(self, departments: Dict[str, Department]) -> List[Employee]:
        """Parse the employees from the config file and return an array of employees."""
        if "employees" not in self.config:
            raise IndexError("No employees found in data")

        # Availability info (This is usally stored in the DB).
        availabilities = {
            "morning": Availability('morning', time(6), time(12)),
            "afternoon": Availability('afternoon', time(12), time(17)),
            "evening": Availability('evening', time(17), time(20)),
            "night": Availability('night', time(20), time(6))
        }

        employees = []
        for index, employee in enumerate(self.config['employees']):
            d = [departments[department]
                 for department in employee['departments']]
            a = [availabilities[availability]
                 for availability in employee['availability']]
            e = Employee(index + 1, employee['name'], d, a)
            employees.append(e)

        return employees

    def parseShifts(self, departments=Dict[str, Department]) -> List[Shift]:
        """Parse the shifts from the config file and return an array of shifts."""
        if "shifts" not in self.config:
            raise IndexError("No shifts found in data")

        shifts: Dict[datetime, Shift] = {}
        for index, shift in enumerate(self.config['shifts']):
            start = datetime.strptime(str(shift['start']), TFM).time()
            end = datetime.strptime(str(shift['end']), TFM).time()

            # For each date of a given shift
            for date in shift['date']:
                d = datetime.strptime(str(date), DFM)
                s = Shift(index + 1, start, end, d, departments[shift['department']], shift['min'], shift['max'])
                shifts.setdefault(d, []).append(s)

        return shifts      