import copy
import itertools
from enum import Enum, auto
from typing import List, Dict, Tuple, Set, Type
from datetime import datetime, time, timedelta
from operator import itemgetter


DFM = "%Y-%m-%d" # Day format for datetime
TFM = '%H:%M:%S' # Time format


def sumRating(preferences):
    sortedPreferences = []
    for index, preference in enumerate(preferences):
        pa = (100 - ((index) * (100 / len(preferences)))) / 100 
        rating = preference[1] * pa if preference[1] > 0 else 5 / (index + 1)
        sortedPreferences.append((preference[0], rating))
    return sortedPreferences

class Day(Enum):
    MONDAY = auto()
    TUESDAY = auto()
    WEDNESDAY = auto()
    THURSDAY = auto()
    FRIDAY = auto()
    SATURDAY = auto()
    SUNDAY = auto()

    @classmethod
    def mappings(cls):
        return  {
            "Sunday": cls.SUNDAY.value,
            "Monday": cls.MONDAY.value,
            "Tuesday": cls.TUESDAY.value,
            "Wednesday": cls.WEDNESDAY.value,
            "Thursday": cls.THURSDAY.value,
            "Friday": cls.FRIDAY.value,
            "Saturday": cls.SATURDAY.value,
        }


class Availability:
    name: str
    start: time
    end: time

    def __init__(self, name: str, start: time, end: time) -> None:
        self.name = name
        self.start = start
        self.end = end

    def __repr__(self) -> str:
        """String representation of the Availability object"""
        return f"Availability(name={self.name}, start={self.start}, end={self.end})"


class Department:
    did: int
    name: str

    def __init__(self, did: int, name: str) -> None:
        self.did = did
        self.name = name

    def __repr__(self) -> str:
        """String representation of the Department object"""
        return f'Department(name={self.name})'


class Employee:
    eid: int
    name: str
    departments: List[Department]
    availability: List[Availability]
    preferences: Dict[datetime, List['Shift']] = {}
    shifts: List['Shift'] = []

    def __init__(self, eid: int, name: str, departments: List[Department], availability: List[Availability]) -> None:
        self.eid = eid
        self.name = name
        self.departments = departments
        self.availability = availability
        self.preferences = {}
        self.shifts = []

    @staticmethod
    def getPreferenceIds(employees, day: datetime) -> List[List[int]]:
        """Get the preferences in a 2D array of shift IDs"""
        return [[preference.sid for preference in employee.preferences[day]] for employee in employees]

    def getTotalWorkingHours(self, day: datetime) -> int:
        """Get the total working hours for the employee on a given day of the week."""
        return sum([s.length for s in self.shifts if s.start.strftime(DFM) == day.strftime(DFM)], timedelta())

    def isTimeBetween(self, start : time, end : time, check : time) -> bool:
        """Check if the a time is between a given range"""
        if start < end:
            return check >= start and check < end
        # For when we pass midnight
        else:
            return check >= start or check < end

    def setPreferences(self, s: List['Shift'], day: datetime) -> None:
        """Set the employee preferences to be any shift in their list of departments that is within their availability range"""
        shifts = copy.copy(s[day])
        preferences, sortedPreferences = [], []
        
        # 1) First, we want to sort the shifts by availability.
        for availabile in self.availability:
            for shift in list(shifts):
                if (self.isTimeBetween(availabile.start, availabile.end, shift.start.time())
                        and shift.department in self.departments):

                    rating = shift.ratings[self.eid] if self.eid in shift.ratings else 0
                    preferences.append((shift, rating))
                    shifts.remove(shift)

        # 2) Next we calculate a sum of the P(availability) and the rating to get the final rating.
        sortedPreferences = sumRating(preferences)

        # 3) Finally, we sort the prefernces in order of the ratings.
        sortedPreferences = sorted(sortedPreferences, key=itemgetter(1), reverse=True)
        self.preferences[day] = [s[0] for s in sortedPreferences]

    def __repr__(self) -> str:
        """String representation of the Employee object"""
        return f'Employee(name={self.name})'


class Shift:
    sid: int
    start: datetime
    end: datetime
    legth: timedelta
    department: Department
    minEmployees: int # Min No. people who work the shift.
    maxEmployees: int # Max No. people who work the shift.
    ratings: List[int] = [] # An array of ratings from different employees for the given shift

    preferences: List[Employee] = []
    poppedPreferences: List[Employee] = []
    employees: List[Employee] = []

    def __init__(self, sid: int, start: time, end: time, date: datetime, department: Department, minEmployees: int=1, maxEmployees: int=1) -> None:
        self.sid = sid
        self.department = department
        self.minEmployees = minEmployees
        self.maxEmployees = maxEmployees

        self.employees = []
        self.setRange(date, start, end)
        self.setLength()

    def setRange(self, date: datetime, start: time, end: time) -> None:
        """Calculate the start and end datetimes given a date and the start and end times of a shift."""
        self.start = datetime.combine(date, start)
        self.end = datetime.combine(date + timedelta(days=1), end) if start > end else datetime.combine(date, end)

    def setLength(self) -> None:
        """Calculate and set the length of the shift."""
        self.length = datetime.strptime(str(self.end.time()), '%H:%M:%S') - datetime.strptime(str(self.start.time()), '%H:%M:%S')

        # Account for going past midnight
        if self.length.days < 0:
            self.length = timedelta(days=0, seconds=self.length.seconds, microseconds=self.length.microseconds)

    @staticmethod
    def getPreferenceIds(shifts: List['Shift']) -> List[List[str]]:
        """Get the preferences in a 2D array of employee names"""
        return [[preference.eid for preference in shift.preferences] for shift in shifts]

    @staticmethod
    def getLengths(shifts: List['Shift']) -> List[int]:
        """Return the length of all shifts in string format"""
        return [str(s.length) for s in shifts]

    def setPreferences(self, employees: List[Employee]) -> None:
        """Set the shift prefernces to be any employee with the shift department in their list of departments"""
        preferences, sortedPreferences = [], []
       
        for employee in employees:
            if self.department in employee.departments:
                rating = self.ratings[employee.eid] if employee.eid in self.ratings else 0
                preferences.append((employee, rating))

        sortedPreferences = sumRating(preferences)
        sortedPreferences = sorted(sortedPreferences, key=itemgetter(1), reverse=True)
        self.preferences = [s[0] for s in sortedPreferences]
        self.poppedPreferences = copy.copy(self.preferences)

    def restorePreferences(self) -> None:
        """Restore the shift preferences to original"""
        self.preferences = copy.copy(self.poppedPreferences)

    @staticmethod
    def restoreShiftPreferences(shifts: List['Shift']) -> None:
        for shift in shifts:
            shift.restorePreferences()
    
    def __repr__(self) -> str:
        """String representation of the Shift object"""
        return f'Shift(id={self.sid}, min={self.minEmployees}, length={str(self.length)}, start={self.start.strftime(DFM + " " + TFM)}, end={self.end.strftime(DFM + " " + TFM)})'

