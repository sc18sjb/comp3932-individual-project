import xml.etree.ElementTree as et
import os
from datetime import datetime, timedelta, time
from typing import List, Dict
from models import Shift, Department, Employee, Availability, Day
from general_parser import Parser
from gale_shapely import GaleShapely

DATASETFOLDER = '../datasets/instances1_225/xml'


class XMLParser(Parser):
    root: et.Element

    def __init__(self, dataset: str):
        tree = et.parse(os.path.join(DATASETFOLDER, dataset))
        self.root = tree.getroot()

    def calculateDays(self, start: datetime, end: datetime) -> List[Day]:
        delta = end - start
        days: List[Day] = []

        for i in range(delta.days + 1):
            d = start + timedelta(days=i)
            days.append(Day(Day.mappings()[d.strftime('%A')]))
        
        return days

    def parseHours(self) -> int:
        for contract in self.root.find('Contracts').findall('Contract'):
            if contract.get('ID') == 'All':
                length = contract.find('ShiftLengths').find('Max').find('Length').text
                return int(length) / 60

    def parseDepartments(self) -> Dict[str, Department]:
        departments: Dict[str, Department] = {}
        for index, department in enumerate(self.root.find('Tasks').findall('Task')):
            name = f"task-{department.find('Label').text}"
            d = Department(index + 1, name)
            departments[d.did] = d

        return departments

    def parseEmployees(self, departments: Dict[str, Department]) -> List[Employee]:
        employees: List[Employee] = []
        for index, employee in enumerate(self.root.find('Employees').findall('Employee')):
            name = f"employee-{employee.get('ID')}"
            d = [d for i, d in departments.items()] # Since we have no preferences, all employees can work in all departments
            a = [
                Availability('morning', time(6), time(12)),
                Availability('afternoon', time(12), time(17)),
                Availability('evening', time(17), time(20)),
                Availability('night', time(20), time(6))
            ]
            e = Employee(index + 1, name, d, a)
            employees.append(e)
        
        return employees
        
    def parseShifts(self, departments: Dict[str, Department]) -> List[Shift]:
        shifts: List[Shift] = []
        for index, shift in enumerate(self.root.find('CoverRequirements').findall('Cov')):
            start = datetime.strptime(shift.get('start'), '%Y-%m-%dT%H:%M:%S')
            end = datetime.strptime(shift.get('end'), '%Y-%m-%dT%H:%M:%S')
            department = shift.get('task')
            minEmployees = int(shift.find('Min').text)
            maxEmployees = int(shift.find('Max').text)
            d = self.calculateDays(start, end)

            s = Shift(index + 1, start.time(), end.time(), departments[int(department)], d, minEmployees, maxEmployees)
            shifts.append(s)
        
        return shifts

def main() -> None:
    parser = XMLParser('1_7_10_1.ros') 
    departments, employees, shifts = parser.parse()

    shapely = GaleShapely(shifts, employees)
    shapely.limitHours = timedelta(hours=parser.parseHours())


    pairs = shapely.iterativeGaleShapely()

    for employee in employees:
        print(employee.getTotalWorkingHours())



if __name__ == "__main__":
    main()