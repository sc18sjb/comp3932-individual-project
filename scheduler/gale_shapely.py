import copy
import logging
from typing import List, Dict, Tuple
from models import Employee, Shift, DFM
from db_parser import DBParser
from datetime import datetime, timedelta

# logging configuration
logging.basicConfig(filename='gale-shapely.log', filemode="a",
                    format='%(asctime)s - %(message)s', level=logging.INFO)


class GaleShapely:
    shifts: Dict[datetime, Shift]
    employees: List[Employee]
    pairs: Dict[Employee, Shift]
    impossibleShifts = List[Shift]
    limitHours: timedelta

    def __init__(self, shifts: Dict[int, Shift], employees: List[Employee]) -> None:
        self.shifts = shifts
        self.employees = employees
        self.limitHours = timedelta(hours=8)

        self.__setPreferences()

    def getPairsSimplified(self) -> List[Tuple[int, int]]:
        """Get the pairs in Name => ID format, the same as we write out."""
        return [(pair[0].eid, pair[1].sid) for pair in self.pairs.items()]

    def __setPreferences(self) -> None:
        """Set the shift and employee preferences ready to carry out the assignment."""
        for day in self.shifts.keys():
            # Set shift preferences for employees       
            for shift in self.shifts[day]:
                shift.setPreferences(self.employees)

            # Set employee preferences for shifts
            for employee in self.employees:
                employee.setPreferences(self.shifts, day)

    def __checkShiftIntersect(self, employee: Employee, shift: Shift, previousShift: Shift=None) -> bool:
        """For a given shift, check if the employee is already working a shift at the same time on the same day."""
        sameDayShifts = [s for s in employee.shifts if s.start.strftime(DFM) == shift.start.strftime(DFM) or s.end.strftime(DFM) == shift.start.strftime(DFM)]
        
        # If the previous shift is provided we don't want to check intersect with this shift since we will be swapping from it anyway.
        if previousShift is not None and previousShift in sameDayShifts:
            sameDayShifts.remove(previousShift)

        for sameShift in sameDayShifts:
            if ((shift.start <= sameShift.start and shift.end >= sameShift.end) or
                    (shift.start > sameShift.start and shift.start < sameShift.end) or
                    (shift.end > sameShift.start and shift.end < sameShift.end)):
                return False
        return True

    def testInersectShifts(self, employee: Employee, shift: Shift, previousShift: Shift=None) -> bool:
        """An interface to test the private __checkShiftIntersect method"""
        return self.__checkShiftIntersect(employee, shift, previousShift)

    def __checkEmployeeWorkingHours(self, employee: Employee, shift: Shift, previousShift: Shift=None) -> bool:
        """For a given shift, check if the employee is already working >= 8 hours or if the shift would put them over that limit."""
        if self.limitHours.total_seconds() <= 0:
            return True
        
        currentHours = employee.getTotalWorkingHours(shift.start).total_seconds()

        # If the previous shift is provided we want to minus its hours from the total hours since we will be swapping from it anyway.
        if previousShift is not None:
            currentHours -= previousShift.length.total_seconds()

        if currentHours + shift.length.total_seconds() > self.limitHours.total_seconds():
            logging.info(f"{employee} cannot work {shift} since it makes the employee work more than the max {self.limitHours} hour limit.")
            return False
        return True

    def __checkDayRest(self, employee: Employee, shift: Shift) -> bool:
        """For a given shift, check if the employees last shift on the previous day was >= 11 hours ago (as per govermental law) so they can work the new shift."""
        previousDayShifts = [s for s in employee.shifts if s.start.strftime(DFM) == (shift.start - timedelta(days=1)).strftime(DFM)]

        # If the employee had no shifts the previous day, we don't need to worry about calculating rest.
        if len(previousDayShifts) < 1:
            return True

        # The last shift the employee worked is the last shift from yesterday
        lastShift = previousDayShifts[-1]

        # If the difference is < 11 hours they can't work the shift
        if (shift.start - lastShift.end < timedelta(hours=11)):
            return False
        return True

    def __checkCanWorkShift(self, employee: Employee, shift: Shift, previousShift: Shift=None) -> bool:
        """ Check if the employee can work a given shift by checking it against a number of given constraints:
            - The shift cannot intersect with another shift the employee is working
            - The shift length must be less than the max hours the employee can work per day
            - The shift must be 11 hours after the last shift the employee worked on the previous day."""
        if (self.__checkShiftIntersect(employee, shift, previousShift)
            and self.__checkEmployeeWorkingHours(employee, shift, previousShift)
            and self.__checkDayRest(employee, shift)):
            return True
        return False

    def __addPair(self, employee: Employee, shift: Shift, remainingShifts: List[Shift]) -> List[Shift]:
        """Create an employee => shift pair and check if the shift has reaches its min employees goal."""
        self.pairs[employee] = shift

        # We add the shift to the employee and the employee to the shift
        employee.shifts.append(shift)
        shift.employees.append(employee)

        # Check if the shift has reached its min
        if len(shift.employees) < shift.minEmployees:
            remainingShifts.append(shift)

        return remainingShifts

    def modifiedGaleShapely(self, day: datetime) -> None:
        """Run a modified version of the gale-shapely algorithm pairing shifts with employees."""
        # Clear data structures
        self.pairs = {}
        self.impossibleShifts = []
        remainingShifts: List[Shift] = copy.copy(self.shifts[day])

        logging.info(
            "Running modified gale shapely with the following preferences:")

        # Log the shift and employee preferences
        for shift in remainingShifts:
            logging.info(f"{shift} - {[e.__str__() for e in shift.preferences]}")
        for employee in self.employees:
            logging.info(
                f"{employee} - {[s.__str__() for s in employee.preferences[day]]}")

        logging.info("Configuration results in the following output:")

        while len(remainingShifts) > 0:
            # We get the first shift in the remaining shifts
            shift = remainingShifts.pop(0)

            # get the first employee in the shifts preference
            if len(shift.preferences) > 0 and len(shift.employees) < shift.maxEmployees:
                employee = shift.preferences.pop(0)
            # We have exhaused all of the employees in the list of preferences, there is not one to match to
            else:
                self.impossibleShifts.append(shift)
                continue

            # If the employee doesn't have the shift in their preference, a match is never possible
            if shift not in employee.preferences[day]:
                logging.info(f"{employee} cannot work {shift}")
                remainingShifts.append(shift)
                continue

            # Default match to the first employee if they are not already paird
            if employee not in self.pairs:
                if self.__checkCanWorkShift(employee, shift):
                    logging.info(f"pair {employee} with {shift}")
                    remainingShifts = self.__addPair(employee, shift, remainingShifts)
                else:
                    remainingShifts.append(shift)

            # If the employee is already in a pair we want to check if there can be a better match
            else:
                # Fetch the previously assigned shift
                previousShift = self.pairs[employee]

                # Lower index = THEY PREFER THE SHIFT since its at the start of the list of preferences
                # We keep the employees previous shift if they prefer it and it has > 0 min employees (so someone has to cover it)
                # We keep the employees previous shift if the don't prefer it but the new shift has <= 0 employees so doesn't need to be filled
                if ((employee.preferences[day].index(previousShift) < employee.preferences[day].index(shift) and previousShift.minEmployees > 0)
                        or (employee.preferences[day].index(previousShift) < employee.preferences[day].index(shift) and shift.minEmployees <= 0 and previousShift.minEmployees <= 0)
                        or (employee.preferences[day].index(previousShift) > employee.preferences[day].index(shift) and shift.minEmployees <= 0 and previousShift.minEmployees > 0)):
                    logging.info(f"{employee} still paired with {previousShift}, {shift} still free")
                    remainingShifts.append(shift)

                # If the employees current shift is lower in its preference then we do want to swap and the employees previous shift is still free
                else:
                    if self.__checkCanWorkShift(employee, shift, previousShift):
                        logging.info(f"{employee} preferres {shift} over {previousShift}, {previousShift} still free : {shift.minEmployees}")
                        remainingShifts.append(previousShift)

                        # Remove the existing shift and employee from that shift
                        employee.shifts.remove(previousShift)
                        previousShift.employees.remove(employee)

                        remainingShifts = self.__addPair(employee, shift, remainingShifts)
                    else:
                        remainingShifts.append(shift)

        logging.info("Finished running modified gale shapely\n")
        
        # We have been popping shifts, so we ned to restore them
        Shift.restoreShiftPreferences(self.shifts[day])

    def iterativeGaleShapely(self, day: datetime) -> List[Tuple[int, int]]:
        """ Run the modified gale shapely algorithm iteratively,
            reassigning employees based on the maximum number of hours they can work in a day."""
        pairs: List[Tuple[int, int]] = []

        while True:
            self.modifiedGaleShapely(day)

            # Base case
            if len(self.getPairsSimplified()) <= 0:
                break
                
            pairs += self.getPairsSimplified()
        return pairs 

    def assignWeek(self) -> Dict[datetime, List[Tuple[int, int]]]:
        """Run the modified gale shapely algorithm for all days the company unit is operational during the week"""
        assignments = {}
        for day in self.shifts.keys():
            pairs = self.iterativeGaleShapely(day)
            assignments[day] = pairs
        return assignments


def main() -> None:
    parser = DBParser('rotaboard.test', '1|OJ2jFHmrFRrzemnbRk6MP63uOn9NzIiuPo8b7LVH')    
    departments, employees, shifts = parser.parse()

    shapely = GaleShapely(shifts, employees)
    pairs = shapely.assignWeek()

    for employee in employees:
        print(employee, employee.preferences)

    print(pairs)

    # parser.createEmployeeShifts(pairs)


if __name__ == "__main__":
    main()