import os
import sys
import inspect
import unittest
import numpy as np
import json

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

from gale_shapely import GaleShapely
from json_parser import JSONParser
from collaborative_filtering import cosineSim, weightedSum, CollaborativeFiltering
from models import Employee, Shift, Department
from working import ShiftRating
from datetime import time, timedelta, datetime

DATAFOLDER = "data/"
DAY = datetime(2020, 12, 14) # Arbitrary day for test data shifts


def collectRatings(data):
    ratings = []
  
    for rating in data:
        r = ShiftRating(
            employee_id = rating['employee_id'],
            shift_id = rating['shift_id'],
            rating = rating['rating']
        )
        ratings.append(r)

    return ratings


class TestClass(unittest.TestCase):
    def test_galey_nomin(self) -> None:
        """Test employee preferences for shifts with min, max=1 (Regular gale-shapely assignment)"""
        parser = JSONParser(os.path.join(DATAFOLDER, "test1.json"))
        departments, employees, shifts = parser.parse()

        shapely = GaleShapely(shifts, employees)
        shapely.limitHours = timedelta(hours=0)
        shapely.modifiedGaleShapely(DAY)

        # Test the preferences are in the correct order
        self.assertEqual(Employee.getPreferenceIds(employees, DAY), [[2, 1, 3, 4], [1, 2, 4, 3], [4, 1, 2, 3], [1, 2, 3, 4]])

        # Test the shift lengths are calculated correctly
        self.assertEqual(Shift.getLengths(shifts[DAY]), ['5:57:26', '4:54:15', '2:45:00', '9:29:50'])

        # Test gale shapely results
        self.assertCountEqual(shapely.getPairsSimplified(), [(1, 2), (2, 1), (3, 4), (4, 3)])

    def test_galey_reassign(self) -> None:
        """Test shifts don't get reassigned for min=1 and do get reassigned for min>1"""
        parser = JSONParser(os.path.join(DATAFOLDER, "test1.json"))
        departments, employees, shifts = parser.parse()

        shapely = GaleShapely(shifts, employees)
        shapely.limitHours = timedelta(hours=0)
        shapely.modifiedGaleShapely(DAY)

        # Test none of the shifts are re-assigned since they all have max=1
        shapely.modifiedGaleShapely(DAY)
        self.assertEqual(shapely.getPairsSimplified(), [])
        
        # Test shifts are re-assigned with max=2 to employees who have not worked them before
        for shift in shifts[DAY]:
            if shift.department.name == 'counter':
                shift.maxEmployees = 2
        shapely.modifiedGaleShapely(DAY)

        self.assertCountEqual(shapely.getPairsSimplified(), [(1, 1), (2, 2), (3, 3), (4, 4)])

    def test_galey_min(self) -> None:
        """Test gale shapely for min>1"""
        parser = JSONParser(os.path.join(DATAFOLDER, "test2.json"))
        departments, employees, shifts = parser.parse()

        shapely = GaleShapely(shifts, employees)
        shapely.limitHours = timedelta(hours=0)
        shapely.modifiedGaleShapely(DAY)
        
        # Test the employee preferences are in the correct order
        self.assertEqual(Employee.getPreferenceIds(employees, DAY), [[3], [4, 5], [1, 2, 3, 4, 5], [1, 2, 4, 5]])

        # Test the shift preferences are in the correct order
        self.assertEqual(Shift.getPreferenceIds(shifts[DAY]), [[2, 3, 4], [2, 3, 4], [1, 3], [2, 3, 4], [2, 3, 4]])

        # Test the shift lengths are calculated correctly
        self.assertEqual(Shift.getLengths(shifts[DAY]), ['5:30:00', '5:45:00', '8:00:00', '8:00:00', '8:00:00'])

        # Test gale shapely results
        self.assertCountEqual(shapely.getPairsSimplified(), [(1, 3), (2, 5), (3, 2), (4, 2)])

    def test_galey_limit_hours(self) -> None:
        """Test gale shapely reassign for max hours = 8"""
        parser = JSONParser(os.path.join(DATAFOLDER, "test3.json"))
        departments, employees, shifts = parser.parse()

        shapely = GaleShapely(shifts, employees)
        pairs = shapely.iterativeGaleShapely(DAY)

        self.assertCountEqual(pairs, [(1, 3), (2, 4), (2, 5), (3, 2), (3, 5), (4, 2), (4, 1)])

    def test_shift_intersect(self) -> None:
        """Test shift intersecting times from single and multiple days"""
        d = Department(1, "teest-dep")
        date = datetime(2022, 2, 14)
        date2 = datetime(2022, 2, 15)
        s1 = Shift(1, time(hour=20), time(hour=6), date, d)
        s2 = Shift(2, time(hour=17), time(hour=20), date, d)
        s3 = Shift(3, time(hour=18), time(hour=22), date, d)
        s4 = Shift(4, time(hour=19), time(hour=7), date, d)
        s5 = Shift(5, time(hour=6), time(hour=10), date2, d)
        s6 = Shift(1, time(hour=20), time(hour=6), date2, d)
        e = Employee(1, "test", [], []) # Create an empty employee instance
        shapely = GaleShapely({}, []) # Create an empty galeShapely instance
        
        self.assertEqual(shapely.testInersectShifts(e, s1), True)
        e.shifts.append(s1)
        self.assertEqual(shapely.testInersectShifts(e, s1), False)
        self.assertEqual(shapely.testInersectShifts(e, s2), True)
        self.assertEqual(shapely.testInersectShifts(e, s3), False)
        self.assertEqual(shapely.testInersectShifts(e, s4), False)
        self.assertEqual(shapely.testInersectShifts(e, s5), True)
        self.assertEqual(shapely.testInersectShifts(e, s6), True)

    def test_cosine_sim(self) -> None:
        """Test the cosine similarity algorithm for a given x and y"""
        x = np.array([2.0, 1.0])
        y = np.array([5.0, 3.0])
        self.assertEqual(cosineSim(x, y), 0.9970544855015815)

        c = np.array([5.0, 4.0])
        self.assertEqual(cosineSim(c, y), 0.9909924304103233)

    def test_adjusted_cosing_sim(self) -> None:
        """Test the adjusted cosine similarity algorithm for a given x and y"""
        x = np.array([2.0, 1.0])
        y = np.array([5.0, 3.0])
        mean_xy = sum(sum(x, y)) / 4
        self.assertEqual(cosineSim(x - mean_xy, y - mean_xy), -0.21693045781865614)

        c = np.array([5.0, 4.0])
        mean_cy = sum(sum(c, y)) / 4
        self.assertEqual(cosineSim(c - mean_cy, y - mean_cy), 0.9908301680442989)

    def test_weighted_sum(self) -> None:
        """Test the weighted sum algorithm for a given set of results and ratings"""
        ratings = np.array([0.0, 2.0, 4.0])
        predictions = np.array([0.63, 0.71, 0.93])
        self.assertEqual(round(weightedSum(ratings, predictions), 2), 2.26)

        ratings = np.array([4.0, 5.0, 5.0])
        predictions = np.array([0.904, 0.852, 0.636])
        self.assertEqual(round(weightedSum(ratings, predictions), 2), 4.62)

    def test_user_user_cf(self) -> None:
        """Test user-user CF and adjusted CF including the predictions from the cosine sim and weighted sum"""
        matrix = np.array([
            [5, 1, 5, 4, 0, 3],
            [3, 3, 1, 1, 5, 1],
            [0, 1, 0, 2, 1, 4],
            [1, 1, 4, 1, 1, 2],
            [3, 2, 5, 0, 0, 3],
            [4, 3, 0, 0, 4, 0],
            [0, 1, 5, 1, 1, 1]
        ])

        prediction = CollaborativeFiltering.userUserCF(matrix, 2, 2, False, 3)
        self.assertEqual(round(prediction, 2), 2.27)

        prediction = CollaborativeFiltering.userUserCF(matrix, 2, 2, False)
        self.assertEqual(round(prediction, 2), 3.13)

        prediction = CollaborativeFiltering.userUserCF(matrix, 2, 2, True, 3)
        self.assertNotEqual(round(prediction, 2), 2.27) # Should not be the same
        self.assertEqual(round(prediction, 2), 1.68)

        prediction = CollaborativeFiltering.userUserCF(matrix, 2, 3, False, 3)
        self.assertEqual(prediction, 2)

        with open(os.path.join(DATAFOLDER, 'shiftRatings.json'), 'r') as f:
            data = json.load(f)
        ratings = collectRatings(data)

        cf = CollaborativeFiltering(ratings)
        cf.method = 'USER'
        cf.adjustment = False
        np.testing.assert_array_almost_equal(cf.matrix, matrix)
        self.assertIn(3.13, [r['rating'] for r in cf.CFzeros()])

    def test_item_item_cf(self) -> None:
        """Test the item-item CF and adjusted CF including the predictions from the cosine sim and weighted sum"""
        matrix = np.array([
            [5, 1, 5, 4, 0, 3],
            [3, 3, 1, 1, 5, 1],
            [0, 1, 0, 2, 1, 4],
            [1, 1, 4, 1, 1, 2],
            [3, 2, 5, 0, 0, 3],
            [4, 3, 0, 0, 4, 0],
            [0, 1, 5, 1, 1, 1]
        ])

        prediction = CollaborativeFiltering.itemItemCF(matrix, 2, 2, False, 3)
        self.assertEqual(round(prediction, 2), 4.62)

        prediction = CollaborativeFiltering.itemItemCF(matrix, 2, 2, False)
        self.assertEqual(round(prediction, 2), 4.49)

        prediction = CollaborativeFiltering.itemItemCF(matrix, 2, 2, True, 3)
        self.assertNotEqual(round(prediction, 2), 4.62) # Should not be the same
        self.assertEqual(round(prediction, 2), 3.34)

        prediction = CollaborativeFiltering.itemItemCF(matrix, 4, 2, True, 3)
        self.assertEqual(prediction, 5)

        with open(os.path.join(DATAFOLDER, 'shiftRatings.json'), 'r') as f:
            data = json.load(f)
        ratings = collectRatings(data)

        cf = CollaborativeFiltering(ratings)
        cf.method = 'ITEM'
        cf.adjustment = False
        np.testing.assert_array_almost_equal(cf.matrix, matrix)
        self.assertIn(4.49, [r['rating'] for r in cf.CFzeros()])


if __name__ == "__main__":
    print("\n------ Running Tests ------\n")
    unittest.main(verbosity=2)