import os
import sys
import inspect
import math
import numpy as np

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

from collaborative_filtering import cosineSim, weightedSum, CollaborativeFiltering

# Actual results
# [1, 3, 1, 2, 4, 4, 3, 2, 3, 2, 1, 1],
# [3, 4, 5, 4, 5, 4, 4, 3, 4, 3, 3, 2],
# [2, 4, 3, 5, 4, 5, 4, 4, 2, 3, 3, 4],
# [5, 3, 5, 5, 5, 4, 5, 4, 5, 5, 3, 4],
# [5, 4, 4, 5, 5, 5, 4, 4, 4, 5, 2, 4],
# [5, 3, 3, 4, 4, 5, 4, 5, 4, 5, 4, 4],
# [2, 2, 1, 1, 3, 2, 1, 3, 2, 1, 4, 1],
# [2, 2, 1, 1, 3, 1, 1, 2, 3, 1, 5, 1]  

def RMSE(preds):
    return math.sqrt(np.sum(np.square(preds)) / len(preds))

def MAE(preds):
    return (np.sum(np.absolute(preds)) / len(preds))

def main()-> None:
    matrix = np.array([
        [1, 3, 1, 2, 4, 4, 3, 2, 3, 2, 1, 1],
        [3, 4, 5, 4, 5, 4, 4, 3, 4, 3, 3, 2],
        [2, 4, 3, 5, 4, 5, 4, 4, 2, 3, 3, 4],
        [5, 3, 5, 5, 5, 4, 5, 4, 5, 5, 3, 4],
        [5, 4, 4, 5, 5, 5, 4, 4, 4, 5, 2, 4],
        [5, 3, 3, 4, 4, 5, 4, 5, 4, 5, 4, 4],
        [2, 2, 1, 1, 3, 2, 1, 3, 2, 1, 4, 1],
        [2, 2, 1, 1, 3, 1, 1, 2, 3, 1, 5, 1],
        [2, 2, 1, 1, 3, 1, 1, 2, 3, 1, 5, 1],
        [2, 2, 1, 1, 3, 1, 1, 2, 3, 1, 5, 1],
        [2, 2, 1, 1, 3, 1, 1, 2, 3, 1, 5, 1],
        [2, 2, 1, 1, 3, 1, 1, 2, 3, 1, 5, 1],
        [2, 2, 1, 1, 3, 1, 1, 2, 3, 1, 5, 1],
        [2, 2, 1, 1, 3, 1, 1, 2, 3, 1, 5, 1],
        [2, 2, 1, 1, 3, 1, 1, 2, 3, 1, 5, 1],
        [2, 2, 1, 1, 3, 1, 1, 2, 3, 1, 5, 1]
    ])

    num_replaced = 10
    k = 3

    # Random (x, y) coordinates
    indices_x = np.random.randint(0, matrix.shape[0], num_replaced)
    indices_y = np.random.randint(0, matrix.shape[1], num_replaced)
    
    previous = matrix[indices_x, indices_y]
    matrix[indices_x, indices_y] = 0

    userUserPreds = np.empty((0, 0))
    itemItemPreds = np.empty((0, 0))
    userUserPredsAdj = np.empty((0, 0))
    itemItemPredsAdj = np.empty((0, 0))
    
    for i, pr in enumerate(previous):
        userUserPred = CollaborativeFiltering.userUserCF(matrix, indices_x[i], indices_y[i], False, k)
        itemItemPred = CollaborativeFiltering.itemItemCF(matrix, indices_x[i], indices_y[i], False, k)
        userUserPredAdj = CollaborativeFiltering.userUserCF(matrix, indices_x[i], indices_y[i], True, k)
        itemItemPredAdj = CollaborativeFiltering.itemItemCF(matrix, indices_x[i], indices_y[i], True, k)

        userUserPreds = np.append(userUserPreds, (pr - userUserPred))
        itemItemPreds = np.append(itemItemPreds, (pr - itemItemPred))
        userUserPredsAdj = np.append(userUserPredsAdj, (pr - userUserPredAdj))
        itemItemPredsAdj = np.append(itemItemPredsAdj, (pr - itemItemPredAdj))

        print(f"""
            userUser: {round(userUserPred, 2)}, userUser adjusted: {round(userUserPredAdj, 2)}, actual: {pr}
            itemItem: {round(itemItemPred, 2)}, itemItem adjusted: {round(itemItemPredAdj, 2)}, actual: {pr}
        """)

    print(f"""
        userUser RMSE: {RMSE(userUserPreds)}, userUser adjusted RMSE: {RMSE(userUserPredsAdj)}
        itemItem RMSE: {RMSE(itemItemPreds)}, itemItem adjusted RMSE: {RMSE(itemItemPredsAdj)}

        userUser MAE: {MAE(userUserPreds)}, userUser adjusted MAE: {MAE(userUserPredsAdj)}
        itemItem MAE: {MAE(itemItemPreds)}, itemItem adjusted MAE: {MAE(itemItemPredsAdj)}
    """)




if __name__ == "__main__":
    main()
