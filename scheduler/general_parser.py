import abc
from typing import List, Tuple, Dict
from models import Employee, Shift, Department
from datetime import datetime, timedelta

class Parser:
    @abc.abstractmethod
    def parseDepartments(self) -> Dict[str, Department]:
        return

    @abc.abstractmethod
    def parseEmployees(self, departments: Dict[str, Department]) -> List[Employee]:
        return

    @abc.abstractmethod
    def parseShifts(self, departments: Dict[str, Department]) -> List[Shift]:
        return

    def getWeekRange(self, day: datetime) -> Tuple[datetime, datetime]:
        """Calculate the monday start date and sunday end date of a week for a given date."""
        weekStart = day - timedelta(days=day.weekday())
        weekEnd = weekStart + timedelta(days=6)
        return weekStart, weekEnd

    def parse(self) -> Tuple[List[Department], List[Employee], List[Shift]]:
        # First, we get all the departments in the company.
        departments = self.parseDepartments()

        # Second, we get all the employees and their respective departments and availabilities.
        employees = self.parseEmployees(departments)

        # Finally, we get all the shifts and which department that shift is in.
        shifts = self.parseShifts(departments)

        return departments, employees, shifts